# Nightsisters

## Crawl

The NIGHTSISTERS--a dangerous tribe of witches from the jungle planet Dathomir. For years, the apprentice TIMM'EH has sought them as the ones who massacred his tribe, and for years they have stayed hidden in the shadows.

Now, as the crew of Apprentices prepare for their next adventure TIMM'EH receives a message from NASRA, an ally of the Apprentices' and former Nightsister herself. She has uncovered evidence that a coven of Nightsisters is acting on the planet ELROOD.

Can the Apprentices uncover what the coven is planning, and why they are so far from Dathomir?

## High-level

* What are the Nightsisters doing in Kriusia?
> They are here to assisinate an Imperial Moff on behalf of the Zann Consortium

* Three nightsisters dispatched
  * Two accolytes (knights) one initiate
* Currently plannning and scheming
* Imperial Moff will be planet-side in a few days
* Intend to assisnate him
  * Manipulate the weather to bring down his shuttle
  * Follow up in person if needed
* Move off planet within hours

## Planet - Elrood
### City of Kriusia
* Terrestrial planet
* Heavily controled by megacorps
  * Moderate-high Imperial presence
* Shadowrun-y
  * Bustling metropolis of 10 million
* Three starports:
    * Primary
		* V. Secure
    * Military
		* Locked down
    * Shipping
		* Somewhat secure, remote
	* O: Lamos city port
		* Secure, nearby city accessible by speeder and commuter train
	* O: Pantha local port
		* Mostly used in delivering fishing equipment from on-world
		* Location of the NS ship
* Businesses in manufacturing, food, colonization goods
* Varied population
    * 60% Human
    * 40% Misc
> Current underworld domintated by the remote Hutt interests
    * No Hutts on-world
    * Local organized crime openly allegiant to Jabba
    * Blacksun Mercenaries active
>> Zann Consortium looking to expand interests
    * High-ranking organized crime aware of conflict between Jabba and ZC
> Governor Cen Horne proissing new public works
    * Major improvement and public-works planned
        * Not sure where the money is coming from
	* Well liked by the locals, and the Hutts, strong relations with Imperials
>> Plans to negotiate new planetary governance model
    * Major improvement and public-works planned
        * Not sure where the money is coming from
	* Local politicians aware
		* Retreat planned at the Jade Star for the next week

## Nightsister coven
    * Sister Samuul
        * Human
        * Mid-aged, dark brown hair folded intricately
        * Driven, industrious, ruthless
    * Sister Terrano
        * Human
        * Somewhat young, long, braided brown hair
        * Social, civil, thick-skinned
    * Sister Vancil
        * Dathomirian
        * Young, short orange hair, pale gray skin
        * Patient, hard-working, nervous
> Contracted by the K'zakk mercenaries
* Newly registered mercenary crew on the planet
    * Local bounty hunters aware of new registration, but never met any
    * Blacksuns openly hostile
>> Shell for the Zann consortium
    * Aided the NS to leave Dathomir
    * Financial links
    * K'zakk offices in corporate district are minimal
        * Coded traffic with the ZC
> Casting a spell to affect the weather
    * Requires ingredients to supplement their limited spirit ichor
    * Local meat stores sold out of Rancor meat
        * All local butchers shipping whole corpses to the NS
> Arrived 6 days ago by boat
    * Port logs show a 'yacht' arrived, but port of origin not registered
    * Planetary starport logs show a shuttle arrived from Dathomir
        * Logged as refugees
        * Not in BOSS
	* Landed in Pantha port
> Stayed in a housing block in the fishing district
    * Disappearances
    * None of the named citizens living the block -> killed by magick
    * Mysterious shipments
		* Rancor blood
		* Orichalchite
>> Day-of, staying at an expensive hotel (The Jade Star) overseeing the primary starport
    * Follow financial records
    * Rumour that "princesses" are staying at the Jade Star
> Boat and ship standing by to get off-world
	* Docked at respective locations, with port fees paid until day after event
	* Not known by the local fishers
		* Obviously out-of-place in Pantha

## Imperial Moff - Vinon Tremarch
    `calm, collected, dismissive`
* Tan skin, graying hair, grey military uniform
* Competent but unexceptional administrator
> Running major development and improveent programs in the sector
    * Some talk of his development work on Bodrin
    * Generally sympathetic view
>> Tacit involvement, or at least acceptance of Hutt interests
    * Organized crime settling down
    * High ranking officials aware of the impending boom
        * Interested in preserving the peace
	* Lots of contracts to Hutt interests
> Arriving in two days as part of a low-key diplomatic visit
    * Increased security lately
    * Local organized crime aware of it
    * Main port Security on-edge
>> Plans to negotiate new planetary governance model
    * Major improvement and public-works planned
        * Not sure where the money is coming from
	* Local politicians aware
		* Retreat planned at the Jade Star for the next week
>> Travelling with family
	* Booking at the Jade Star for 2+1daughter
	* Rumour with the hotel staff that he's staying in the penthouse
		* Wife is attractive

## Assassination
> Running an assasination
	* Mission info in the NS hideout
	* Rummor on the street is that there is a big bounty on someone
	* Not on any of the boards
		* Figure that's why there are so many BHs
		* Maybe someone not playing nice with the Hutts
>> Not issued by Hutts or Imperials
	* Local guild has nothing
	* No records in ISB
	* Hutt-alligned crimebosses don't know anything
> Targetting the Moff
	* Target info in the NS hideout
	* Boat they arrived on and ship both have recorded communications
>> Preference to look like an accident
	* Contract details in the K'zakk headquarters
> Large amounts of magical materials being stockpiled
	* Requires unusual materials -> Rancor blood, orichalchite
	* Rancor carcasses being shipped to a housing block in the fishing district
	* Orichalcum purchased by a woman from weapons shop/starship components
		* Used for manufacturing large shells
> Weather being manipulated by the coven
	* Always raining - unseasonable
	* Persistent dark presence
>> Will cause the Moff's shuttle to crash
	* Terrano near the crash-site to follow-up
	* Vancil providing long-range support if needed from the hotel
	
# Next steps
* Check on property company which owns the complex
    * Recently been bought out by a shell company (TPI Consolidated)  with subsidiary connections to K'zakk
	* No record of the building 
		* Passed off to a construction company
			* Also shell with connections to the other two
	* One of the office staff has a brother in the BlackSuns
		* Heard rumour that the K'zakks picked up a big bounty from off-world
		
* Check on former tennants of the building
	* No sign of them
	* Most not in the city anymore
	* Rest remember moving out, not where it is
	* One under continuous influence, works at the Jade Star as an attendant

* Sisters aware someone knows, additional security in hotel
	* Entryway -> Symbol of Distraction 
		* Drawn towards an unusual painting of a knobled tree groing into five parts
	* Elevator -> Nightmare spell
		* Fear check, fall into a nightmare
		* Attacked in the elevator
		* Enemies unaffected
		* Wake up
	* Room -> Deception spell
		* Room appears normal and empty until Samuul attacks
	* Draws one member in and seals the door
		* Interrogates
	* During fight, appears to be fighting each individually
		* Make discipline checks after attacking her
		* Attacks are immaterial, but cause strain
		
	* Symbol of exsanguination
	* Symbol of mirror
	* Choking fog
	* Mindfog
	* Hunger for flesh
	* Maze
	* Mindblank
		
* In mean time, Vancil positioned on the roof with a rifle
	* Concealing use of force
	* Tries to slip away
	* Provides supporting fire for Terrano
* Terrano concealed by the starport
	* Aggressive use of force
	* Force lightning!
	
