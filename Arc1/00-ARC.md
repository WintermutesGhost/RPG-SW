## Planned

* [01.P] Attacked on ISO-One
    * [02.D] Flee to soccorro
* [02.P] Locate temple, learn of Jedi and Sith
* [03.P] Leave temple, ambushed, escape, Navid has fled to askaj, leave with Argon Heavy
    * [04.D] Shown rebellion fleet
    * [04.D] Navid has visions to travel to Kanthi
    * [04.D] Arrive to find Askaj besieged
* [04.P] Meet with Navid, find Askajian friends captured, free and escape
		* [04.S] DERAILED - Under fire and no good way out
    * Seek to travel to Kanthi -> need more info
    * Ashur Sungazer captured
* [06.P] Free Ashur
* Intermission 
    * Trade/raid for rebels/soccorro guy
    * Ashur recommends raiding imperial Scout Corps archives for starcharts
* Archive raid, meet Inquisitor
* Travel to Kanthi
    * Shot-down, pass out
* Vision as ancient jedi
* Come to, avoid crashing


## Actual

