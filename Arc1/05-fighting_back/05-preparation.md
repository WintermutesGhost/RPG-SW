Reunion - Part 2

The Apprentices have reunited on the fringe-world of ASKAJ, but celebrations were cut short. The heavy boot of the Empire can be felt even on a back-water like this.

MOS SHUUTA has fallen under Imperial control and EREN GARAI has pursued them from Soccorro. After sneaking in, the Apprentices found the inhabitants desperate and their friends missing--or worse.

Now they have been discovered. Imperial forces are hunting them through the city and life promisses to only get harder for its inhabitants. Will the apprentices choose to fight, or to flee?