23rd irregulars "Deaf Bandits"

## Aema Starbringer
	* Leader
	* Dedicated, neat, self-righteous
	* Copper hair, silver eyes
		* Characteristic of Cardotans
	* Comes from a noble family
	* Modified DH-17C blaster carbine
	* Composite armour

## Nejilin "Nej" Hillac
	* Slicer
	* Practical, nervous
	* Cyan hair tied back
	* Once part of Ha'am's orphans
	* BAW E5 blaster carbine
	* Flack-jacket

## Khrusk "Slug" (Trandoshan)
	* Pilot
	* Amiable, gullible
	* Scar over left eye
	* Once worked for the Hutts
	* HH-50 Heavy Blaster Pistol
	* Flight suit

## Jace Carte
	* Face
	* Loyal, unrealistic
	* Slicked back short brown hair
	* Brother disappeared to Imperial camp years ago
	* Blastech HSB-200 Holdout blaster
	* Silver Armoured Suit


Message from Daro:
	"To the crew of the free-ship 'Golden Noble': Deneb spoke highly of you after your last misson, and has told me that you are interested in further work. I have a time-sensitive contract to transport four passengers from Kalinda, and you are the only ship I have in the sector. Pay will be 6000 credits if you complete pickup within 3 hours.

	Reply if interested, but do not wait for confirmation. Passengers will be waiting in Landing bay 17 in Kalinda Prime. Avoid Imperial interference."



* Team arrives at Kalinda
	* Find the 'passengers' under fire from storm troopers
	* Luggage tram with remains of a Viper probe under a tarp
	* Jace is wounded badly. carried in by Khrusk
	* Pickup