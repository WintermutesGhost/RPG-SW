

# Phoenix
> Dawn breaks over the forests of Kanthi. You awake in the shattered hull of the Golden Noble, held tightly by the crash-belt of your seat. Beside you, the hull is split open--torn in two the length of the ship. Oils drain from the fracture into the murky pools you have landed in. A muted, futile warning sounds somewhere in the ship. Raw fuel fumes waft up from the rainbow-coloured water.

> You are alive, saved by the crash seats and a good amount of luck. There is a taste of iron in your mouth. Check yourself for injuries.
* Start at 50% wounds and strain, roll critical

> As you pull yourself out of your seat, a wave of nausea passes over you
* Currently in a phase 4 radiation leak from the compromised sublight reactor-housing
* Resilience save after each action round - Normal
  * Upgrade difficulty each round
  * Failures cause one wound
  * Setback cause strain
  * Despair -> Nausea
  * Triumph -> Avoid next round

> Ha'am, across from you Sar is unconcious, but breathing.
* Starts at 50% wounds and threshold strain, roll critical

> The lower deck is a mangled wreck. The floor has buckled, and sections have been pressed completely to the ceiling. Smouldering electrics have made the air toxic, stinging your eyes and nose. Emergency lighting has failed, but occasional crackling arcs from damaged couplings illuminate the area. Lodged beneath a dislodged cylindical housing, you find an inactive K-17. The droid is mostly in-tact, but cannot easily be removed from where it is stuck. Nearby, Baldyr lies among a pile of heavy tools and dislodged conduits. He is injured, but alive. 
* Start at 75% wounds and threshold strain, roll critical +20


## Broken wings
* Recover damaged items
* Choose something you have lost in the crash
* Choose one of the group's items to save, the others are destroyed
  * You may give up something important to save another
```
- Healing crystal of fire
- Val'isa Holocron
- Force Sense datapad
- Iron-arm's vibroblade

? Nightsister crystal
? Lothcat
? Pilot's helmet
? Druma'san codex
? Ancient sword 
```
* Some things may not be worth carrying
  * Scrap
  * Assault cannon
  * Box of Orichalcum

### Lost
* Stranded on Kanthi, but recall the map from past visions
* Currently atop the Dhakira falls, in the pools
* Land below is as they recall, but damaged starship in the barren hills
* Hovercraft still near bottom of falls, walkers near the landed exploration ship


## Aside

### Cave of echoes
Meet echoes of their past. If they have done well, they receive a {{Kyber crystal}}
> What are you looking for?
* Fighting Asori
  * Dark power overcomes
* ISO-1
  * Airlock slams shut
* Attacked on Alidade
  * You won't stop me
* Asori - Beast attack
  * Orca savaged, being fed upon by the beast
* Ha'am - Gang attacked on old world
  * Feevo (Rodian) frustrated by the Imperials
  * Turns to the window, shot
* Timm'eh - Questioned when leaving Dathomir
  * Ship rocks as it pulls away from the planet
  * Duros first-officer asks about reason for leaving - upbeat, beautiful planet
* Baldyr - Dark beasts
  * Inside the temple, in the room of blades secret door slides open
  * See inside, dark tunnel
  * At the bottom, rows of obsidian sarcofogi
  * Sliding open, skittering demons emerging

### Father
* *Sovereign of Keena*, a Xanter-class exploration vessel
* Depleted of fuel, the reactors were run for months, but are now out
* Contains records of Dr. Adneris' research
* Ilum amulet on the dash of the ship
* Voice when entering the ship


## Ruined temple
* Alssaea is no worse for wear
* Lines of plants growing by the front entrance
  * Neat rows of celto, charbote, and balka-greens growing out front
* Crystal orb is missing

### Chrono
* Vision of Sith Lord taking the crystal
  * Lord Starskimmer steps around the crystal, examining it
  * Turns, surprised, back to the meditating students


## Forgotten master 

### Inquisitor
> **The Inquisitor - Tycho Sunfell**  
> Calculating, wrothful, creative  
> Born on devastated Humbardine, witnessed the suffering left by the Clone Wars during the separatist assault. Driven by Anger, and to ensure this can never happen again.   
> Intends to make the Empire strong, and safe. To ensure no calamity like the war happens again.  
> Has no love for the Inquisition, but remains a member to continue removing subversive elements.  

### The Forgotten
> **The Forgotten - Mathias We**  
> Cold, disheartened, generous  
> Raised from a young age to become a Jedi, Mathias trained but showed a poor aptitude for the force. After failing the Padawan   Trials three times, he chose to enter the Agricultural Corps.  
> Was a close friend of Master Zyn, and fled to Kanthi with him when Order 66 came. They crashed, and Zyn was killed.  
> Mathias took his sword, and has survived here for over 30 years.  

### The Master  
> **The Master - Zyn**  
> Impulsive, quick-thinking, craven  
> Childhood friend of Mathias, Master Zyn was well suited to the force. His studies on the Jedi were lacking, but Zyn still became a Knight at a reasonable age, and was given the title of Master after heroics in the Clone Wars.  
> Led daring missions targetting rear facilities, and with his crew of troopers quickly established a reputation in the Republic armies.  
> Warned of the impending Order by his squad, he fled to Kanthi with his friend Mathias. Believed to have been killed in the crash on Kanthi.  


## Rebirth

### From ashes
* Damaged, heavily modified Consular c70 Charger *Corona Kite*
```
* Requires fuel pre-injectors from *Sovereign of Keena*
  - Reactor is in-tact and fueled, pre-injectors needed to ignite
* Power system is offline -> starts at full system strain
* Structural damage from the crash -> starts at 50% hull trauma
  - Can be reinforced with salvaged plating
  - Armour can be temporarily boosted with triumphs
* Repair engines - Partial allowed
  - Seized, unable to start
  - Obstructed by significant amounts of dust
  - Thermal shock has braced the turbine vanes
  - Engine overheating
  - Cooling vanes have clogged, but no spare coolant to drain
* Restore shields
  - Deflector dishes separated in the crash
  - No power
  - Spare couplings turn to dust when touched
* Reboot computer
  - System offline
  - Requires significant re-programming
  - Primary datastores compromised
* Calculate astrogation path
  - Stellar positions are extremely wrong
  - Primary APU indicates stellar date 15 years from now
  - Secondary APU indicates date 700 years in the past
* Supplies
  - Autochefs offline, but can work off arbitrary biomass
  - Could restock otherwise
* Bring weapons online
  - Turbolaser batteries heavily damaged, non-functional
  - Servo hydraulics run dry for some of them
  - Others have shattered exciter barrels
* Hyperdrive functions
* RCS intact
* Landing repulsors in-tact
* Life-support operational
``` 
* Inquisitor Sunfell is closing in
  * 4x AT-ST
  * 100x Stormtroopers
  * 20x Deathtroopers
  * 5x Tie-LN
  * Cypher-9
  * 1x AT-AT



### Flight
Ship Attachments
* 0 Dyne 597 engine upgrade - +1 Speed
* 1 Engine Ring Stabilizer - +2 Handling
* 1 Stalling Engines - -1 Speed, 1 SS to accell/deccel/eva
* 0 Custom refit - +1 Hardpoints
* 0 Salvaged turrets - Turbolasers lose linked
* 1 Pseudo-cloaking device - Difficult to detect
* 1 Faltering Deflectors - Shields collapse after taking damage, avg mech to restart
* 1 Comm static - Upgrade comms checks, Easy(upg) comp check to communicate
* 0 Suspect Transponder - ID code is suspicious


Attacked by Ties + Interdictor cruiser


## Hatchling

### Require Repairs
http://www.mediafire.com/file/ibxknumrbb5zobk/Ship+Quirks.pdf


### Require a crew
* Droids
* Nasra and Morgana
* Ashur Sungazer
* Rebels
* IsoTech
* Deneb and Jeela
* 