## Setup

### Droid work

K-17 requires full use of the ship to decode hyperspace routes to Kanthi


### Unexpected Accompaniment

* Message received from Baldyr requesting meet at Zheri Station
* Going on pilgramidge, wishes to join the party

### Job offer

Contract offer received from KTC: Perform a tunnel dive into the depths of Hypori to retrieve valuable patent paydata from a Clone Wars-era droid factory. 

	FROM: KOVA TECHNOLOGY CONSOLIDATED
	TO: INDEPENDENT CREW OF FREE-SHIP "GOLDEN NOBLE"
	SUBJECT: CONTRACT OFFER

	YOUR CREW HAS BEEN RECOMMENDED BY OUR SUBSIDIARIES FOR ABOVE AVERAGE MISSION PERFORMANCE AND RESPONSE CAPACITY. CONTRACT COMPLETION RATE CONSIDERED EXEMPLARY. COMBAT RATING HIGH. SUBTLETY FACTORS BELOW AVERAGE.

	PERSONAL RECOMMENDATIONS HAVE BEEN OFFERED FOR THE FOLLOWING CREWMEMBERS:
		> ASORI TA'EM
		> HA'AM SHATIRA
		> K-17
		> NA'VID ADNERAS
		> TIMM'EH

	ACCORDING TO THE ABOVE, YOU HAVE BEEN SELECTED AS A PREFERED CONTACT FOR AN UPCOMING CONTRACT ON PLANET HYPORI.

	MISSION PARAMETERS ARE AS FOLLOWS:
		> RETRIEVAL OF PAYDATA FROM ABANDONED FACILITY
		> LIKELIHOOD OF COMBAT: 78%
		> EXPECTED COMPETING SALVAGE TEAMS: 4
		> NUMBER OF HOSTILE UNITS: UNKNOWN
		> PROBABILITY OF ADDITIONAL COMPLICATIONS: 99%
		> MISSION PAYMENT: 10 800 CR

	CONFIRM ACCEPTANCE OF CONTRACT WITH TRANSMISSION TO HOLONET ADDRESS: KB04HFJ2 JK9H9E

	PROCEED IMEDIATELY TO PLANET HYPORI FOR FURTHER BRIEFING.


		KOVA TECHNOLOGY CONSOLIDATED - MERCENARY AFFAIRS DEPARTMENT 





#### Kova Technology Consolidated
	* Shell corporation for IsoTech
	* Robotics and personal electronics manufacturer
	* Patents held in the planet have potential applications in cybernetics



## Adventure

HYPORI: A small, mineral-rich world at the edge of the Ferra sector. Location of numerous ongoing mining operations. Historically the site of a large number of subteranean Techno-Union weapons factories. Supports a small community of quasi-legal mercenaries known as 'tunnel-divers' who complete salvage contracts to the abandoned facilities.

RANDELL MINING CORPORATION: A mining company that worked primarily in the Elrood sector. Although headquartered on the planet Elrood, the corporation owned many other planets in the sector, such as Halbara, Acatal and Dega and had offices located in the Elrood Bazaar. During the time of the Galactic Civil War, Radell Mining Corp. struggled with Imperial Mining, Ltd. for economic control of the sector. As a mining company, RMC funded many planetary explorations, discovering planets such as Alluuvia.


### The Dig

#### Meet and Greet

Met by KTC representative Director Ne'hen.

* Daral Ne'hen:
	* Female Human administrator
 	* Assertive, vigorous
 	* Loves strategy games
	* Hates wasting time
	* Dark Skin, deep hazel eyes
	* Wearing sharp blue business-wear

* Introduced to facility
	* Called the Dig by local workers
	* Extracts 40,000 tonnes of ore daily
	* Employs 5,000-15,000 employees at any given time
	* Extensive facilities for recreation or provisioning
	* Quarters are available if desired
	* Network of tunnels cross throughout the ore-rich rock
	* Techno-Union established countless automated factories beneath
	* Stockpiles and processing facilities discovered every few weeks
	* Full factories less common, but mid-sized recently discovered
	* RMC has no interest in salvage, so sells rights
	

#### Contract Details

	* Required to retrieve secure patent data from CIS facility
		* Extractable by primary core dump
		* Payment docked for replicated data without valid access keys
	* Three other interests also purchased rights
		* May be seeking paydata as well, or other salvage
		* Competition is expected, outright hostility forbidden
	* Supplies and transport supplied by KTC
		* Transport via "Kouhun" tracked vehicles
		* Most recent geological and seismic mapping data 
	* Facility is located approximately 7 km below the surface
		* Believed to be a war-droid manufacturing facility
		* Some level of activity expected, low-autonomy without CnC
		* Activation of facility possible
		* Told you handle combat well
	* Condition on salvage contract
		* Facility must be destroyed or rendered inoperative
		* All salvage rights revoked if facility in-tact
		* Options include planting of mining charges, overloading of reactor, or flooding with water or magma
		* Entrance tunnel will be sealed 4 hours after salvage begins if confirmation of facility destruction is not received.
	* Still interested?
		* Any additional salvage yours to keep
		* Came looking for paydata
		* Heavy munitions excluded from salvage - contraband

#### Carousing

Introduction to other teams:

* 2 Black Sun Mercenaries
	* Officially functioning for Xizor Transport systems
	* Headed by Female Quarren 'Maral Kendine'
		* Laidback, aggressive, bad loser
	* Qes - F Toydarian
	* Soda - M Human
	* Eir - M Human
	* Most likely to interfere
		* Dive-team has received 9 salvage penalties 
	* Seeking droid brain prototypes
	* Plan to overload reactor

* 1 Hutt Cartels
	* Officially functioning for Basuda Corporation
	* Headed by Male Twi'lek 'Eswol'lia'
		* Quiet, gambler, ruthless
	* Nol'enotak - M Twi'lek
	* Nebtr'etorr - M Twi'lek
	* Jartsi'arawn - M Twi'lek
	* New arrivals
		* Keep to themselves
	* Seeking munitions to smuggle out
	* Plan to detonate magazine w/ Thermal Detonators

* 4 Consolidated Learning Systems
	* Voting member of CSA
	* Headed by M Human Cpt. 'Ienn Villia' 
		* Logical, relaxed, non-commital
	* Kelah - M Nikto
	* Boc'acor - F Twi'lek
	* Fir - M Human
	* Experienced
		* Nikto is new, lost member few weeks ago, won't say how
	* Most likely seeking patents as well
		* Play by rules, but not affraid to push hard to finish job
	* Plan to bring Plasma Resonance Multiplexer in operations


#### Sabotage

Qes loosens Resonance Dampener. If undetected, triggers failure after 3 km 


#### Starting line

Ne'hen meets first thing in morning near the entrance to Bore 238.
Kouhun is primed and fueled. 
Other dive-teams lined up and strapped in.
After 30 minutes, boring machine emerge.
RMC official (M Human) announces salvage start.
Teams leave in 5 min intervals


### Bore 238

0. km - Surface
1. km - Lose connection with navlink - Perception/Computers/Survival
2. km - Rough terrain - Piloting
3. km - Possible component failure - Mechanics
4. km - Cave in - Perception/Piloting/Computers/Survival
5. km - Attack - Maral's team attempts to damage tracks - Mechanics
6. km - Bad air/Flamable gas - Perception/Survival/Piloting
7. km - Factory


### Factory

Computer core needs power for operations
Handful of slow droids wandering, partially active turrets

1. - Auto-turret
2. - 
3. - 1d2 low-power B2
4. - 1d6 low-power B1
5. - 1d6 active B1
6. - 2d6 inactive B1
7. - 1d6 inactive B1
8. - 
9. - 2d6 collapsed B1
0. - 1d6 collapsed B1


### Forgotten Munitions

Reactor activation triggers full activation
Radio from Cpt. Villia 
	* "Reactor emissions are spiking, and we're picking up increased positronic feedback from the droids."
T-DH7 takes command of facility
	* "All combat ready units, prepare to repel Republic Infiltrators"
Radio from Cpt. Villia
	* "This is getting complicated fast. We are moving the resonator into place for immediate detonation. Confirm copy."
Manufacturing begins
Radio from Cpt. Villia
	* "..., ..., confirm copy. We are looking at full Auxon status if we don't put a stop to this right now. If you don't want to die burried under a million tonnes of rock, move to the bore-point and help us get the resonator in place."

1. 
2. 
3. 
4. 1d6 B1
5. 2d6 B1
6. 2d6 B1 + B1O
7. 1d4 B2 + 1d6 B1 + B1O
8. 1d4 B2 + 2d6 B1 + B1O
9. 2x DD
0. 2x DD + 2d6 B1 + B1O


### Distress

* Maral's team pinned down in Droid Brain Programming
* Eswol'lia's team MIA in Munition Assembly
* Villia's team unable to retrieve Resonance Multiplexer - Secured by droids


## Wind-down

### Topside

Met by Dr. Ne'hen


### Old Friend

* Norta waiting to meet them
* ISO-One barely escaped
	* Heroic actions of chief engineer saved the ship
* Provides secure node and decryption coordinates to locate ISO-One
* 