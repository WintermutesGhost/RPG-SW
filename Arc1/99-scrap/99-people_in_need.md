### The Milk Run


#### ATTACK!  (???)  
-Interdicted by pirates   
    -Asteroid dragged into hyperspace lane  
        -Need to reconfigure hyperdrive  
            -10 turns  
            -A Astrogation reduce turns by 1 per success  
        GAT 12-H attacks  
	

#### The crooked buyer   
-Daro Tillie  
-Offers 5000 credits  

-Unfortunate change of plan  
	-I will give you 5000 credits  
-One of the nearby villages has fallen on hard times and cannot pay fees  
	-Can not even afford to pay his security forces  
	-At this point getting dangerous for the villagers  
-Seem like well intentioned folk  
	-Go to the village at these coordinates  
	-Bring the townsfolk back here  
		-Not safe with the M'onnok    
-If you can bring back the rented vapor conedsators, I will pay extra 2000  
-Offers speeder to reach there  
-4000 credit export fee to leave with the milk    
-If ask, offers to guide to temple  
	
#### Hired Thugs  
-Navigation & Survival to reach village  
	-Navgation 4
	-survival 2
-Arrive in village  
	-Destitute  
	-ruined huts sit at the edge of the village  
-Reluctant to leave  
	-Elder Oman has no interest in talking  
		-A+ Charm he will ask for more time to pay  
		-Explain that problem with M'onnok raids has meant they can barely feed themselves  
	-4 Refuse to leave their homes   
		3x  
		1x Skyholme Astromancer?  
#### Another way   
-Desperate farmers  
	-Lin Waldas says that their village is dying, they cannot afford food or medicine  
	-begging for charity  
-Lend aid  
	-Food  
	-Medical  2
	-Mechanical 3 - Repair condensers  
	-Retrieve stolen parts from M'onnok camp  
-If helped, they can lead to the other villages, which will buy the milk @125  
-Can also guide to temple  