The Golden Noble isn't comfortable, but it is familiar. As the ship lifts off you think of all that has happened as late. Visions, phantasms, ancient maps, assassins, imperial attacks and slave colonies.

For the moment you are free of your pursuers, and you may travel where you wish. The Noble will need some time in port for repairs. Adarlon is the nearest planet with a Stellar-class starport, but you may travel further if desired.

Having finally scored a victory against the Empire, where will you go? What will you do?






