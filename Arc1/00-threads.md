## Items

### Found lightsabre crystal
- Calls to Asori -> Side adventure
- Will be ghostfire

### Datapad
- Personal log from chief engineer onboard former freighter (now ISO-One)
- Reading can lead to abandoned pirate base with loot and abandoned fighter
    - Alpha-3 Nimbus [SoT:57]

### Library temple navcoordinates
- Coordinates to ancient temple-worlds
    - Soccorro
    - Velari
    - Vergesso
    - Cranan
    - Kanthi
    - Ryndellia
    - Pire
    - Gara
    - Nila 
- On the other side of the A'Karra Nebula
    - Navigational information will be needed

### Holocron shards



## People

### [Jungle Lady]

### Deneb, Daro and the Rebellion

### Isotech

### Alloy
* Personal conflict with the party
* Tortured Asori
* Involved in human trafficing and slavery
* Part of the ZANN CONSORTIUM

### Zann Consortium
* Deal with the Nightsisters
* Oppose Empire
* Conflict with the Rebels

## PC Motivations

### Asori
* Tennebrous - Seeks to become material
  * Aids Asori
	* Twilight blessing
	  * Once per session, may flip the colour of one force pip as part of a force check (dark -> light or light -> dark)
	* Twilight consecration
	  * Once per session, add one light and two dark force pips to a force check
	* Twilight bennediction
	  * May spend dark force pips without spending destiny points

### Timmeh
- Becoming a Seer
- Hunt nightsisters

### Ha'am

### Navid
- Find father
- Reach Kanthi

### K-17
