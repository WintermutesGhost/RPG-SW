# TODO
* Assault on the Vehement
  * Scathing rebuke

# Nothing lasts forever

#### Crawl
KANTHI, an ancient planet lost to time. Four apprentice Jedi have travelled beyond the edge of known space to reach it, seeking knowledge of the ancient Jedi Order.

Even as they approach the planet they wonder what drew them here, and what mysteries lie beneath its lush forests. The ancient temple on the surface may hold the key to rediscovering the secrets of the Jedi.

More worrying, perhaps, is the mystery of what calamity could have befallen this world...

## Beartrap
Ambushed by imperial Interdictor, with Ties already in the air. Cypher 9 leads attack run and shoots down the Golden Noble.

### Flat spin
Team attempts to recover before crashing
```
* 4 Rounds of each character acting, 16 total

* 4 - Regain control
	* *Required to survive*
	* **Daunting without power, Hard with power**
	* Determine re-entry vector
	* Maintain heading
	* Locate landing site
	* Analyze atmospheric density
	* Copensate for wind-shear
* 4 - Restore power
	* *Reduce difficulty of 'regain control' checks by 3*
	* **Hard**
	* Extinguish the fire
	* Replace the fuel linkages
	* Reconfigure the control system
	* Realign control rods
	* Unstick thrust vectors
* 8 - Brace for impact
	* *Stow equipment and secure everyone to avoid losses*
* 4 - Integrity
	* *Decreases with critical failures, causes disaster if it hits 0*
	* Jammed hydraulics
	* Breached fuel reserve
	* Broken wing
* / Avert disaster
	* *Deal with ongoing effects*
	* Atmospheric dampeners
		* Add 2 disadvantage to all checks
	* Heat shields
		* Upgrade all checks
	* Injury to Saar - Cranial trauma
		* Hard Medicine to treat
```
---

# Blackout

## Dramatis Personae
Team of Knights that have travelled together for the past two years. Have quelled raiders and fought to restore stability in the outer rim.

Most recently, deployed to the Kessel sector where they eliminated the Alad V and his criminal enterprise importing and cybernetically modifying slaves to work in the spice mines.

Currently on retreat to Kanthi to recover and for Uray to visit his sister Ara who is undergoing her padawan trials.


> You are one of a team of Jedi Knights fighting in service of the Republic. You have fought and bled with your companions for the past two years--bringing order and security to the Outer Rim. You have broken up the rancor smuggling ring along the Hydian Way, ended the Luminous War before it even began, delivered humanitarian supplies to Tatooine during the moisture famines, and avoided tragedy during the Shattered City hostage situation. You are the Republic's sword and shield, and a symbol of hope to the downtrodden. You are Jedi.

> Recently, you eliminated the pirate kingpin Lord Adler and dismantled his spice-mining consortium. The slaves you freed swore eternal gratitude to you and the Jedi, and using the proceeds from the sale of the cartel's assets you bought them passage with a new life on the Rim. 

> Now, you are visiting the Jedi retreat on Kanthi. Despite being a young and remote temple, a growing number of Jedi choose to visit it and escape from the turmoil of the greater galaxy. Seers and prophets are also drawn here to the mysterious vergence, and often find their powers amplified.

> Aside from being a reprieve from the strife you face in your duties, the visit to Kanthi has given Knight Uray the opportunity to see his sister. Young Ara Madak is completing her training, and currently undergoing her Padawan trials. 

> Shortly after you began your stay on Kanthi, news arrived from the core worlds: An invasion force from beyond the Rim, and claiming to be the reforged Sith Empire, had attacked the Republic. There is debate amongst the Jedi as to whether they are truly Sith, but the silencing of several rim-worlds and loss diplomatic ships have forced the Republic to organize a response. If the Empire has truly returned, it seems a war is inevitable.


### Master Sola Tane - The Veteran
**Species:** Human - F  
**Role:** Consular - Elder  
**Class:** Sage / Makashi Duelist / Peacekeeper  
**Weapon:** Saber  
**Personality:** Calm, forceful, scarred.  
**Summary:** She is the longest standing member of the team and served as the Master to Uray. Fair, evenhanded, and not easily swayed, Master Tane is highly regarded as a diplomat and negotiator. Warfare, in turn, is an unavoidable extension of diplomacy, and Tane has no problem fighting when the time comes. Favours a decisive strike to end conflict.  
**History:** Raised into the Jedi ways, she has been a warrior and ambassador her whole life, as was her own master, Ur'quoy. Fought in the Daimar war for twelve years to safeguard Republic citizens against the threat of autocratic rule. After a lifetime as a Knight and soldier, Tane seeks an end to conflict.  
**Team:** Uray was his last student, and although she is a Knight now, Sola appreciates the companionship. The rest of the team was hand-chosen by him, and while they are from a different age than she is, can do more good on the lawless fringes than she ever could by himself. And besides, there is still much she can teach them--Seela, the eager young warrior, reminds Tane of herself a lifetime ago.   
**Secret:** *Chaos* During the war, Sola killed a surrendering Daimian prince to try and force an end to the conflict. Instead, talks collapsed and the war raged for another seven years.  
**Motivation:** *Duty* Sola fights because she must. She is a soldier, and it is her duty to die so that others can know peace.  

### Uray Madak - The Guardian
**Species:** Pantoran - M  
**Role:** Guardian - Student  
**Class:** Protector / Soresu Defender / Colossus  
**Weapon:** Saber  
**Personality:** Empathetic, trusting, fearful  
**Summary:** He is the student of Master Tane, but became a ranked Knight one year ago. While not the most acclaimed student, Uray has proven his resolve on many occasions, and is passionate about protecting others. Extremely empathetic, he has no concerns about risking himself for the sake of others. He fears harm befalling others, or his sister, Ara, far more than he fears for himself.
Relatively recent inductee to the Jedi Order alongside his sister. Empathetic, and does everything in his power to protect others.  
**History:** A relatively recent inductee to the order, Uray grew up on streets of Pantora with sister, Ara. He sacrificed much over the years for her, and would gladly do so again. They were found by passing Jedi envoys seeking force-sensitives who discovered Uray and Ada's deep connection through the force. Although they have seen each other little over the years of training, they can still sense one-another.  
**Team:** Even though he has completed his studentship under Master Tane, Uray has spent most of his time as a Knight continuing to travel with her. He would not be allowed to work on the Rim by himself, but with his old Master's team he is able to reach those most alone and most in danger. Telera in particular has a talent for finding the criminals and tyrants that make the Rim as dangerous as it is.  
**Secret:** *Emotion* Fear for his sister's well-being has weighed heavily on Uray's heart, and he worries she will not make the rank of 'Jedi'. He worries even more for the dangers she will face once she does.  
**Motivation:** *Selflessness* Uray for no-one to endure what he and Ara suffered. Nothing can undo what he experienced and the damage it did, but he can save others.  

### Seela Poy (Dancing wings) - The Soldier
**Species:** Twi'lek - F  
**Role:** Warrior - Pilot  
**Class:** Starfighter Ace / Niman Disciple (Jar-kain) / Advisor  
**Weapon:** Dual sabers  
**Personality:** Driven, studious, mistrusting  
**Summary:** Though she is the youngest of the Jedi, Seela very quickly reached the rank of Knight. By all impressions she is a prodigy in space and star-fighter combat, but she has yet to be truely tested it battle. Her background has made her silver-tounged and graceful, operating comfortably on both sides of the war-table. She is expected to make an excellent commander one day.  
**History:** Born as the daughter of a court vizier in the courts of Cora, she was raised to become an advisor to the nobility. Though she lived comfortably, she knew others were less fortunate. The rigid restrictions on Twi'lek property ownership meant little chance for advancement and most were destitute their entire lives. A Jedi visiting the court inducted her into the Order. As a student she moved quickly to the top of the class, and despite dedicating herself to her studies she never forgot the cities of Cora.  
**Team:** Seela quickly found herself at the limit of what she could learn in the Core-ward temples and libraries of the Jedi. When she heard Master Sola Tane was planning on travelling the Outer Rim, Seela jumped on the opportunity. As a warrior and diplomat, there is much she can learn from Sola, though she wishes there were more chances to apply what she has learned more... directly.
**Secret:** *Passion* Seela plans to one-day to return to her home and use her powers and influence to guide her people. The Twi'lek suffer greatly through the galaxy, and she will liberate them by force if neccessary.  
**Motivation:** *Legacy* She works to make sure the mark of the Jedi on the galaxy is indelible. Through her and the other Jedi's efforts, all peoples of the Republic and future generations on every world will thrive.  

### Telera - The Investigator
**Species:** Mikkian - F  
**Role:** Sentinel - Shadow  
**Class:** Shadow / Shien Expert / Sentry  
**Weapon:** Double-bladed saber  
**Personality:** Quiet, cheerful, uncompromising  
**Summary:** Unusual for a Jedi, Telera joined the Order as an adult. Her skills with the force may be weaker than some, but her breadth of knowledge outside the typical teachings have served her well. Clever and unconventional, there are some that might doubt her abilities as a Jedi but few that have ever worked with her. She stays grounded, and often reminds other Jedi of who they are here to serve.
**History:** Telera began her career as a Republic civil security officer, and made a name in the towers of Coronet City for her uncanny ability to solve the unsolvable. When her investigation pursuing an off-world weapon-smuggling ring brought her into contact with Jedi investigators, Telera came to realize her connection to the force. She received a special dispensation to test her aptitude as an adult, and she began her unusual training shortly thereafter.  
**Team:** After completing a brief Studentship and quickly moving to Knighthood, Master Tane approached Telera to invite her to travel the Rim. Telera had always concidered the Core Worlds her home, but Urays stories of the wrongs they had faced out there eventually swayed her. The Rim may be uncivilized, but it is undeniable the good they have done for all those they have met. Besides, for all their Jedi training, there are lots of things the others were never prepared for. How did they get this far without ever learning to shoot a blaster or pick a lock?  
**Secret:** *Ignorance* Telera has little use for most Jedi teachings, as sees them as a means to an end. She worries that others will discover her weak connection to the force, and underdeveloped knowledge of the Jedi religion.  
**Motivation:** *Injustice* Opression and cruelty are found on every planet in the galaxy, Telera now has the power and resources to do something about it.  

#### Crawl
KANTHI, an ancient planet lost to time. Four Jedi Knights have travelled beyond the edge of known space to reach it, seeking a reprieve from the strife of the galaxy.

Young URAY MADAK eagerly awaits his sister ARA's return from her Padawan trials, while Knights TELERA and SEELA POY are enjoying the moments of silent contemplation. Master SOLA TANE, however, worries about the specter of war that looms.

As they watch the events unfold, a lone messenger speeds towards the planet carrying a dreadful message...

## Temple life

### Alssaea Temple

> **Alssaea - The Hourglass Temple**  
> Population of 200  
>  * 90 Jedi
>  * 90 Civilians
>  * 20 Republic Military

Built of gray sandstone
* Entrance
  * *Neat rows of celto, charbote, and balka-greens growing out front*
* Visitor's retreat
  * Guest rooms
  * Meditation chambers - Overlooking the river valley
  * Study
* Temple square
* Meditation chambers - Open-sky, with large, round crystal in the centre
  * *Legend that the crystal was there before the temple was built*
* Housing wing
* Kitchen/Mess
* Garden
  * Small pool
* Maintenance
  * Machine room
  * Computer core
* Training room

> **Master Tak - M Arcona Temple-master**  
> Altruistic, poised, secretive  
> Heterochromic blue/purple, green skin  
> Helped build the temple, and has been its guardian for thirty years since  
> Shamed during the Daimar war, plans to never return to the core

> **Vira - F Chadra-fan Temple-keeper**  
> Supportive, humble, fearful  
> Swirling tattoos down arms and on hands  
> Helps to maintain and cook for the temple, well known and liked  
> Sometimes sleeps in the meditation area and dreams of the crystal  

> **Nebtr'lia and Nabri'han - F & M Jedi Couple**  
> Cooperative, outgoing, needy  
> Sage and Knight  
> Longstanding temple-members, help training padawans and leading meditations   
> Have forseen their death together on Kanthi, accept it and have not told anyone  

> **Hubut - Frontier hamlet**

> **Sgt. Soren Flamebringer - M Human Republic Sergent**  
> Driven, self-assured, ruthless  
> Crystal-blue eyes  
> Dispatched here 6 months ago, more remote than he is used to, but appreciates the time away  
> Has an illigitimate family on Coruscant  

> **Lt. Riyal Parryn - F Human Republic Commando**  
> Bold, courageous, impolite  
> Braced left arm - broken during a mishap last patrol  
> Stationed here 20 months ago, reminds her of Sluis Van but misses it, committed to the Republic  
> Will never forgive the Empire for the sacking of Sluis Van  

### Daily meditation
Link back to main characters

### Distant war
The atmosphere in the canteen is unusually quiet, despite being quite full. There is a stillness as everyone sits turned towards the holo-projector on the wall.

The holo is replaying images of the shipyard planet Sluis Van, it's massive orbital dockyard rings illuminated against the night-side of the planet. As the camera pans, a massive battlefleet of alien ships are seen closing into orbit. Swarms of fighters begin pour out of the battleships and cruisers, and turbolasers begin to streak towards the planet.

The video cuts to the feed from a lunar base. The dockyard ring is breaking apart, flames streaming from the battered stations as they detatch and deorbit. Blinding flashes briefly overload the holo-recorder as smaller pieces are destroyed by internal explosions. Half-completed starships can be seen disintigrating during re-entry.

The holo cuts again to newsreel footage from the surface. Vanna and Bima City were completely destroyed in the calamity, reduced to craters and burried under a trillion tonnes of debris. The death-toll on the rings was tens of millions, and on the planet billions.

The news feed pans up as assault shuttles punch through the smoke and clouds of ash. Static displays on the projector for a moment before the holo replays.

Sluis Van has fallen.


## Exploration
Already seen the holos from Sluis Van, the Order is preparing for war. It is not safe to stay here so far from the core, so they have been ordered to evacuate within the week. 

Two ships appeared earlier today and entered the atmosphere, landing some distance away, you are needed to investigate at Wahid and Dhakira Falls.

In addition retrieve the Padawans from the Cave of Echoes.

Unable to fly there because of the interference from the cave, and the winds along the cliff.

### Kanthi
> **Kanthi - The planet lost to time** 
> Distant world, untouched for millenia
> Inhabitted by Jedi in Old Republic, current home to Jedi fugitive
> Crystals from this planet are linked to time, found in a cave where a meteorite impacted
> Cataclysm millions of years ago on the impact event has caused its current state

* At night, the constellations are fixed
* Alssaea - Remote Jedi Enclave  
* Daiye - Region between the two mountain ranges (Metrak, Sinda), down to the sea (Rhu)
* Hubut - Clear valley where the temple and town have been built along the river Kasa
* Metrak mountains - Range where the meteorite impacted, unreachable by flight or commlink due to mysterious force
* The Daiye Forest - Endless forest of green
* The Dhakira Pools - Tranquil lakes overlooking the forest
* Dhakira Falls - Feeds the river Kasa, no apparent source
* The Stained Cliffs - White chalk with bright streaks of hematite
* The Crumbling Path - Road to CoE which was there before colonists, never requires maintenance
* Cave of Echoes - Impact crater from an otherworldly meteorite
* Wahid - Lonely meadow in the midst of a forest, prismatic flowers bloom
* Hands of Shaa - Rough stone outcroppings visible over the tops of the trees
* Barren hills - Constant strong winds wiped the hills clean

### Dire message
Called back by Master Tak. Situation is urgent and they are needed immediately. Republic troopers pick them up on a Rendaran shuttle

### Encroaching war
Approached by a Centurion battle-cruiser Vehement (http://starwars.wikia.com/wiki/Centurion-class_battlecruiser)
Scrambling an escape


## Vanguard

> **Republic detachment**  
> 1x Republic Sergent  
> 14xRepublic Trooper  
> 4x FT-8 Star Guard - http://starwars.wikia.com/wiki/FT-8_Star_Guard  EotE Core pp258
> 2x Rendaran shuttle - http://starwars.wikia.com/wiki/Rendaran-class_assault_shuttle - AoR Core pp273

### On wings of night
Suggests closing in stealthily.
Swarms of Sith fighters (http://starwars.wikia.com/wiki/Sith_fighter)

### Assault

#### Foes
* Squad of troopers and droids
* Bulkhead breach
* Seal secrity doors
* Droid ambush
* 

### Sith
Meet the Sith Lord on the bridge of the Vehement.

> **Lord Kha Starskimmer - F Miraluka Sith Lord**  
> Self-possessed, guileful, dominant  
> Gray skin, black hair  
> Wears light plate armour, with rippling black robes, simple black blindfold across eyes.  
> Seeking the rumoured artefact on the planet  
> Working to become a member of the Dark Council  
> Appreciates having an easily controlled Apprentice  

> **Tal'akoa - M Twilek Sith Knight**  
> Driven, self-assured, ruthless  
> Green skin, heavily scarred face  
> Heavy Sith armour, dark gray robes  
> Former slave, become an acolyte  
> Easily controlled by Lord Starskimmer  

Attempts to convince **Uray** to join them and save his sister, else everyone will be killed.


## Desperation
Lord Starskimmer has ordered the crew to fire on the temple once in range. If she is at risk, she orders the ship to be crashed into the temple while she escapes onboard a shuttle.