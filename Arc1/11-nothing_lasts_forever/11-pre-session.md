# Asori - The cave

## Into the cave

You are jolted back to consciousness by a hot pain down your left arm--you remember taking a blaster round during the fight. Probably broken, but not too badly. 

You try to grit your teeth as your arms are yanked on again, dragging you out of the back of the speeder. They pull you backwards through the rocky crags, your heels leaving a small trail in the gravel. The sun sits low, barely visible between the peaks. 

You struggle to control your thoughts, you need to fight back, to make them pay... but your mind slips away again. 

Cowards.

They pause for a minute, and you can hear a metallic grinding behind you. As they pull you through an ancient bunker door the musty air smells of rust and decay. One of them swings the door closed, shutting out the thin, yellow beam of sunlight. 


You cannot remember the questions they asked you--or if they even asked questions. All you remember is the darkness.


## Flickering Embers

### Awaken

You huddle close to the flickering campfire. The smoke stings your eyes and nose, but it is all you have against the biting cold. Without it you would freeze--or worse. You will need it to last until morning, but that is hours away yet. 

Beyond the orange glow of the fire is an inky, moonless night pressing against your dim sanctuary.

> Perception ⬡♢

Perception: Result 2 Success, 1 Threat.
Combined Discipline/Ebb Check: 3 Success, 3 White Pips.

    Asori would like to attempt to gather her focus while simultaneously returning energy to her body as she has trained herself, she is seeking the strength to understand, strike, and escape this situation. The above roll is the result of that attempt, although I have not rolled any difficulty (successes equate to the discipline check, and the white pips equate to Asori recovering 2 strain).  If you could roll the difficulty that would be great. Note: For fear checks, Asori downgrades the difficulty by one.

### Tenebrous approaches

#####
> Discipline result (vs. ⬡♢◻) -> 1 Success, 2 threat
> Light-side force pips remove the effects of threat from the Perception and Discipline checks
#####

You sit and focus, channeling your inner strength. You have endured far worse before, and a well disciplined mind is all you need to survive. The fire seems to burn a bit brighter and the chill leaves your skin.You can pass the hours in meditation without being bothered by the cold and the darkness.

To your left, you sense a presence... foreign, intrusive. Opening your eyes, you see two dark-red eyes flickering in the firelight. A great scaly creature sits just outside of the fire's glow--the basilisk Tenebrous. The fire flares again, and the creature recoils slightly, stepping back into the darkness.

Unmoving, you hear it's low voice resonating inside of you. "You should preserve your strength, Asori, dawn is yet a fleeting dream. Do not delude yourself that you have faced everything the dark might bring to bear. Your light may seem bright, but the void is infinite. A storm draws near, you must endure."

It remains motionless just at the edge of the light, its gray-mottled scales still as the stone around you. A cold wind causes you to shiver and the flame to dance brightly.

#####
> Make a hard upgraded (⬡♢♢) Resilience check 
> Add two boost for the successful Discipline/Flow (?) check
#####

> Resilience ⬡♢♢

	OH BABY! Scraped through.

	Resilience result roll: 1 Success.

	If I can talk, can I say the following: "You of darkness, your behaviour a perversion of destiny, if you have nothing to fear, and you speak in earnest, show yourself in the light."

### Wind picks up

"Do you not cling to the warmth of the flame? Why do you not step forth and succumb to the darkness? I am born of the shade and your light would consume me as surely as the darkness would swallow you."

"You think me your enemy, one of the countless maws of the infinite darkness. A shadowy agent manipulating you to your own destruction. You are mistaken. I am not part of the ever-hungry void... and I do not wish to see you smothered."

"But, look now how your flame roars! A lesser being would be broken in the tempest, in the torment, but not you--your light is enduring. If you draw strength from your hatred of me, so be it. For now, it is your survival that matters. Your dawn is distant-still. remember that your mind, like your body, is not unbreakable."

"I will never be far in your time of need, know that my stength is yours should you need it."

The creature recedes into the darkness, out of sight, though you can still sense its presence in your mind.

---

A low crash of thunder overhead breaks through the howling winds. The light of your fire dances eagerly, as though goading the storm to do worse. 

Even still, the icy winds claw at your skin, clouding your mind.

You focus on your meditations, drawing forth your strength. You will not lose yourself.

#####
> Make a hard, twice upgraded (⬡⬡♢) Discipline check
> You may make an ebb/flow check with this if you wish
#####

> Discipline ⬡⬡♢

> 2 Success, 2 threat, 1 Despair – 2 light pips, 1 dark side pip.

If ebb/flow is standard recover strain check I will only use light side pips. If it is something else, she would use the dark pips. 

I want to reply “You speak with soft words, yet you fail to aid me? If it is my trust you want you must earn it! And know this, it is not hatred I feel toward those whom manipulate through the shadows but disgust.” Pause. “I have lived as a Druma’san and I will die as such if I must, but this is not my place.... I will not be undone by cowards... Even if I fall, my brethren will come... They will come... I’m sure of it... Aid me, or leave my sight and allow me to focus!”


### Rain

Stinging rain showers you from behind, icy needles chilling you to your core--the flame dances violently. Your arms and hands begin to numb from the wind. The cold rain soaks your robes and draws away your strength.

---

"Hm..? Trust?"

You see Tenebrous standing opposite you at the edge of the firelight. The flickering flame is reflected in its glassy eyes. 

"Why should you wish to trust me? What would you have me do? Pluck you from this nightmare, and return you to the comfort of your friends' embrace? My powers are not so... blunt... as to remove you from the present circumstance. You will endure, and you will survive."

Slowly, the great serpent moves around the fire, keeping its distance while holding you locked in one jet-black eye.

"Still... the night is long, and you have much to bear before dawn."

The massive serpent crosses behind you, great scales dragging along the stone. Like a great boulder settling it stops with its massive body half-encircling you--sheltering you from the worst of the wind and the rain. 

"Rest. Preserve your strength. Reflect on your trials... and the lessons you can learn."

---

#####
> Make a formidable, twice-upgraded (⬡⬡♢♢) Resilience check
> You are assisted by Tenebrous with a skill of 4/4
> You have three boost from your Flow check
> You have two setback from the freezing rain
#####

## Dawn-break

> Roll result: Fail neutral, 6 advantage.

Note: I rolled the assist as if Tenebrous was rolling 4 yellows, it transformed my roll from 2 green to 2 yellow and 2 green.

	Asori would like to position herself so that her eyes are locked with the dark creature Tenebrous and sit in meditation as the Druma-san taught her, showing no signs of retreat or weakness; an act of stubborn defiance against the storm and the creature in front of her. She wishes to sit in a strong cross legged mediation posture, muscles tensed, presenting herself as a dangerous and confidant foe if attacked; a Jakku beast in a corner. This all despite her currently poor state.

	She says to Tenebrous "... Your truth, it will be known to be someday. But... until that time, you have my thanks creature of the dark."


---

You wake to the warmth of the sun on your face, a distant dawn rises into a clear sky. The fire still burns in defiance of the storm. 

The creature Tennebrous is gone, though you can still see the scuffs where it dragged itself along the stones around the camp. 

It is time to carry on.

---

Your time meditating has allowed you to better understand the movement of the force
> You may re-spend 10 xp from the Enhance -> Ebb/Flow

The trials you have endured have strengthened your resolve
> You receive 5xp for the Resilience skill

The darkness takes its toll
> Lose 2 Morality

You are connected to a strength not your own
> Twilight blessing - Once per session, may flip the colour of one force pip as part of a force check (dark -> light or light -> dark)


# Baldyr

> Move to after next session

You are dreaming.

It has been months since you were last on Thila, but the vibrant jungle comes to you every night.

Sitting on a stone, you watch as the first dawn rises above the mountains. Rich purples and deep greens colour the valley as the light sweeps across it, bringing the jungle to life. Nearby, a cluster of azure flowers begin to slowly open. The crisp, damp smells of early morning give way to sweetness and warmth. The dark gray stone beneath you begins to warm under the orange-red light of the first sun. Birdsong, bright and cheerful, replaces the quiet trickling of the nearby stream. A soft rustle in the undergrowth draws your attention to a small slitherette emerging from its den.

You glimpse a solitary bird sitting on a nearby branch, preening its yellow-gold feathers.

It is... unfamiliar. 

Three long, ornate feathers crown the top of its slender head, bobbing slightly as it runs its long beak through the feathers of one wing. It stops, staring at you through a piercing blue eye, calm and still in the vibrant jungle. 

A voice echoes in your mind, like a ringing crystal: "Baldyr."

"I am Eos, the spirit of the Druma'san. You are not of our world, but your soul is one with ours--a child of the great tribe."

It tilts its head quickly to the side, as though listening. The jungle is silent now. Your mind is filled only with Eos' voice.

"You are close with our destined child, Asori. She is strong in ways you do not understand, but you must help her."

Pausing again, it preens the feathers along its wing before looking back to you.

"She is fated to save the Druma'san, no other can do so. She must help us. You must help her." 

It listens again to the silent valley.

"A shadow stalks her heart, crawling into her mind, twisting her fate. I cannot reach Asori, but I can reach you, Baldyr. Free her from the twilight."

Eos gently alights to a lower branch, just out of your reach.

"I will give you a glimpse of my powers. Use them to aid Asori. Use them to free Asori. Reach out and I will help you."

You sit transfixed by its golden form, shimmering brightly in the morning suns.

"Use my strength, Baldyr, but do not speak to Asori about me. The beast's claws are tight around her heart, rending claws... killing claws."

The ringing in your mind is louder now, but the voice remains clear.

"Walk her back to the light, Baldyr. Bring her back to fate's embrace."

Silently, the bird stretches its wings. With a graceful sweep it pulls into the sky, turning towards the dawn. You lose sight of it over the lush valley.

**_Dawn blessing_** - *Once per session you may add one light-side force pip to your force pool instead of rolling force-dice.*
* *You may purchase force talents despite a force-rating of 0.*
* *You may not purchase force powers, but you may attempt to use force powers untrained.*

## Investigate with the Druma'san handbook
> Fail, two threat

After the dream, you spend some time searching through the Druma'san codex for mentions of Eos or other deity, but you find nothing referencing it. You cannot recall anything like it in your teachings, either.

Still... it was warm, and comforting... and familiar.


# Ha'am
Your dreams have been troubled since the incident with Sar and the crystals. Visions of the fight, and the darkness, and Sar spring forward. Even when awake you have difficulty speaking to him--the growths on his arms and face remind you of that grim night. Darkness and evil are no strangers to you, but this was something else.

Sar has been understandably distant since then, and you do not know how to bridge the gap. He keeps to himself even more than before and refuses to speak of what he endured.

---

Tonight you are in the cave again. You can sense your way through despite it being completely void of light, and you move to the central cavern where you found him. You step around the pillar, but Sar is not there--another presence is in his place. As it speaks, you recognize its form: The Serpent, Tenebrous.

"Ha'am Shatira, Guardian, Healer... you have spent much time of late here, in the darkness. You must take care, it is easy for one with your form to become lost or consumed."

You can barely make out the beast's enormous form laid across the stone, but its two dark-red eyes shine even here. 

"You have seen, and survived, what few ever do. The hunger that sleeps in the deepest darkness. The primal drive that would swallow up all of the light you seek to protect. Unreasoning, uncaring, beyond understanding... far removed from the evils you are accustomed to. Without malice it will destroy everything you love and protect."

The beast pauses and raises its head gradually, deliberately, to your height.

"Why do you come here? To fight it? To understand it? To use it? If you met the darkness again, would you fare any better? Could you save anyone?"

The beast's voice is more forceful, authorative, commanding.

"Return to your light, bask in it, remind yourself why you venture here in the deep. Do not lose yourself--you will help no-one if you allow yourself to be consumed."

Tenebrous lowers itself back to the crystaline floor and turns, moving slowly off into the cave. It stops and looks back with a single glowering eye.

"If you wish to understand the darkness and learn to fight it, to heal it, return to me when you are ready. I will teach you of the voracious shadows."

It turns back and leaves you alone in the empty cave.

*You receive 15xp from your exposure to the darkness (missed session experience)*

*Make a morality check with 7 conflict and adjust your morality accordingly*

*If you accept its help, gain:*  
**Twilight blessing** - *Once per session when you make a force check, you may flip the colour of one force pip that was rolled (dark -> light or light -> dark)*

# Deep space

## Departure 

With a hold full of provisions, you set course for the outer rim. The path through the A'Karra nebula winds through two dozen systems to avoid the worst of the clouds and despite the relatively short distance it is expected to take more than two weeks to traverse. Even with the ISC's data the trip will require constant monitoring of the navicomp: The last pathfinding expedition through the sector was thirty five years ago, shifting mass shadows and uncharted anomalies mean the trip will be risky. Still, this is an opportunity you cannot miss.

There is an energy and anticipation on the Golden Noble. The mysteries of Kanthi--and perhaps answers to why you were drawn there--are finally within reach.