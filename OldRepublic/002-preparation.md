# Commando

## Crawl
It has been two months since the attack on SLUIS VAN and the undisputable return of the SITH EMPIRE. Despite the devastation, the REPUBLIC is unwilling to enter an open war.

The JEDI ORDER, however, recognizes their ancient foes and is mobilizing already. The TINGEL ARM has quietly fallen to the Sith and the Order is making plans for their counterattack.

SHEVA-MI is the largest orveth gas refinery in the sector and a strategic target for both sides, and the Order will not allow the Sith to hold it. A team is being assembled, both veterans and recruits, to strike back...


## Summary

* Commando operations on the Sheva-Mi platform to undermine Sith foothold in the sector
	* Most of the Tingel arm seems to have fallen under Sith control without major conflict
	* Sheva-Mi in the Lo'Uran system is an Orveth gas extraction and refining platform
		* Operational for 34 years now
		* Population of ~ 200 000
		* Operated by ~ 100 000 droids
* Republic still scrambling to establish a defence
	* Unsure if they should fight or negotiate
* Jedi moving quickly to assemble fleets
	* Want to undermine the Sith to more easily push them back into the wilds

## Dramatis Personae

> **Voren Aongo - Human Governor of Sheva-Mi Platform**
> Strong willed, judicious, impatient
> Black hair, left cyber-eye
> Orange, poorly cut, business attire
> Was placed due to likely Sith sympathy, quickly turned over control during the invasion

> **Tensi Mellian - Human Captain of the _Destiny of Mahi_**
> Forceful, resolute, dismissive
> Bright white hair, tight-fitting gray robes
> An outcast, as most Jedi captains are

> **Dallo Lyaamis - Human Leader of Krayt Squad**
> Thoughtful, outgoing, needy
> Scars on the left of his face, brown robes
> Recruited shortly after the Jedi Purges
> Squad: M Gotal, F Kel'dor, F Human

> **Souhik'kairn - Twi'lek Leader of Runyip Squad**
> Serious, realistic, insensitive
> Lost right arm in the Daimar War
> Refuses to stop fighting, despite injuries
> Squad: M Human - Yhar, M Sakiyan, M Human

> **Sori Ouron - Duros Leader of Fynock**
> Spirited, diplomatic, foolish
> Blind in left eye
> Younger, but a prodigy. Studied on Coruscant.
> Squad: M Trandoshan


## Operations
* Multiple squads of Jedi being dropped on the platform 
	* Arriving on a Jedi Destroyer *Destiny of Mahi*
	* Being dropped 200 km out in a dense storm
	* Travel to the city on Aratech 7B airspeeders
		* Land on the outskirts in low-traffic areas
		* Unlikely to be detected below the Orvethosphere
		* Encounter a group of Beldons

* Extract the City governor from Vilga
	* Krayt Squad
	* Governor Voren and their staff are located in the Governor's mansion
	* High-priority, as the Jedi want to earn good-will with the Republic Senate

* Additionally, they want the governor to speak to the attrocities of the Sith
	* Limited support from Republic High-command for this portion
		* Providing a fast courier with encrypted diplomatic codes to get the gov. back
		* Gov. not to be transported in a Jedi vessel - the Jedi are at war, not Rep.
		* No non-diplomatic staff in the shuttle
	* Krayt reaches the gov., but are ambushed by Sith accolytes - killed, 1 captured
* Destroy the refinery
	* Purrgil Squad
	* Capable of extracting and processing over 400 000 m^3 per galactic day
		* Significant asset for operating an army
		* Sent to disable or destroy the facility
	* Provided with Thermite-Baradium charges to demolish the facility
* Sabotage the Polyphemus orbital battery
	* Runyip Squad
	* Array of 24 high-output turbolaser batteries
		* Designed to defend against approaches from orbit
	* Targetting the reactor and orveth supply lines
	* No defence on approach, but reactor door sealed with a hardlock
		* Ambushed while trying to gain entrance
		* Only 2 escaped
* Steal documents from the Perchta Garrison
	* Fyrnock Squad
	* Local garrison had documents on local fleet movements and capabilities
		* Local fleets were no longer responding
		* Order interested in the intel, as well as denying the Sith
	* Room sealed, gassed, entire squad killed

## Sheva-Mi Refinery
* Operated by local civilians and droids, roughly 20 000 sentients and 40 000 droids
* Local security are present
* Sith are prepared for the attack
    * Placed a radio jammer in the facility
		* Employees are complaining about non-functional radios
	* Sith response squad are located in the command centre, and will rapidly respond if the squad is made
	* Can sense the attack before it happens
	* 30 Sith troopers
	*  6 Sith attack droids
	*  4 Sith acolytes


## Starskimmer
Starskimmer's battlecruiser, the Vehement, waits in orbit for resupply and tips off the Sith authorities on the Jedi's arrival. It has been repaired and re-equiped since the last time they saw it.

* *The Destiny of Mahi* may detect it on approach
	* Moored 40 000 km above the platform
* Starskimmer will stay on her ship, near the Orb of Kanthi, and may draw new insight from it
* The ship is well defended, with hundreds of troopers, tens of acolytes

## Disconnect
No contact with other squads, do not regroup at the *Destiny of Mahi*
* Runyip eventually reaches the beach-head, they met excessive resistance and fell back, managed to kill their pursuers at the cost of two of their team
* Krayt still unaccounted for, no sign at the diplomatic shuttle

## Crush
* *Destiny of Mahi* is missing, destroyed by Sith fighters and plunged into the gas depths
* If the diplomatic shuttle launches, the Vehement will fire upon it

