# People

## Crew

### Matthias We - The Forgotten
> Cold, disheartened, generous
. Raised from a young age to become a Jedi, Mathias trained but showed a poor aptitude for the force. After failing the Padawan   Trials three times, he chose to enter the Agricultural Corps.
. Was a close friend of Master Zyn, and fled to Kanthi with him when Order 66 came. They crashed, and Zyn was killed.
. Mathias took his sword, and has survived here for over 30 years.
* 003-005: Has lost his memory of self, and of history. Witnessed the suffering of this galaxy. Told he is a Jedi. Has chosen to train with B, to recover his memories or help those in need.

### Saar - Hoodlum in Over His Head
* 003-005: Witnessed the effects of the NightSister spell, stealing their memories. Can no longer take the pressure of losing H to an unseen, indefensible enemy.

### Han 'Skib' Garalyn - Uplifted Simpleton


## Independent

### Dr. Ashur Sungazer - Wayward Academic

### Daro Tillie - Shrewed Criminal Fixer

### 'Badger' - Hard-scrabble Independent Businessperson

### Val-Isa - Artefact Soul

### Reom - Principaled Isotech Boss

### Norta - Isotech Second

### Kodo - Friendly Buffoon


## Alliance

### Deneb - Free-spirited Rebel

### Jeela Ajin - Unwitting Force-sensitive Mechanic

### 23rd irregulars "Deaf Bandits"
#### Aema Starbringer
	* Leader
	* Dedicated, neat, self-righteous
	* Copper hair, silver eyes
		* Characteristic of Cardotans
	* Comes from a noble family
	* Modified DH-17C blaster carbine
	* Composite armour

#### Nejilin "Nej" Hillac
	* Slicer
	* Practical, nervous
	* Cyan hair tied back
	* Once part of Ha'am's orphans
	* BAW E5 blaster carbine
	* Flack-jacket
	* Buddies with Sar

#### Khrusk "Slug" (Trandoshan)
	* Pilot
	* Amiable, gullible
	* Scar over left eye
	* Once worked for the Hutts
	* HH-50 Heavy Blaster Pistol
	* Flight suit
	* Bonded with Asori

#### Jace Carte
	* Face
	* Loyal, unrealistic
	* Slicked back short brown hair
	* Brother disappeared to Imperial camp years ago
	* Blastech HSB-200 Holdout blaster
	* Silver Armoured Suit


## Imperial

### Cypher 9 - The Assasin

### Tycho Sunfell - The Inquisitor
> Calculating, wrothful, creative
. Born on devastated Humbardine, witnessed the suffering left by the Clone Wars during the separatist assault. Driven by Anger, and to ensure this can never happen again.
. Intends to make the Empire strong, and safe. To ensure no calamity like the war happens again.
. Has no love for the Inquisition, but remains a member to continue removing subversive elements.

### Eren Garai - Disgraced ISB Operator

### Hesh Oetho - Weak Imperial Administrator
> Duplicitous, weak-willed, craven
* Never produced anything of worth
* Attempted to blackmail a Moff, but failed
* Assigned to manage Mos Shuta after the last incident
* Looking for a way up


## Consortium

### Tyber Zann
* Works with lead NS to collect slaves and export NS

### Alloy - Broker of Lives and Information
* Fronts as a modified IG-86 droid
* Began existance as a 2-1B surgical droid
  * Has since integrated a multitude of other droids into an extended central processing matrix
* Operates as an information broker, selling secrets and patterns to anyone with credits
* Seems to operate on several worlds, with no clear base of operations
* Uses its extensive databanks to collect and index data feeds from across the galaxy
* Bought into the Consortium several cycles ago by handing over priceless secrets about the Hutts
  * Now central to the Consortium's intelligence arm
* Has begun procuring droids and droid parts from the bunkers on Hypori
  * Producing new lines of upgraded battledroids based on CIS models
  * Supplying battledroids to the Consortium for shipboard and mercenary operations

### Uema Duskrover - Consortium Gatekeeper
> Attentive, glib, 
* Serves as TPI Consolidated VP of Mercenary Relations
  * Front for the consortium
* Responsible for checking on potential crew before passing on
  * Assisted by Sister Terrano
* Former bounty hunter
  * Has personal commendations from House Tresario

### Sister Terrano - Reluctant Assistant
        * Human
        * Somewhat young, long, braided brown hair
        * Social, civil, thick-skinned
		
### Sister Vancil - Rising Apprentice
        * Dathomirian
        * Young, short orange hair, pale gray skin
        * Patient, hard-working, nervous



## Removed

### Commander Ironarm - Fierce Jedi Hunter

### Zyn - Buried Jedi Master
> Impulsive, quick-thinking, craven
. Childhood friend of Mathias, Master Zyn was well suited to the force. His studies on the Jedi were lacking, but Zyn still became a Knight at a reasonable age, and was given the title of Master after heroics in the Clone Wars.
. Led daring missions targetting rear facilities, and with his crew of troopers quickly established a reputation in the Republic armies.
. Warned of the impending Order by his squad, he fled to Kanthi with his friend Mathias. Believed to have been killed in the crash on Kanthi.

### Dr. Adneris - Researcher Out-of-time

### Heron Voss - Adventuring Archeologist

### Teemo - Populist Administrator


# Locations

## Planetary

### Askaj - Neglected Frontier-world
#### Mos-Shuta - Destitute Mining Town
. Barracks: 20 Local Security, former Teemo, now working for Kordes. ~60% recruitable. Run by Melyn Paramis
. Garrison: 30 CSA security [Card]
. Landing bay Aureck: HT-2200 Freighter [F-CRB: 264]
. Landing bay Besh: Gozanti Armoured transport [ND: 63]
. Stables: Present, but business ruined. Run by Jorgi.
. Cantina: Under new proprietorship, run by Menn Kirala. Frequented by Local Security
. Baba's Grill: One of few with old management (Baba). Frequented by CSA and sympathizers
. Jawa Traders: Have required part, cheap but broken
. Locals oppressed (~20% imp sympathizers)
. Teemo
. Kodo
. Jorgi: Runst stables. No interest in helping. Business ruined.
. Melyn Paramis: Human, former judge. Forceful/dismissive. Respects Teemo. Not risk taker.
. Menn Kirala: Human, quick wits/argumentative. Dislikes imp but doesn't make it known
. Baba: Old Twi'lek, imp sympathizer


### Soccorro - Secretive Criminal Hive
#### Port Hanja - Freeport on the Fringe
#### Temple of Light and Dark - Dark Secrets Stirring


## Deep-Space

### ISO-One - Shifting Corporate Headquarters

### The Wheel - Neutral Freeport


## Starship

### Argon Heavy - Deneb's Tramp Freighter
> Modified YT-2000

### Stalwart Auditor - Garai's Enforcer
> Imperial/ISB CR-70
