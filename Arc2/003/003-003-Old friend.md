## TODO
* Write intro to recovery

## Synopsis
* Recovering from NS attack
* Bond as a group
* Find a proctection against NS influence


# Elsewhere...
TO: Dr. Sungazer
FROM: K-17
SUBJECT: Rapidly deteriorating crew health

Dr. Sungazer,

You are aware that the Solar Fate is currently traveling to Sullust for shore leave and to meet with you. Unfortunately, the crew has suffered what they believe to be some sort of psychic attack, or perhaps a stress-induced hysteria. Their demeanour has changed, and some are reporting acute retrograde amnesia. Given the symptoms, I am investigating the possibility of a gas leak from the drive exciter coils.

They are well beyond my ability to treat, but you have a history with them. You first assembled them, directed them, and mentored them. There is no one better qualified to help them, within the distance I would feel safe traveling with them in their present state.

I hope you are able to help them. It would be difficult to find replacements for all of them at once.

>> K-17


# Scenes

## Recovery
* Establish how characters are feeling
* Cut to Ashur's home, K-17 has asked for help

"K-17 "


## Questions
### Ceneza
* Kept in the Blood Moors on Dathomir
* Not a NS by birth, stolen from another tribe

### Mother Movra
* Working for ZC
* Currently trying to unlock the secrets of the holocron

### NS Magic
* 

### Why Timm'eh?
* Survivor of the tribe which guarded the temple
* May know the secrets of unlocking it


## Protection
* They should find some way to make sure they are protected if they are being targeted
* How to protect against 

### Ashur
* Cult of Tund wrote about "Imperial Aurodium"
	* Tundian royalty wore armour and built structures of it to proctect themselves from spells of their adversaries
	* Some links to the Jedi Order
* Requires:
	* .5 tons of aurodium
		* Would cost over 100,000 cr. on the open market
	* Directions on creating it
		* The Baitha Slates
		* An ancient book bound in leather trimmed with Umbaran lynx fur
		* In the collections at the Sullustian Sector Academy
	* Forge
		* Melt the aurodium with Braccan Quartz
		* Form a protective seal or mesh

### Val-Isa
* Meditation will allow them to steel their minds
* Requires:
	* Group meditation
	* Building bonds together
		* Must build inner fortress
		* Shared dreams over the course of 4 days
	* Overcoming inner conflict
		* Shadows come on the 5th day

### Nasra
* Cast a protection spell to ward themselves
	* Based on Nightsister spells
* Requires:
	* Ingredients
		* Blackroot (boiled)
		* Salt
		* Marcan (Illegal on Sullust--euphoric effects)
			* Will cost upwards of 10k cr.
	* Lifeblood of Dathomir 
		* Anything with a deep connection to the world
		* eg. rancor blood
	* Ritual
		* Draw spell circle with boiled blackroot 
		* Grind the remaining blackroot and marcan, mix with salt and place in the circle
		* Speak the spell, meditating to draw forth power
		* Pour the lifeblood into the powder
		* Draw the protection runes around the rooms you wish to protect


# Additional detail
