## Lessons from the Past
* Wealth of knowledge
    * Form the group
    * Acquire Golden Noble
    * Friends with Ashur Sungazer
    * Make an enemy of Eren Garai
* Wreck of the Sanctuary
    * Acquire Holocron of Val Isa
    * Obtain Val Isa's talisman
* Hidden depths
    * Basic Jedi tests
    * Acquire kyber crystals
    * Defeat Cmdr. Ironarm

## Fledgeling Trials
* A Cry for Help
    * Ha'am called by Sar Bakom
    * Travel to Ilum, and complete Jedi Trials
    * Liberate Sar, who joins the crew
* Hunted by Darkness
    * Explore damaged Corellian Colonizer
    * Obtain the Healing Crystal of Fire
    * Asori gets possesed
* Tasks below the Jedi
    * Visit Mos Shuuta, form bonds with the locals
    * Befriend Lothcat
    * Defeat pirates

## Beyond the Rim
* Tale of Sal Nalador
    * Summoned to clear a debt for K-17 previous master
    * Visit The Wheel
    * Undertake work for the Isotech corporation to explore a starship on a distant world
* Part 2
    * Rescue Captain Harsol
    * Burn bridges with Cratala

# Arc 1

* Still Asleep
    * Dream of the world
    * Attacked on IsoOne
    * Uncover the Cypher Agent's plot
    * Flee the empire attack
    * Pulled to Soccoro

* Burried Sun
    * Investigate mysterious temple on Soccorro
    * Learn of light and dark
    * Meet the twilight

* Jackboot
    * Attacked by Ironarm, Eren Garai, and Cypher agent
    * Navid has left with Sar
    * Escape with help of Daro Tillie and Deneb
    * Introduced to the Rebellion

* Reunion/consequences
    * Return to Mos Shuuta
    * Taken over by Imperials thanks to Eren Garai
    * Fight with the Imps

* Fighting Back
    * Liberate Mos Shuuta, fighting Imperials and Cyper 9
    * Reunite with Navid
    * Drawn to Kanthi

* Prison Break
    * Ashur Sungazer was used to draw them out
    * Freed Sungazer, injured Cypher 9

* Midworld
    * Looking for a way onto Alidade -> need directions to Kanthi
    * Met Nasra and Morgana
    * Made an enemy of Alloy

* Rebellion
    * Daro Tillie introduces to the Lazy Bandits
    * Block the messages sent to the Empire -> known rebel sympathizers
    * Meet another Ha'am child

## A Brother in Need
* A Brother in Need
    * Meet Baldyr
    * Touched by mysterious Druma'san powers
    * Rescue Elder Me'lya

* Alidade
    * Board the station
    * Steal a route to Kanthi
    * Ironarm is killed by the inquisitor and Cypher 9

* Abandoned Weapons
    * Join with Baldyr
    * Stopoff to prepare and help Isotech
    * Extract valuables, but lose other teams

## Fight-club
* Fight-club
    * Discover Alloy is active on the planet
    * Save the lives of others

## The Shadow of Avarice
* The Shadow of Avarice
    * Discover Alloy is upset, and targetting them
    * Make allies with the corporation

* Nightsisters
    * Nasra sends to hunt Nightsisters on Elrood
    * Learn the danger they pose
    * Fail to stop them from destabilizing the planet
    * Kill one of the coven

* Nothing lasts forever
    * Travel far to Kanthi
    * Shot down, Golden Noble destroyed
    * Dream of the past
        * Find something strange

* Phoenix
    * Lose something of theirs
    * Navid leaves
    * Meet Mathias--a Jedi
    * Discover something missing
    * Resurect the Solar Kite
    * Fight the Inquisitor

# Arc 2

## Snowstorm in the stars
* Snowstorm in the stars
    * Meet Squib
    * Repairs the Solar Fate
    * Hate pirates
    * Meet another Ha'am child

* Liberation of Mos Shuuta (again)
    * Travel back to MS
    * Discover life has changed
    * Pay for its freedom--of sorts

* Return to Kriusia
    * Hired by TPI
    * Kill a Hutt
    * Destabilize the planet

* Phlegethon
    * Hired by ZC
    * Intimidated, but turn down
    * Uncompromising, get into a spat with the NS
    * Blast their way out, killing the second of the coven
    * Learn something about the consortium
    * Resolve to hunt NS

## Dreamscape
* Dreamscape
    * Attacked by Mother Malek
    * Learn of Ceneza
    * Lose something of themselves