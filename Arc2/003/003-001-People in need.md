"It looks like they slipped into the vents. Can't pin 'em down--these clankers can't keep up. They'll be heading for their ship, we'll cut them off." 

The hologram clicked off, leaving Commander Kandria's office dark. She looked out over the bustling city--the bustling warcamp. Thirty thousand souls aboard. Seris was outclassed, not something he was used to. 

Dangerous. Uncompromising. Idealogues. Duskrover caught two of the three. They probably knew too much before even getting on the station. 

Commander Kandria grabbed her rifle and headed for the lift.

---

She hardly noticed the cool wind in the landing corridor and the alarm seemed distant, muted.

Why was she doing this? Why pursue? Vengence? Bloodlust? Ideology?

The sting of cold metal on her cheek focused her as she raised the rifle to her shoulder.

No... duty. This was the commander's duty. No lose ends. The Consortium, her crew, her sisters, all depending on her.

The rifle's optics lit up, a sillhouette coming into focus.

This was her duty, and she wouldn't fail again.

She pulled the trigger.

---


## Mathias
* What are you doing out here? Are you helping?
* You've lost so much, why continue to fight

## Sar
* Are we making a difference
* 


## Nasra and Morigan
* Nasra contacts them
  * "Timm'eh, I am glad to hear you are still seeking answers, but caution you on how dangerous the Nightsisters are. We are working a job on Ventooine now, come meet us and maybe there is some help we can offer."
* Nasra is driving a Nerf herd across the desert to Harcey Docks
  * Number about 1000
  * Travelling with pair of Ithorians
	* Mala and Huli
  * Riding dewbacks (11)
  * 3 more days to reach their destination
  * Worried about sand-people attacks
* Morgana waitnig in Harcey Docks
  * Has visions of a great terror, death, emptiness
  * Also, tides of fate becoming tumultuous
  * Sickly, will be seeking medical attention in mid-worlds
* Knowledge of NS
  * Know various criminal groups helped get NS off-world, past the blockade
  * No knowledge of ZC

## Deaf Bandits
* Nej contacts them
  * "Ha'am, I know you've probably got a lot going on, but we're in a bind and could really use the help. Something big is going on and... we're pretty much on our own out here."
* Need help--extra firepower
	* Operation Flashback
  * Destroying a hypermatter extraction facility and a massive shipment leaving
  * Besh Scorpii system, harvesting from a white dwarf
	* Rotating 2 hour harvest/2 hour refining cycle
  * Single landing corridor along vector behind the Besh Scorpii 1 planetoid
	* Fast rotations mean only 2 hours they can be on the surface before Phase 4 radiation will scour clean the surface
	* Protected by a Carrack Light Cruiser, and Gozanti assault carrier
  * Reinforcments never came
  * Standing by with a decoy ship on the edge of the system to draw off the patrols
	* Board station, plant explosives in hypermatter refining
	* Careful! Extremely volatile!
  * Bulk freighter is docked, loading massive shipment of hypermatter bound for DS-1
  

## Ashur Sungazer
* Ashur sends another message
  * "Asori, Ha'am, Timm'eh. Hope you are doing well, and that you are finding the answers you are looking for. I was hoping that you could share some of the details of your expedition with me to help plan my own."
* Looking for info, organizing an expedition to Kanthi
  * Currently staying on Sullust
  * Acquired a ship and crew: Peyton's Fortune
* Ashur is curious what the Jedi have been up to
  * Disheartened to have missed the secrets of Kanthi
	* Would like to visit some time
  * Remains loyal to them
 * Knows he has a target on his back, especially due to the imprisonment
  * Perhaps not so bad since the ISB agent responsible is disgraced
  * Been laying low but looking for a way back to academia
	* Preferably coreward, but could live in the mid-rim
	* Never like Heron, can't stand being out here on the rim
* Needs a way to prove his loyalty
  * Perhaps turining over some sufficiently interesting treasures...?
	* Hoping to find something on Kanthi

## Daro Tillie
...a standing request from Daro Tillie asks for a fast ship to make a delivery to Mygeeto. The Imperial blockade currently in place around the planet goes unmentioned

* Weapons for local rebel cells
  * Working on dislodging the imperial administration

## Distress beacon
* *Monarch of Ariel*, out-dated Trade Fed Freighter
  * Fern Bhyrrin Kel-dor captain
  * Former fighterpilot
  * Zealous, trusting, jumpy
  * 
* Triggered some sort of mine, unable to extricate itself
* No sign of pirates... yet
* Triggering mines will cause alert
  * Droid, driven starvipers attack
  * Marauder corvette to jump in 
