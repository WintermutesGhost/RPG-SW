TL;DR Crazy dream story
-Asori doesn’t remember the sword of destiny, hates Mathieus, wants to save Ceneza (Cinder in Spanish as you know)
-Timmeh doesn’t recognize Asori, but remembers everything else, doesn’t remember Blu
-It is implied that Timmeh had one seen the inside of the temple and the item, but has forgotten it (under his own doing or someone else)
-Haam doesn’t remember Saar, but now has a past with the child Talikum
-Mathieus is a dream-wizard, but had to sacrifice and take damage to get them out, he doesn’t know his own name, or past
-All this can be temporary, as the dream was supposed to scare them into fearing the potential long term damages the NS can do with memory tampering
-If it isn’t permanent then that somewhat wraps up the other people whose memories were wiped by the NS
-They all want to save this little girl, Ceneza, who is being used by NS
-The all want to kill this unnamed NS (who isn’t even described in detail on purpose)

--- As it played out ---
Short form:
T: Timmeh
H: Haam
A: Assori
M: Mathius
B: Bluth (Blu) Indego
C: Ceneza

This is the night after the last encounter.
Before the game I handed each player a strip of 2x11 inch strip of paper, each strip was half white, and half black (marker sketched, with runic symbol), split along the long side. I asked each character to write, in a single sentence, what they ‘dreamt’ about that night.

T – about how safe he feels in the new ship, how it feels like home
A – Emerging from the darkness, into the dawn light, warming her green skin; appreciating the connection with Tenenbraus
H – Staring down the scope of her rifle, a panther in her sights, itching to pull the trigger

I took each strip (+1 that was dreamless, and the dark side jaggedly descends to nothing) and tapped them end to end, with a twist at the loop connection (Mobius strip).

-Cut to beginning-
T sees nothing; darkness
The smell of smoke, and an ember float into view and snuffs out
T awakens in his room, everything is a haze (ethereal like)
T fails discipline check; still thinks its reality and that he may be drugged
T leaves his bed, his leg falls through the floor
T tries to leave the room
The hallway is metallic and ship like, but jungle vines run throughout the hallway, humidity permeates everywhere
T goes next do to H, bangs on the door
T stim packs him self, he feels adrenaline and can see the medicine rushing through to his 2 hearts
-Cut to H dream-
H is staring down the scope, panther-like creature in the scopes
H tries to move, but is absent of her body; only the scope is scene
H passes discipline check
H knows this is a dream… bang, bang…
Gun shot? The panther is alerted, moves out of the scopes sight
H turns to see, darkness evaporates, H is in the room staring at her door, someone knocking
H opens the door, T on the other side
They meet, walk the hall, crawling with vines
Vines pulsate with light, and life energy
A flower appears along vine as T, it’s the Krak flower of Thila
It sprouts, flowers and dies all in a moment. T fails discipline check again to peer through dream, he still thinks he’s been drugged haha
They began to walk to the front of the ship, the vines twist and pinch their progress…
They arrive at the entrance to the elevator, vines are not present
-Cut to A dream-
Blackness, everywhere. Darkness pressing against every inch of her body, holding her in place. The smell of dirt…. Then smoke… an ember floats upon the wind in her vision, and then darkness
A is held in place
A fights with minimal effort, her head at first, cracks appear in the darkness, dirt crumbles and light break through… She pushes harder, blinding light breaks through, the thin shell of dirt falls away as she move through it, gravity shifts, she is not pushing through, but digging down
Down, down through the ceiling of the elevator room… falls onto the H and T.
Damage is only taken if they asked about damage (if you believe you can take damage, then you do, this a dream after all).
They collect themselves
They try to com-link K17…*static*
They tune into other frequencies, only one has the scream of a man, saying “they’ve breached the wall” “fire, fire, fire” (GM note: these are M’s dreams of war seeping into the others).
Nothing else
They board the elevator, all is normal. (If they tried to read the numbers of the floors, I would have said they could not focus their eyes or attention to it, unperceivable)
A asks if her sword is with her (I lead her to asking for something specifically, Nathan chooses the sword of destiny i.e., the sword Baldyr gave here from the Elders)
The door opens into thick alien jungle, arid though. T rolls, this is Dathomir, T’s home and hunting ground
T realizes this is dream now
The sound of a Reek is in the distance, its mating season, and wild Reek (introduced to Dathomir, but wild Reek roam in areas)
A voice whispers over their shoulders, a direct voice sternly tells them to get down, take cover. Bluth appears, ignore them, and takes cover in the brush, singling for them to take cover
-cut to Bluth back story- Bluth (aka Blu) is a female Zabrak that T grew up with as an orphan after his family died. She has indigo skin and is tall, powerful, and confident. She speaks with slight semi-African accent. I had Michael invent her backstory on the spot. He said:
-they are the same age, she was also orphaned, and was the closest thing to family, and later, a potential mate he’s ever had. She may be force sensitive, but more attuned to everyday needs of life and less to the force. They grew up and had ideas of life together, but the draw of T’s pilgrimage had separated them. They have not talked since. Story idea: add her back in later on Dathomir?
A attacks her, B blocks, shoves the shoulder of her rifle into her stomach, dropping A. She carries on signalling that they get down. She also makes bird sounds, that are clearly signalling another hunter nearby. The reek nears.
H gains cover in the trees, in her ghillie suite, the tree and moss meld into her, starting to engulf her, further camouflaging (the dream reflects intentions).
Asori sits on the dirt and meditates. (Fails Discipline) Receives no images, but feels the ground give a little, softness beneath. She sees that beneath her, outlining just her seat is the black sand of Thela and Drumasan.
The Reek nears, B and T share words, B seems nonplus and annoyed at his noisy demeanour and lack of hunting skills
T gets into the mode of hunting, feels at home again. Pleasantly enjoys the dream for what it is: a good memory. 
The Reek nears, quite, no site of it. H settles in, rifle aimed. A grows impatient, makes noise, altercation between B (loosing patience over the hunt being ruined), T, and A ensue, but before the first blow, the Reek erupts from the brush and rears up upon A.
Before the Reek crashes upon A, H fires her weapon. Blackness blinks upon them, the gun shot reverberates and rings out longer than normal, turns into the sounds of a hover carrier beeping loudly.
-Cut to H Dream-
T, H, and A appear in the a city scape (akin to Corellia). They stand on a busy street, bustling and crowded with citizens and vehicles whizzing about. A stands in front of a sign post on the street, recoiling from it (it is roughly where the Reek was coming from)
H is across the street, her rifle drawn and aiming across towards A and where the Reek were
A looks the left, a soup cart and man have been knocked entirely into the wall
It’s clear that H shot the cart
A helps the man (human), he yells at her in a foreign, unintelligible language (fail lore check, it is an ancient language of Jedi). He is crying, inconsolable, and driven to his knees
T begins to loose patience, tries to use discipline to push through the dream. Wake up. (success)
A deep reverberating bellow erupts from the cloudy sky, it shake the very earth around them
The clouds part, and a hundred km mobius strip is now visible in the sky (I took the strip and hung it from Nathan’s light fixture). It slowly turns in the sky
Each character looks to the strip, it is miles away in the sky, but a dark and light strip are visible, with illegible markings
Each character who investigates the strip sees a single quarter that is illuminated brighter than the rest, emitting familiarity (they decipher that each quarter is only illuminated for the individual who wrote the dream) The fourth is unaccounted for, they think its Baldyr.
A child appears tugging at the side of A, begging for money. A has no money (she asked me if she had money). The child runs into an alley near by when confronted for information, just as he dips past the corner, a little girl (8-9 y.o.) stands there staring at A,
She has grey and cracking skin, the wind whisking flakes off of her. She stares, and runs off into the alley
They follow
It is littered with bags of garbage and containers, narrow. The two children easily weave and skip through the garbage, T, H, and A follow, but slowly
The follow, and when they look back, after moving maybe 50 ft. they see that the alley is now spanning to infinity, in both directions, the wall now reaching up well beyond before
A sliver of sky cuts and reveals the Mobius strip between buildings
The girls stands twenty ft away now turned, A approaches her
Now, can be seen that the girl ashen, cracked and embers burn beneath her skin
She says nothing when confronted just stoic and resolute. A becomes confrontational.
She says “this isn’t how it works”
The boy from before comes to H’s side, H recognizes this boy as one of the orphans she cared for and protected. A notable one names Talikum (gruff dirty boy, blends in, plain and forgettable).  (I forgot, but I wanted Weston to make up a story for this character, but we pressed on)
He begs H for food, H reaches into her bag and produces the kebobs from the Wookie (Mother Shiva)
Garbage begins to move, children emerge from beneath and behind, countless children start to stream from where they were towards H
All begging for food, H produces more and more food from her bag, each time a child takes the food, they move it to their chest and it melds into them and they demand more
Ever needy, she tries to keep up
A notices that the girl has disappeared, she struggles to move upstream and through the children to H
A and H are buffeted about by the swell of children, now increasingly hungry, needy and aggressively grasping at H, T is unaffected completely
T ignores this, and now stares to the sky, and tries to connect with the Mobius strip (if Nathan or Weston would have asked to talk to Michael, I would have said he was no longer there)
In T’s place their stands a little boy zabrak (T as a child)
H and A look up stream of the children, and see the only things not moving are the child zabrak (T) looking dead eyed and past them, and the ashen girl standing several feet away, back turned to H, and staring at the zabrak boy
H and T try to push upstream to the girl and zabrak, the children become aggressive in holding them back, wanting food
A negotiates with them, saying that if they help her, she will give them Thela mangos
All the children stop, become still, their eyes fade black, they all turn their faces to A, and mutter in unison “Thela”… “Mangos” (Nathan actively shuttered at the imagery of countless children acting creepy)
They start to clamber to A, she is overwhelmed, trips to the ground
T is still un-phased by the swell of children, tries to force move the strip “forward”, I allowed it: the sky bellows and erupts
A is incapable of producing Thela mangos (this is Eos/Tenenbraus protecting her memories from being corrupted)
A pulls a frag grenade and blows herself up
-Cut to A’s dream-
An endless expanse of black Drumasan sand, dusk with thick low clouds diffuse the sky purple and pink, the Mobius strip still hangs in the sky, but only a third dips below the sky, slowly turning
A mound 50ft a way begins to move. It a ring of sand can be seen enclosing the T, H, and A
The ring moves slowly in a circle, the sand shifts as something moves underneath (Nathan immediately suspected a snake, and then Tenenbraus, succeed discipline)
A says don’t harm the ring (she did not admit she thought it was Tenenbraus)
The sky and darkens
Mounds of sand begin to form outside of the circle
They grow taller, humanoid
Countless black sand figures stand, peppering the plane and off into the horizon
A bang from the sky erupt
Swiftly, the black sand figures rush the group, black sand swords in hand
As they reach the edge of the circle, and start to walk up over the circle mound, they sand collapses, returning to the ground
More, more, more sand effigies emerge, blotting out the horizon with figure
Each of them fall rushing the mound
Then 1 gets through (random roll of di to see how many and from what direction)
It comes, and attacks Asori. It brings its sword to its chest, takes a stance (perception, knowledge warfare reveals it’s a Drumasan stance) 
After landing a blow, the figure collapses to sand again
More come, if the team gets a hit on them first, the figure collapses
More randomly make it through, the circle begins to shrink
H calls out on the com-link for K17 to rain down fire from the ship, and imagines the Reek appearing. Both happen (dream fulfillment, if they wanted something, I would make it appear)
the team was rather conservative in their dreams, the catch was that if they delved too deep into wish fulfillment, they would have the chance of loosing the memories of things/people as part of it
A chooses to ask for her light sabers, they manifest in her hands (side effect, with failed discipline roll, she forgets the sword of destiny, possibly forever)
Two triumphs on some roll, I granted a important information: Dotting into vision amongst the figures, at extreme range, is a small child figure, embers peel off her 
More and more figures come
T’s necklace from Blu begins to burn on his neck, he throws it away and as he does, he looses his memories of Blu
The Reek comes barrelling through the crowd into the circle and out again, clearing the way
They group each at a time push their way out of the circle against the sand figures, they make their way to the child
She is crying, pleading with them, apologizing “I am soo sorry”, “this is the only way”, “this isn’t how it’s supposed to work”
They are at a lose, taking damage (if they ask)
She points to the sky, “you must…”, she mouths words after this but nothing comes out
T tries to discipline/force rip the Mobius strip in the sky
-Cut to Weston cutting the Mobius strip in half along the ring, resulting in a longer ring with 2 loops, now one half is the blackness, the other the light-
All of the figures fall into the sand, the terrain changes growing mounds and rolling hills of moss, with black ‘ichor’-like fluid pooling in low areas, countless forever
The girl begins to dance around, laughing and singing, elated
A,T,H talk to her, she finally speaks normally, her name is Ceneza (C)
She admits this is her home, her dream, the “Blood Moors”, her skin is flaking and deep cuts reveal burning embers fluctuating in brilliance (She is from the Blood moors, and she is a child night sister, but her appearance is guised, she is being used as a conduit by the actual night sister to access the dreams of the group without danger to the nightsister)
She says that the ‘other’ came, and pushed her out of her dream, and told her to get you all here, to C’s dream
They probe a bit, the other is not Baldyr, but Mathieus (all force based users were brought into the dream) (Baldyrs affect I haven’t decided)
Mathieus came to her dream and had to hide himself, so he put layers of protection to hide within the childs dream, hiding his own dream.
*the group then solved a number of puzzles that were Iron wood rods floating in the ichor, they would not move unless they pulled the right ones, the puzzles were remove 3, leave three squares, that sort of thing. The number associated with the group. H and T have 2 hearts each, which H has 2 brains. H and T take the rods on one, and 6 become 2, when they do this they succeed, but their 4 hearts give out, they collapse unconscious, leaving A and C
A must solve the last puzzle with C, and use a destiny point to move the last rod
A kind of gets annoyed at C, pushing her away and ignoring her, trying to solve the puzzle by herself: moving the iron wood rods on her own, doesn’t work
The girl hides from A, throws a chunk of moss at A from behind a mound, being a mischievous child that was scolded by an adult 
A realizes she has to work with C, asks her, same mound is thrown haha
Finally they work together with persuasion and frustration 
They use the destiny point
They all fall through pools of ichor, as they glow bright green with every success, they fall through
Touching moment * A grabs C as they fall into the darkness, C clutches her close *
As gravity inverts and they come out on a mirror image of the moors, C continues to clutch A for safety (Nathan realizes she is just a little, scared girl, changes attitude)
The other side is the same, but millions of dead bodies litter the mounds
Mathieus sits on a cleared mound, greets them
The Mobius strip is in the sky, but only the white facing to them.
Ceneza can now be seen as a girl without embers across he body,  she is a nightsister girl with the tattoos and all
-Exposition from Mathieus-
He is a master vivid dreamer, he knew exactly what this was when he got her, made a pocket dream reality inside of the little girls dream to hide himself from the prying eyes of the entity that brought them there (he doesn’t know who, or why)
But he knows this whole dream is for T, something to do with getting some thing or information from T
M knows now that the entity wants something from him now too, information on the location of a Holocron, *M pulls this out from the light side of the Mobius strip to the blood moors, he says he can possibly let the entity find this
M tells them to wake up, get out of the dream, to be careful of what they dream for… it is revealed that T doesn’t recognize A any more…
M comforts the girl, and reads her mind
They see this, A is disgusted by M
M relays that the girl is being used by a nightsister to peer into their dream, but she is hiding a lot, using the girl as a shield
A says “how could you use this girl like this, pushing her out”
M says “she’s no worse for ware, everythings fine” playing down A’s concerns really made A angry, A attacks M
M immediately redirects the attack and falls into the pool, reappearing somewhere else (he is a master of vivid dreaming)
H or T tries to do something with the force the strip
The sky erupts in sound, M reacts in panic “What are you doing? What did you do? This is a delicate system” “oh no, no, no!” The strip starts to invert, showing the dark side to them
M immediately brings out his light saber and slashes the holocron, it cracks into pieces, each chunk goes from visible to dark void in the shape of the chucks, falling into the pool
He pushes them out, saying he will keep the entity busy
-Cut to  Merged dreams-
The team comes a place that is a chaotic mashup of all their dreams overlapping, sand, jungle, city.
There is a temple, part of a temple in front of them… T (lore) knows without knowing this is “the temple name you gave me”, and when they go inside, they see that Blu and another zabrak woman are there, another friend from T’s past (Triz) He does’nt recognize Blu, but does recognize Triz from his past (but this is a planted memory from the night sister)
The temple portion is a small square area, with towering rocks and mortar for the walls
The center has a pedestal, but the figure on it is roughly the shape you gave me, but guised in a void of darkness
Triz and Blu talk to T, Triz reacts to T not knowing the item on the pedestal, reveals herself to be the embodiment of the NS, (unnamed, nor described)
She breaks the void item, and begins to attack the group
The way is closed behind them with the children of H
NS starts to attack them, floating through the walls
-Battle-
They are stuck inside the tower, and crazy shit happens, but basically NS flys on the outside of the temple, peeling away the walls to attack, during that time, the each had to catch a glimpse of their portion of the dream and “mentally attack it” to wake up. 
H asked for Sar to help and appear, Sar would block the force lightning

-Side effects-
H starts to forget Sar (NS attacked H, and she got two triumphs)
-As each of them attacked the their sections, they would have an exiting dream, touching on the dream they had before, and I asked how each would wake themselves up…

-T (I don’t fully remember what he said)
-H talks to Talikim, share a great moment, where they eat salted dried fish in the Market,
Talikim asks H if she remembers him, kinda breaking the vale of the dream
H replies, “Yes I remember you”
Talikam replies “Cause I never forgot you, or what you did for me”
-A is left to escape on her own, she dream attacks the NS, stabbing her through the stomach, NS laughs (her deep evil chuckle was super fun), and says she (A) was no fun anyways
NS makes a motion and takes something from A, a black slithering entity rolls out from A’s hands and into NS

-A no doesn’t remember Tenenbraus or it is gone for now?

-Cut to ship-
The 4 are standing in a circle, K17 shows up (says what you told me) and there is a NS diagram in blood and muck on the ground, it is cleat they drew it together and were sleep walking.
When confronted, M seems normal, but then reveals he doesn’t remember anything about himself (like his name,”My name? well it’s obvious… obviously it’s … um… it’s obvious….”)
Timmeh still doesn’t recognize Asori *cliff hanger*

End






