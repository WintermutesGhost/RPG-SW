How are we feeling?
	* T - Memory issues with Asori, trust. Disoriented.
		* Forgotten Asori totally--understands she is trustworthy
	* H - Exhausted, sleepless, uncertain. 
		* Positive feelings for Sar, memories return after session
	* A - Frustrated, concerned, upset. Angry. 
		* Hates Mathias--selfish, self-defeating, useless. 
		* Hates Nightsisters--abusing others, hatred, equivalent to the Jedi (???)
		* Worried about Timm'eh--lost their progress, they have both changed
	* B - Does not understand what has happenend to others
		* Concerned about memory lost
	* M - Blank slate
		* Sent to the brig
		* Mostly released
	
* Travel to see Ashur
  * Bring Sar
* Put everything back together
  * Timm'eh begins to recall Asori
  * Pulls her back through their emotional bond
  * Remember everything

* Asori says the Jedi are misguided

* Ashur is amazed

* Purchased their equipment

* Sar is frightened of going back to sleep
  * A fails to charm
  * H fails to lead
  * T ultimately decepts
  
* Ashur asks for coordinates to Kanthi
  * They hand over gladly!

* Investigate the history of the Sith
  * Scrubbed, but find sideward references
  * Learn of the Empire
  * Unable to find specifics about the old republic
  
> Search for details on the necklace from the temple of Light and Dark

* Tells Ashur about Kanthi

* Ashur gets little from Mathias and Val Isa

> Ask Ashur to contact before leaving and when returning

* Ask M if he wants to go back to Kanthi -> No, fear
* Test Mathias about his lightsabre
  * Does ok -> agree to bring him
  * Asori avoids him

* H sends Sar to watch Mathias
* A meditates on
  * Convene with Tenebrous
  * Protections from mental force attacks
  * Location of Ceneza
* B meditates on Eos
  * Attentitve to Asori
  * Mathias asks for training
* T works with Val Isa on new leads


* A 2 conflict for using Twilight Blessing
* T 2 morality for decepting Sar
* H and A 1 conflict for failing to help him
* A 1 conflict for confusion about Mathias


* 13 xp for remembering past
* 2  xp for complete history
* 2  xp for helping Sar
* 2  xp for insight from Ashur
