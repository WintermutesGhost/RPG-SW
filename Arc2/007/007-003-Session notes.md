Sneak to the planet
	* Ditch the wookie ships
	* Close in silently
Introduced to Chuundar, the mayor
Enjoy meals with the wookies
H tries to get in touch with the BH guild
	* Fails
A, B, H partake in Orga
	* Saar partakes, Squib partakes
A arm-wrestles with a wookie
	* Beats Hewbacc
	* Eventually smooths it over
Grachaw wants to life-debt H
	* H is reluctant
	* Grachaw feels unworthy -> resolves to try and help
A B H have dreams of the Tyvokk
Purchase repairs
	* Grachaw helps fix guns in exchange for the destroyed fighter
Purchase cyber-eye
	* A influences, T reads mind 
Hewbacc challenges A to a race
	* A agrees
B trains with energy bow
H chats with Grachaw
A practices with the fighter
H gets knickers in a twist over the bow
	* B destroys
	* H and A frustrated
		* A is mean to Baldyr
RACE!
	* Hewbacc flames out, A rescues
A appologizes to B
	* Meditate together
T gets cybereye installed
T will allow the bow to be used, only by Baldyr
Dark pull to the forest floor -> Conflict!
	* H searches deep beneath
		* A tug, pull, not sentient
	* A asks Hewbacc to guide -> succeeds w/ advantage
		* Will come, but only as far as they seem able
	* Saar and Squib have felt nothing
Meditate on it
	* Alien
	* Placated
Hewbacc guides to secret elevator
Fight the Kinrath on level 5
Detect the Terent
Thrown off by the anakkona
T seeks for the rest of the party
Separated!
H takes the offered Orga root
T and A attacked by the Kitarn
	* Easy. 
	* Hunter killing a hunter
Chat about the voices
	* Believe it is something else... something common to the two
	* A divulges that it brought B to A
Hewbacc is afraid, plans to return to the surface
	* T convinces to stay, but Hewbacc expects to die
Nope-> "Were any of the the voices in our head a dark manipulation?"
Avoid the wyyschokks
	* But too slow
Terentateks move in
	* Back into a corner and wait -> scary!
	* H fearful -> Despair!
	* A bold -> Triumph!
	* Mathias gets knocked out, crippled
	* B and A fell one 
	* H tries to fear it -> no dice
	* B and A fells the second
	* Pricks A with its venom -> Recovers no problem
"Is this an artefact of the dark side?"
	* A uses dark
	* -> "No"
	* Baldyr takes and carries it



3 xp for returning the Wookies home
1 xp for completely avoiding Imperial detection
2 xp for forming bonds with the citizens of Otikuti
    (Rivalry with Hewbacc, friendship with Grachaw)
1 xp for saving Hewbacc from a fiery death
4 xp for surviving the descent to the Shadowlands
6 xp for defeating a pair (!!) of terentateks
4 xp for locating the Tyvokk--the Seeker

In addition:
Asori and Ha'am, 5 xp for Vigilance or Coercion for defeating the Kitarn
Baldyr and Timm'eh, 5 xp for Discipline or Negotiation for receiving the Orga plant's offering

Morality:
A   H   T
1       1   Using force influence for a discount on cybernetics
1   1   2   Arguing over the use of the NS energy bow
2   1   1   Guilt over nearly getting Hewbacc killed (race and terentateks)
1   1   1   Call of the Tyvokk
1   1   1   Concern over the manipulative voices
    3   1   Facing down the terentateks
1?  1?      Dark side force use?