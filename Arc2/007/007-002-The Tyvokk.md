## Eos
* "Deep beneath the forest canopy, a stone. Ancient. Dangerous. It twists the forest cruelly. A darkness bleeding like a terrible wound. It has turned the creatures of the jungle against you. They hunt you. It has caught the scent of your powers. You cannot escape it, and must put it down, as a rabid animal."

"The stone is dangerous, and will pull you into its darkness. Yet it is powerful, a tool of reconstruction. Keep it safe. Do not let it twist the wills of the others. Take care, and protect Asori from it."


## Tenebrous
* "The artefact you seek on the forest floor contains powers ancient and dangerous. It bleeds black into the forest roots, twisting it to be more vicious and violent. Beware the Terentatek, they hunt beings such as yourselves. They know no fear nor reason, and wish only to feed upon your powers--take care."

"The relic is dangerous, but contains great power. I have asked little of you until now, but keep the relic safe. I have use for it, and lend you more powers in return."


## The Map
"In the centre of a clearing sits a crude altar made of stone, with remains of a temple or offeratory around it. Atop the altar is a crumbling, three-armed stone structure. As you draw closer, you realize that the remains of the temple are formed out of the twisted roots of the trees, and vines lining them have moulded together almost into solid walls. The altar appears to be be stone, pulled up from the ground itself, distorted, or perhaps melted and shaped, into a triangular platform. The object on the platform is made out of smaller rocks, pressed and crushed into three arms that look as though they could hold something. One stone stands out: Black like obsidian, with faint runes carved into it, near the base of one arm."

"As you remove the stone, the others in the structure collapse with a crunch, scattering about your feet. A cloud of powdered stone drifts down around the altar."
