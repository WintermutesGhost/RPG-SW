## The Blockade
* The *Wrrly* is parked at the edge of the system waiting
* Light blockade of Imperial ships present
  * Not yet detected
  * Only illegal to land on Wookie Planet C


## Town of Okikuti
* Currently begining life-day ceremonies
  * Wookies attend in traditional robes 
  * Party invited to join as Grachaw's honor family
  * Feast of meats and fried chyntuck
  * Partaking in Ogra root will give visions of the darkened grove

* Asori    - Race Hewbacc with Agr starfighters
* Baldyr   - Younger wookie will help making a bowcaster
* Ha'am    - Grachaw wishes to swear a life-debt
* Timm'eh  - Called out to

* Matthias - Helps with crop issues
* Saar     - Falls in with Wookie youngs (Chitca) who are going hunting
* Squib    - Eats. Butt of jokes. Wookies love him.


## Forests of Kashyyyk
* Wroshyr trees
  * Tall, used for everything
  * Grow together into a single massive plant
* Seemingly endless forest beneath
* 

## Journey to the Seeker
* Timm'eh can feel the tug of something deep beneath wanting to be found
  * The Okikuti villagers know of the Tyvokk
    * Ritual relic in the deepest layers of the forest
    * Hundred of years ago, Chief Grikoo used to to find enslaved wookies
  * Actually a fragment of a Rakata Star Map
    * Reaching out for the draw of force energies
    * Searching for the device from which it was hewn
    * Can be used to find something, but will be destroyed

### The Descent
* Gorryl slugs
* Kinrath
* Kkorrwrot

### Deep forest
* Something following, descending
* Anakkona
  * Too large to be a direct threat
  * The trunk beneath begins to move, threatening to throw the party into the depths
* Separation!

### The Bright Grove
* Filled with daubirds, kkryytch and agrs
* Orga plant is present -> Some roots are offered
  * A single root is offered
  * Otherwise it reacts violently
    * Saw-lke vines
    * Tentacle-like petals
    * Acid pools
    * Acid-filled seed pods thrown

### The Twilit Grove
* Shadowy, dangerous, watched by glinting red eyes
* Katarn attacks
  * Testing, driving them to be stronger
  * Departs if clearly bested

### Ambush
* Stalking wyyschokk
* A swarm descend on the party -> entangling

### The Darkened Grove
* Pair of Terentateks
* A fragment of a Star Map lies here
  * Bleeding darkness into the foliage
  * Has the ability to empower the seeking of something
