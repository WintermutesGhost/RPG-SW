Ashur departs

---

Arrive in Iridonia.
Get out the gifts
	* Beast call
	* Stories
	
Led to long-house
	* See people training
	* Give beast call
	* Tell stories of the past

Saba in Iridia Prime knows of the issues they've faced
	* Group is concerned about it
	
T says vengeance against the NS is part of his driving

Al'ise pitches a fit, says they need to return to their ways
	* Leaves
	* T agrees,
	* H follows Al'ise
	  * H agrees with Al'ise that a people's strength is in how the recover from loss
	
A wants to train with Doone
	* A fails
	
T speaks to Baub
	* Baub says T's village was in the Indar Caldera, now in the Keena Valley

T helps in the field

A spars with B
	* H blocks the force
	* B trounces A

T communes with Val-Isa
	* H refuses to share necklace -> Is he posessed??????

T speaks to Baub
	* Baub explains their history
	* T wants to go

Blu returns with the hunters
	* B doesn't remember, but A does
	* B asks to spend time with Blu

Blu chooses to come with them on their trip to the temple
	* Blu guides them to the village grounds
	
T remembers some, but does not feel it
	* T meditates, makes peace with past
	* H takes a broken energy bow

Head on to temple
	* Blu stops when the Terentatek appears
	* Move around it
	* T sees it is tracking them
	
T storms directly into the door
	* Look around, no ideas
	
T smears blood on the door, exposing self to the darkness
	* ***1 Conflict, T***

A sees the statue, and looks for resemblance of the half-mask

A recognizes Sith symbol
	* ***1 conflict***
	* T: Maybe were supposed to keep people out
	* B: Feels anger
	* H: Fear - ***1 conflict***

T wants to destroy the statue
	* A and B object

Find the droids, deduce the power source
	* Decide to save one for K-17
	* Promise him

Turn over the barracks and mess

T gets worried, asks A to cut the arm off the statue

Go in the statue
	* Turn off lights ***1 conflict H fails***
	* T feels a tug from the dark side
	* A touches the first statue - Succeeds ***2 conflict A***
	* A touches second statue - Fails, then succeeds ***2 conflict A***
	* T worries about Asori
	* H touches third statue - Fails, fails,
	* T touches third statue - Does nothing
	* Mathias touches thrid statue - Works! ***2 conflict***
	* T touches fourth statue - Fails, uses force ***1 conflict***. 
	* A goes to touch statue, but B  intervenes - Succeeds **2 conflict***
	  * A snaps at B
	* T touches fifth statue - Succeeds, uses force ***3 conflict***
	* A hates bearing this cost, H encourages
	* A dislikes the killing of the Sith, T rebukes
	  * Argue philosophy ***1 conflict, A and T*** opens mind to dark side
	* T uses force on the statue... but decides not to use the dark
	* H uses force on the statue... ***3 conflict, H***
	* Wall opens
	
Notice the destroyed droids

Decide to bring K-17 to examine the library

T examines the tomb ***1 conflict***
	* Opt not to go in...
	
A touches Holocron door, but resists
	* Leave with a limited understanding

A discovers the location of 'Rane 7', central to the Axus Contingency

Wonder about the lab, H investigates the tubes

A shuts down the reactor

Go to investigate the tomb
	* A fails fear ***1 conflict***
	* H supresses
	
Debate opening the tomb
	* Ask the universe, and no, do not open
	
T gets spooked
	***1 conflict***

T tells Baub about everything
	* Asks them to keep it to himself
	
Blu leaves without speaking

H and B get sweet tattoos
T gets own tattoos
	
2 - Reconnecting with the Hortoon
4 - Visiting ghost village
2 - Opening the temple
4 - Excavating the temple
8 - Secrets of Ta'siim
