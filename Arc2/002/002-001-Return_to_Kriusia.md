## Crawl

After returning to the small mining city of MOS SHUUTA, and witnessing the suffering under negligent Imperial rule, the Knights set to negotiating a new future for its citizens.

The upstart shipbuilding company NIHI-OSHIN has offered repairs to the SOLAR FATE in exchange for the mining rights. Currently without a ship, the Knights have been offered a meeting with TPI CONSOLIDATED, a shipping company hiring mercenaries and spacers.

Last visit to ELROOD, they faced a coven of NIGHTSISTERS. Now, they are once-again travelling to the metropolis of KRIUSIA, hoping for a simple job this time.


## Storyboard

### Travel to Elrood
* Onboard the Azure Wayfarer a Curich-class shuttle (AoR:DA:61)
	* Captain Hoda - M Human; flexible, energetic, blunt; tanned skin, milky white eyes
	* Spacers - Recruited for merchant navy
		* Lt. Uato - M Sullustan Officer; skeptical, cunning, rebellious; Yellow eyes
		* Xensi - F Rodian Navigator; efficient, spontaneous, aloof; left cyberleg
		* Inun'kairn - F Twi'lek Quartermaster; impassioned, inquisitive, disorganized
	* TPI Management from Torina
		* 11 Mixed Humans, Nautolan, one Gamorrean, one Xexto
		* Meeting to discuss a new TPI branch on Almaran
* Attacked en-route by pirates (acting for ZC)
	* Goal to raise interest of potential mercenary team
	* Asteroid tugged into path of the shuttle
	* Dropped from Hyperspace during dinner
		* Passengers directed to return to quarters
	* Attacked by a pair of StarViper M2s (EotE:MotPQ:81)
		* Easily driven off by damaging either one below 50% integrity

### Elrood
#### City of Kriusia
* Terrestrial planet
* Heavily controled by megacorps
  * Waning Imperial presence under new administration (Moff Daivand)
* Shadowrun-y
  * Bustling metropolis of 10 million
  * Coastal city off the Dona sea
* Three starports:
    * Primary
		* Secure
    * Military
		* Locked down
    * Shipping
		* Insecure, remote
	* O: Lamos city port
		* Secure, nearby city accessible by speeder and commuter train
	* O: Pantha local port
		* Mostly used in delivering fishing equipment from on-world
* Businesses in manufacturing, food, colonization goods
	* Currently suffering
		* ZC pirates raiding shipping lines
		* Imperials unwilling to dispatch fleets this remotely
	* Manufacturing dealing with from materials shortages
		* ZC corps are emphasizing shipments to other sectors
		* Factories closing, leading to unemployment and disatisfaction
	* Food shortages affecting the sector, mostly impacts other worlds
		* ZC pirates attacking shipping lanes
		* Decreased Imperial investment in the sector
	* Conflicts with other planets in the sector
		* Decreased exports/support from Elrood
		* ZC influncing local leaders to raise disputes with the sector capitol
* Varied population
    * 60% Human
    * 40% Misc
    * Unemployment approaching 15%
* Hutt controlled underworld has been gutted
	* Combination of assasinations, imperial raids, and ZC-backed groups
    * Sector Hutt Rooda Gana Tobba has moved to his local palace on Elrood
    	* Desperate to keep control
    	* Part of a smaller family, control over this sector is vital
    * Local organized crime mostly disappeared
    	* Very few Hutt-aligned units
    	* Mostly upstart gangs
    	* Growing ZC presence unting the gangs
    * Blacksun Mercenaries losing influence
    	* Several major bounties went badly, others assasinated
    	* No public support, seen as just an extension of the Empire
* Zann Consortium taking root, both above and below ground
	* K'zakk mercenaries rebranded -> Vornskr Security
* Governor Cen Horne struggling with waning public support
    * Major improvement and public-works cancelled
	* Tool of the Empire
	* Doing nothing to help the population
	* Frequent protests, turned violent recently

### TPI Consolidated
* Offered to work with TPL Consolidated
	* Formerly a smaller rim-world transport company
	* Recently expanding into multiple industries
	* Major boom in the Elrood sector
	* Distantly controlled by ZC
	* Contstantly hiring new crews for their Merchant Fleet
		* Mercenaries for security
		* Reputation for not cheaping on security for their transports
	* Quickly becoming a major power in Kriusia
		* Relatively little resistance from other MegaCorps

* Meeting with Uema Duskrover at new TPI headquarters
	* Towering office block in downtown
		* Shining alusteel tower covered in clari-crystaline windows
		* Mid way, walkout transparisteel band around the building offering a view of the streets below
		* 42 floors
		* ~ 3000 employees
	* Office clerked by a receptionist droid
		* Escorted up by Qall, F Kel Dor clerical worker
	* Dir. Duskrover's office is on the 38th floor
		* Clean, Light gray walls, window overlooking the downtown streets
		* Tappestry of Battle of Concord Dawn
			* Troops wearing strange armour in different colours of black and green
				* Faces hidden behind masks with T-shaped visors
			* Child clutching a dying soldier
			* Green forest with red sky above
		* Mythra top-skull on desk - Gift from House Tresario
		* Earthy smell from the spells
			* Rudimentary truth spells on seats, two setbacks when lying

* Uema Duskrover - Consortium Gatekeeper
	* Attentive, glib, 
	* Serves as TPI Consolidated Vice President of mercenary relations
		* Front for the consortium
	* Responsible for checking on potential crew before passing on
  		* Assisted by Sister Terrano
  	> Knows the party are powerful, wants them on her side, means a way up in the ZC

* Party came highly recommended
	* No background through BH Guild or Major Merc. Orgs
	* Excellent recommendations from partners on Hypori
		* Kova Technologies records on party's work unfortunately destroyed
		* Director Ne'hen personally messaged regarding your skills
	* Want to help people
		* President Carthen of Nihi-Oshin says party working to support disenfranchised on Askaj
		* TPI wants to help the people of Elrood and the Rim
			* Empire isn't doing enough
			* Piracy rampant
	* Even come with own ship
		* Used to paramilitary work?
	* No issues with force-users
	* Need mercenaries willing to do difficult work
		* Core looked in a war costing everyone
		* Rim is forgotten

* Preliminary mission to eliminate local Hutt responsible for criminal elements
	* Believed to be responsible for the growing pirate attacks
		* Unemployed easy targets for illegal slave trade to frontier worlds
	* Trying to solidify influence, temporarily relocated to the planet
	* Staying in summer palace on the northern shore
		* Rudimentary Black-sun protection
	* Private bounty
		* Up to 12,000 cr., 40% in brokerage fees


### Rooda the Hutt
* Rooda Ganna Toba
	* Green with gold tattoos
	* Came personally to shore up the Kajidic's influence
		* Believes the Nokkos are responsible
			* Connected the K'zzak's back to them
	* Unwilling to negotiate before life is endangered
	* Will negotiate for escape after threatened
		* Offers money
	* No relation to pirates, beyond current privateers targetting rival interests
	* Profiting off illegal slave trade

* Ganna Kajidic
	* Declining in power
	* Only major holdings are in Rim sectors
	* Believes losing power due to the K'zzak mercenaries working for the Nokko Kajidic

* Staying in Villa Edion
	* Luxury palace on the northern coast of the Dona sea
	* Yacht port
	* Dock
	* Throneroom
	* Baths
	* Kitchens
	* Corellian Whiskey Cellar
	* Security area

* BlackSun protection
	* Defensive turrets
	* 2 Cpt
	* 12 Lt
	* 30 Merc
	* 6 Trandoshan shock-boxers

## Phlegethon Station
* After mission, VP Duskrover offers long-term employment
	* Majority of fleet operations and dispatches in the nearest three sectors are launched from Phlegethon Station
	* Mooring and access to bounty boards provided
	* Solar Fate can be transported to rendezvous at the station

* Phlegethon Station
	* Combined venture by several shipping, transport, mercenary and bounty-hunting units
		* Additionally several quasi-legal groups
		* Nearly all backing from ZC groups
	* Built in a rogue asteroid hollowed out (~ 100 km)
		* Large primary docking corridor
		* Six secondary docking corridors
		* Roughly 30000 employed in various roles
	* Hosts hundred of ships
		* Headhunters and StarVipers for fighters
		* Interceptor IV frigates
		* Upgunned TZ-86 and RZ-52 transports running goods as Q Ships
		* 2 Nebulon B Frigates Moored 
		* Keldabe-class Cruiser *Flame of Tridai* undergoing a refit in the primary bay
	* Shipments of modified B-1 droids arriving later today to serve as ship-board marines

* To be met by "Cmdr. Kandria", actually Sister Terrano
	* Sister Terrano - Reluctant Assistant
    	* Human
    	* Somewhat young, long, braided brown hair
    	* Social, civil, thick-skinned
    	* Currently leading flights in the area
    		* Including assaults on Imperial facilities
    		* Instructed that the party should take part in it - powerful
    * Suspicious of party - familiar?
    * Welcomes them, provides accomodations
    	* Investigates their background, finds links to incident on Kirusia
    		* Believes related to the death of sister Samuul
    	* Next day will attempt to kill them
    	* Sends trained BHs
    * Will attack directly if no other option
