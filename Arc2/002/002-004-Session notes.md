* Receive message back
  * A is interested
  * T cautious
* Agree with missions
* Suspicious of oncoming craft
  * Message back
  * Turn on lights
  * Meet pickup
  * Sink ship and board
* T messages K-17
  * "Is the ship ready 'cus we need it for our next mission. Going well. xoxoxo"
* Land
* Eat melons
  * B and A reminisce
* Head to port
* Board the ship, talk to Asky the pilot
* T messages Uema over secured pad
  * "Leaving. Funds?"
* A and H play djarak
  * Draw, H, H
  * H wins Navid's necklace
* T watches the news

Trip there
* H remice on necklace
* B talks to H and meditates
> A practices game and messages Nasra
  * Brag about killing Hutt, Wants to learn more of their ways
  * Asks to meet soon
* T wonders if on the right path
* Dream
  * H readies self and prepares self
	* Maybe feels remorse?
  * A gets saber, tries to manipulate crystal (???)
* A hangs with the captain
* A and B play djarak
  * A, A, A, B
* T worries to H about A, H rebuffs -> All going through things

Arrive
* H impressed
  * A similar
* T dislikes Terrano
  * A wonders if she knows her
* Worried, Feel threatened
* T realizes NS
* Decide to go into the room when the troopers show up
* T advises on threat - fearful
  * High alert

* NS is angry, but do not get along
  * Makes do with consortium
* Seris (M Duros) delivers access cards
* Camasi takes them to the bar (silent)
  * A sits in between other people
	* A starts drinking heavily
  * T and H sit and drink
	* Tensi the bartend tells what's going 
	  * Stops short with mention of Kandria
* A has outburst at T
  * Call in Seris
	* Offers a tour
	* Talk about origins of the station
* Sleep

* Seris come back
  * A applogozies for being rowdy
  * T appologizes for getting her wound up
  
* Talk to Kandria
  * Why here
	* Help others
  * Why killed Samuul
	* Defence
  * Why hunted?
	* Hurting people?
  * T, A, argue morality
  * No reconcilliation

* Solar fate just arriving
  * Message Uema lets them know they are leaving
  * Decide to leave
  * shoot the guards
  
* Sar finds map
  * Timm'eh gets lost
  * Seris appears
	* A says to let them leave
  * He draws too fast

* Grav chute off the cliff
  * H makes it
  * B lands short, but clear path
  * A and T land way short, ~ 1 km
  * Seris lost track
  
* Kandria fires but misses
  * Baldyr throws self up
  * Asori uses 1C to hide person
  * Drop Kandria, removing her left leg
  * Seris arrives, organizes attack
  * Pull Kandria aboard, blows self up and injures H, T, Skib
  * Ha'am heals her...? Fails
  * K-17 scans commander
  * B takes Skib to the medbay

* T threatens to kill Kandria
  * Not directly successful

* Ship takes fire
  * Peel off
  * Escape with significant damage
  * Unable to repair

* A investigates the NS -> tattoos, but can't read
  * T also -> no idea
  * T and H meditate on LS
	* LS crystal is synthetic -> made of ichor
	* LS kinda crude
	* 1 DS for Timmeh
* A and B perform sentimental cremation rites

* Talk with everyone
  * Mathias doesn't know what we are doing
  * B thinks fate
  * A and H think need a target
  * T split
  * Want to hunt NS
	* Maybe Consortium as well, but only to target NS, really

* Message Duskrover -> Be careful

* Message Baub -> we are on our way

* Going past Ashur -> To Iridonia -> To Ventooine

> Check on Astromech for capital
> Check on whether Secret Objectives accomplished

 

* 3 RP on way to station 
* 5 for figuring out identity of Commander Kandria
* 2 for investigating motivations of ZC
* 2 for slipping the trap
* 1 for sweet grav-chute
* 4 for killing Sister Terrano
* 2 for damage on *Flatline* frigate
* 5 for clean escape
