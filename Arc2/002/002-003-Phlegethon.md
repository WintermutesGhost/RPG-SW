## Intro Message
```
FROM: ▘▞▗▝▜▚▙
TO: ▜▟▜▞▖▚▚▘▚▛
SUBJECT: ▛▙▟▛▙▜▙▗▙▞▟▟▝▘▟▗

Congratulations on the successful operation. We are confirming completion of the objectives and expect to release payment shortly. Requested transport has been despatched: hold position for rendezvous within the hour.

-

Your work so far has been exceptional, and I would like to invite you to take part in the Phlegethon project--a joint venture to establish independence and security in the region. We can provide safe mooring, bounty brokerage, and varied work at your discretion, including with those equally talented individuals. 

I am unable to provide additional details without confirming your involvement. Reply if interested and transport will be arranged accordingly.

This is an opportunity to shape the future of the Rim, and freedom for those that live here.
```

### Positive reply
```
FROM: ▗▝▜▚▙▚▘▚▛
TO: ▜▘▞▟▜▞▖▚▛▙▟▛
SUBJECT: ▙▜▙▗▙▞▟▟▝▘▟▗

Thank you for agreeing to join us. I look forwards to working alongside you in the future.

A shuttle will be waiting in the Lamos City Starport to transport you off-world. Astrogation coordinates will be provided to your crew to meet you at Phlegethon Station with your ship.

Commander Kandria will meet you there and provide details about the services offered. She also coordinates the more interesting operations, and can help you find something suiting your talents and interests.

Contact me when you are next on Elrood so that I can buy you a drink.
```

### Negative reply
```
FROM: ▗▝▜▚▙▚▘▚▛
TO: ▜▘▞▟▜▞▖▚▛▙▟▛
SUBJECT: ▙▜▙▗▙▞▟▟▝▘▟▗

I am sorry to hear that, hopefully we will have the opportunity to work together in the future.

A shuttle will be waiting in Lamos City Starport to transport you off-world.

Contact me when you are next on Elrood so that I can buy you a drink.
```

## Storyboard
### Lamos City
* Hard to distinguish from Kirusia
	* Significant manufacturing base
* Suffered badly from the loss of manufacturing
	* Smoke from last night's riots still visible in business district

### News report
Delivered by a gray-suited reporter for ECN.
```
The governor's office has confirmed reports that the notorious crime-lord "Rooda Toba", known as commonly as "Rooda the Hutt", was killed in a before-dawn raid by PDF forces. 

Sources have said that a warrant was issued to search the Hutt's villa near Dona Nova early this morning when a firefight broke out between the PDF and the Hutt's private security. Several mercenaries and the Hutt themself have been confirmed dead in the aftermath.

Rooda had been implicated in the rise of piracy and organized crime in the sector, though the Hutt denied this, claiming to be an important investor in the sector's development. The Corporate Sector has so far been unwilling to act against the hutt without further evidence of wrongdoing.

When asked what triggered this raid, and what role the CSA had in the investigation, Governor Horne's office released a statement saying:

"Elrood is the keystone colony of the sector, and we have a responsibility not only to the people of our planet, but all the people of the sector. We cannot wait for the Core-world interests to support us. Elrood stands proudly on the Rim, and I will do whatever is needed to protect her future."

Despite accusations, it is clear now that the Governor's office and the PDF have been working tirelessly to dismantle criminal networks on Elrood and across the sector. While unable to provide more details, we expect similar decisive actions in the future.
```

### Dreams
#### Asori
You dream of a barren world beneath an eclipsed star. Flickering warmth emits from the campfire before you, shining a warm orange glow over the dusty stone steps around you.

In the darkness oposite you sits the wyrm, Tenebrous.

"Once again, you stumble recklessly into a den of vipers, I suppose it is your nature to place yourself in harm's way. You must be wary of the servants of darkness--you are a beacon, drawing them from their crevices and caves, driving them to hate and destroy. You burn brightly, and many fear you for that. You are certainly capable of defending yourself, but is it enough?"

"Your weapons must suit themselves for the task at hand. While shadows retreat from a torch, they cannot truly be slain. So same you can drive out the darkness before you, but you cannot destroy it, nor take its strength."

"I will lend you the power you need, to walk the border of shadow. To sup upon its strength without being consumed. With that power, you will be able to hunt what you will, and protect yourself from the machinations of others. Draw forth your sword."

You draw forth a silently gray blad from its sheath, a translucent flame extends from the hilt. The light of the campfire does not seem to shine on the blade's surface.

"You have asked how I will help you, and in your hand is a tangible contribution to your survival. Use this to strike down your foes that I can share more of the strength that we gain."

-

You awake chilled in your room, the campfire is gone, but the freezing wind remains with you.

#### Baldyr
You dream of a cool, misty day in the jungles of Thila. Sitting on the stone steps near lake Sut'tung, you can see distant bone spires piercing through the canopy, pointing upwards towards the clouds. You do not recall them being there when you left Thila, but it seems they had always been there.

Like shards of glass, high-pitched birdsong pierces your ears. Near your feet, the firebird Eos pecks at a nut lying on the stone.

"Spirited child of the Druma'san, you honour yourself by your efforts to help our destined child. You give so much that she might succeed."

The bird halts its pecking, and locks its eye on yours.

"But you must do more."

"She slips further from the Druma'san ways. Each step takes her away from her destined path. Ours are the sacred ways, you must remind her. Bring her back to our embrace."

"I will grant you the strength you need. You must find a way to reach her."

As the voice quiets and your thoughts return to you, you remember Sar, and the crystals, and the darkness they held. When the others had explained what happened to Sar you didn't understand it, but you wondered maybe if they might help Asori. Even now, though, you cannot conceive of how.

As if listening to your thoughts, the bird speaks again, driving the image from your mind.

"Yes, the crystals. The energy they hold. The child of stone. Maybe the darkness can be drawn out. Venom from a wound."

Eos' tone is imperative, determined. Gradually, its shimmering feathers seem to be shining brighter.

"Yes. Give him the talisman, the child of stone. A connection to the Druma'san. A fragment of ourself. Give him the talisman, so that we can reach out to him. So that we can use the crystals, draw on their energies. Pull the darkness from Asori."

Like the sun, you cannot bring yourself to look directly at Eos any more. You feel its warmth on your skin, and the bright light drowns out other thoughts and sounds.

"We will work through you. Strengthen you. Drive you. Use our power."

The searing light stings your eyes and skin, and resonates through your mind. It encompasses you, illuminating every aspect of your being.

"Go, quickly. Time is short. We must not lose the destined child."

-

You wake with your eyes still stinging and Eos' hum in your ears.

#### Ha'am
You dream of a barren world beneath an eclipsed star. Flickering warmth emits from the campfire before you, shining a warm orange glow over the dusty stone steps around you.

In the darkness oposite you sits the wyrm, Tenebrous.

"Once again, you stumble recklessly into a den of vipers, but I suppose it is in your nature to seek others to help regardless of the cost to yourself. How much would you be willing to give, to help those you most care about? I wonder what sacrifice is too much. Always willing to give of yourself, give to those around you. You are the consummate healer." 

"But what do you do when the wounds are too deep? When your skills will not suffice? What of those you could not mend? Inevitably, all your companions will suffer that fate, stricken beyond your capacity... but there are deeper wells you can draw upon. I can lead you to the powers you need to heal any trauma, but you must help me to regain my own strength."

"Today you can manipulate the ley-lines running through a being, mending them and restoring their strength... or severing them, perhaps. But I can teach you more. I can teach you to pour your own strength into another, imbuing them with the force of life where their own has failed. You might also draw out their energy... or perhaps their curse. You will never be able to mend your young student, but perhaps you can extract the venom."

"Your teachings have limited you, and shackled your capacity as a healer. I can teach you the full extent of your abilities. Open your mind, and feel the flows underlying 'life'. Share in the energy of others, and I will share my knowledge with you."

-

You awake chilled in your room, the campfire is gone, but the freezing wind remains with you.

#### Timm'eh
You dream of a smiling Val-Isa standing before you, not as a hologram, but a spirit-made flesh. Her matronly face shines brightly in the dawn sun, and she smiles warmly as you approach. She speaks with an unfamiliar voice, but it rings deeply in your soul.

"Honourable Timm'eh, I am glad to speak to you after all this time. Our paths are so distant, yet I can see you here today. My time is short, but I want to thank you for following the path of light. It may be difficult at times, but you always oppose the darkness."

"I worry for the others, though. A shadow has settled over their hearts, and they stray from the teachings. The pull of the darkness can be too much for some, and you must help them. Asori walks a destined path, but she tempts her fate by dealing with dark forces. Please, do what you can to help her."

A deep sadness washes over Val-Isa's face, and she steps closer, hands held forwards imploringly.

"But be careful. The claws of the darkness are gripped around her heart. A shock or careless word might send her deeper into the void. I speak only to you, because I know you can help. You must be the light."

-

In a blink, you awake in your dark room. The light of the dream fades quickly, but you can still feel its warmth on your soul.

*__Dawn blessing__ - Once per session you may add one light-side force pip to your force pool instead of rolling force-dice.*


### Phlegethon Station
```
You check the viewports as the ship drops out of hyperspace in a remote system. A solitary rogue asteroid can be seen, half illuminated by the sun. Rocky and roughly circular from its current profile. Small constructions can be seen on the sun-side surface, while faint, blinking lights outline the other.

As you get closer, the size of the asteroid becomes more apparent. What appeared to be small ports and hatches grow into capable docking pads, then into sizable internal berths, then again into huge landing corridors. The characteristic spires of a pair of Nebulon B frigates are dwarfed at their mooring on the sun-side of the asteroid, and could fit easily inside the primary corridor.

Defensive batteries dot the surface, though only the largest turbolaser barbettes are visible from here. From the bottom sprouts a cluster of sensor and communication towers suitable for a star destroyer. 

On the approach, a flight of A-wing fighters in gray and orange livery form around the shuttle, escorting you to one of the bays. Massive armoured doors slide into the rock allowing entrance.

Passing through the dock's matter shields, you can see that much of the asteroid's mass has been hollowed out, and the access corridor you are in runs several kilometers into the rock. Dozens of ships on pads and clamps line the length of the corridor, with fighters and armed frigates nearest the entrance. 

The route you are on runs briefly alongside the primary corridor, and you can see a huge Battle Cruiser of unknown design stationed in a dry-dock, its port engine nacelle exposed and swarmed by grav-lifted workers.

Your shuttle touches down deep in the asteroid's heart, next to an exposed concourse running several hundred metres down the length. Numerous doors set into the stone lead deeper into the asteroid's structure.
```

* Phlegethon Station
	* Little record other than mining permit issued in the system for station LX-M503
	* Combined venture by several shipping, transport, mercenary and bounty-hunting units
		* Additionally several quasi-legal groups
		* Nearly all backing from ZC groups
	* Built in a rogue asteroid hollowed out (~ 30 km)
		* Large primary docking corridor
		* Six secondary docking corridors
		* Roughly 30000 employed in various roles
	* Hosts hundred of ships
		* Headhunters, StarVipers, TIEs, A-Wings for fighters
		* Interceptor IV frigates
		* Upgunned TZ-86 and RZ-52 transports running goods as Q Ships
		* 2 Nebulon B Frigates Moored 
		* Keldabe-class Cruiser *Flame of Tridai* undergoing a refit in the primary bay
	* Shipments of modified B-1 droids arriving later today to serve as ship-board marines
	* Fighters involved in the attack on the player's shuttle last run are present, could correlate the flight time/locations

* To be met by "Cmdr. Kandria", actually Sister Terrano
	* Sister Terrano - Reluctant Assistant
    	* Human
    	* Somewhat young, long, braided brown hair
    	* Determined, civil, thick-skinned
    	* Looking to prove something
    	* Wears Phrik-enhanced plastoid armour. Gray with orange stripe down right side. ZC logo over left breast
    	* Fights with LS and pistol-grip carbine, or HBR
    	* Force: 
    		* Provides mind defence
    		* Enhanced accuracy and defence
    		* Force jump
    		* Fear
	* Currently leading flights in the area
		* Including assaults on Imperial facilities
		* Instructed that the party should take part in it - powerful
    * Suspicious of party - familiar?
    * Welcomes them, provides accomodations
    * Investigates their background, finds links to incident on Kirusia
    	* Believes related to the death of sister Samuul
    * Will work with the party, if convinced - can do more for the sisterhood by making the ZC happy
    * Otherwise, will do anything to kill the party

### Attack on IF-937
* Imperial base on moon
* Fire support needed during landing operations
* Using new B-1s for attack