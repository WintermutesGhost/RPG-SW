A and B talk about why Asori cannot touch the Tyvokk
	* A doesn't think the spirits could speak to someone
	* B says that Jesma heard the voice of Eos before
A and H talk about Tenebrous
	* Why didn't H share it with A?
	* H fears A at her worst
B, A, H dream
	* A tells Ten that they were not betraying, just cautios
	* Ten accepts -> Needs to leave for a time
	* H is suspicious
	* B still seeking help of T

B talks to Grachaw
	* Tells B the story of Grikoo and the Tyvokk hunting Yarudar and the Hitca cloak
	
A and B analyze the Tyvokk
	* A's ammulet turns to glass
	* A suspicious
	  * B certain
  
T meditates on it
	* 6 - dark
	* Could see the universe
	* T does it again!
	* 2 - dark
	* Focuses this time
	* Can guide it
	* 3 - dark
	* Seeks selves
	* Also finds the defunct terraformer
	  * Tyvokk turns off

B connects, but it isn't searching at the moment

Head to the mayor
	* Attacked!
	* A also attacked back at the ship
	* All wake up
	  * T disbelieves
	  * A B H defeat attackers
	
A recognizes Ceneza
	* T is immediately suspicious
	* M collapses

A goes to regroup
	* head in to see the mayor
	  * Not happy, not listening to the stories
	  * Others feel unhappy about their actions
	  * Asks them to leave. Soon. Now.
	  
Back to ship
	* Head to the terraformer
	* Grachaw says Yakchackyyyska
	
Land in the forest, travel down
	* A PILOTS THE V-WING DOWN
	
Reach the builder forge
	* T connects to the Tyvokk
	  * 1 dark to power
	* Seek a Jedi temple
	  * Finds the temple on Katarr
	* H senses it
	  * 2 dark to connect
	  * Understands what it is for. What happened.
	  * Perhaps this planet is broken. Full of fear.
	  * Asks of the universe:
		> "Has the error on this machine put the planet into a state that was not intended?"
		* Yes
	  * H clears the error, it turns off
	
4  for surviving the attack
7  for understanding the starmap
10 for finding a relic of the infinite empire


A  H  T
   2  5  - Connecting to the starmap
2  1     - Terrifying attack by the NS
   3     - Disabled the terraformer
