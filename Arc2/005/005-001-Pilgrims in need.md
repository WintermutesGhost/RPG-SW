## Attacking fighters
* Under attack from: 
	* 1 wing of Starvipers (led by ace; MPQ:81)
	* 1 wing of V-Wing Alpha 3's (SoT:57)
	* 1 RZ-52 (modified for boarding; like Gozanti E-CRB:277)
* Reinforced by
	* 1 wing of Headhunters (E-CRB:258)

## Boarders
* Aboard the *Wrrlyyywiawhronk*
	* Helmed by Captain Grachaww
* Boarded by pirates
	* 1 squad of cut-throats
	* 2 squads of boarding droids (Modified B1s - shotguns and vibroblades)
		* Command B1 - Rockets and flamethrowers
* Trasporting pilgrims back to Kashyyyk for Life Day
* Stopped to pick up escape pods
* No doubt some have been taken as slaves
* Shipments of Orga probably stolen as well

## Tracking the pirates
* Have left behind multiple shipwrecks, and escape pods are drifting throughout the debris field
* Ship transponders can be traced back with some nifty navigation
* Debris field is disrupted around the station

## Recover supplies/disable station
* Abandoned Republic deep-space FOB
* Filled with ill-gotten gains
	* 12 captured Wookies
* Crewed by ZC
	* 3 squads of cut-throats
	* 2 squads of pilots
	* 4 squads of boarding droids
	* 2 squads of Z1 commando droids
	* Reinforcments from the *Ryloth Wanderer*
* Protected by 
	* TZ-86 Q-Ship (Like Gozanti E-CRB:277)
	* Interceptor IV Frigate *Ryloth Wanderer* (MPQ:81)
	* 2 wings of Headhunters on deck
	* 1 wing of V-Wings being serviced
		* Poorly mounted G-Seat
		* Disassembled weapon
		* Landing gear locked out
		* Engine flares
		* Overloading inertial dampeners
		* Engine false starts
		* 

## Escort freighter
* There is a servicable YT-1000 half-loaded with Orga
	* Hyperdrive is being overhauled
