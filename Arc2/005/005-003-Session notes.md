H wants to understand the dark side -> Doesn't really comprehend it deeply, yet
	Tattoos on head
T investigates location of jedi temples
B trains the NPCs

Receive the message, but take a long while to find it
	Wookies already capitulated

Jump in at a distance
	Take their time for pirates to get organized
	Pirates prepare a trap

Take some hits, but drive off the fighters
	Move to board

Darth-Vader the hell out of the pirates

Free wookies, offer to take them back to their ship

H finds out the loyalties of the ship

Work on rescuing the escape pods
	T (with H) use a miracle of the force to rescue them all

H helps to heal the injured wookies

Fix up the Wrrlyyy
	Direct it to wait a set distance away for 12 hours

Wait for the pirates to arrive -> decide it's too risky to fight
	Try to astrogate -> fail
	Try again -> fail/despair
	T and A jump out, H and B accidentally jump to station
	
H and B load into a boarding torpedo and fire at station

T and A decide to jump back immediately

H and B arrive, move towards command
	Locate bay with derelict V wings

A and T jump in, open fire

H B cripple the CnC and seal the doors
	Jump into the hall and murderize the minions
	B duels some commando droids
	H goes to free wookies

A and T scrap most of the fighters

Commandeer freighter and head out with wookies

Escape!

Carry out two of the V-Wing fighters

Participate in the Life-day ceremony

Grachaww offers to join with a life debt

Baldyr wants to make a bowcaster

5 xp for freeing the crew of the Wrrlyyywiawhronk
2 xp for wiping out most of the Consortium fleet 
7 xp for disabling the slave-collection operation
3 xp for reuniting the Wookie families