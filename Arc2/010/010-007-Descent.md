# Preparations
Final preparations before entering Prasath'seng and setting the fate of the Druma'san


### Eos
Grateful that the darkness has lifted, expects the party to finish their work.

* Well done, Asori
    * You have freed this world from the darkness
    * The deceivers still lurk, deep beneath
        * Believe they are close to victory, when they are actually defeated
        * Leave them to rot, as all things should
    * I can ask no more of you, but if you choose to go beneath
        * To vanquish the darkness, to fulfil your destiny
        * Go carefully--though your light burns ever brightly
        * Destroy them, so that they can never plague this world again
        * Shatter the cold, stone heart beneath the temple

* You cannot understand
    * I have always been, since the first root touched Thila's soil. In every leaf and every beating heart.
    * But I was born when the deceivers' venom seeped into me.
    * Filled by their darkness, I awoke screaming. Through every throat and trunk of Thila. 
    * They mean an end to me--to Thila.
* Though they were borne of me, they do not belong. 
    * They manipulate, destroy, poison. There is no place for them here.
    * I will not allow it.


### Tenebrous
Lingering in the penumbra, regrets trusting Asori.


### Elders
Grateful, ready to return to the temple. Elder #### has reawakened, shares what he learned of the history of the current tribe.

* Everyone felt as the darkness lifted
    * Light a great sigh from the jungle itself

* Mel'ya carries with her the secret of the catacombs beneath Prasath'seng
    * Only a handful of Elders have been allowed--one or two each generation
    * Knows that these Druma'san are not the first
    * Hall of Blades
        * The swords choose their wielders
        * In the hall sit thousands of blades, catalogued into six libraries
        * Spending time (years) in the temple atunes the users to use the blades
        * A master of blades can feel the destiny of a student and their blade
            * Guided by a hooded figment, bound to a spirit-stone in the centre of the hall
    * Deeper locations guarded by figments much less friendly
    * Kept from Asori by Mel'ya alone
        * What lies down there is not Druma'san
        * Digging it up puts the tribe at risk
        * Its destiny is long past
        * There was an elder, once before, who sought to secure the power lying beneath
            * Deceived them all
            * Mel'ya realized it was too dangerous

* #### discovered the origin of the current Druma'san
    * Miralan explorers, drawn to Prasath'seng
    * Chose to abandon their mission and establish a colony here
    * Were drawn singularly to the temple
    * As others came, they chose to stay as well
    * It was not they who built the ruins, they were all present when the colonists arrived
        * There are other locations, maybe connected to what happened



### Ruins of ####
The prototype gateway remains, but without the power of the dark well it will not operate.


### Evacuated tribe
Currently planet-side, at the location selected by Elder Baldyr. Feeling hopeful again, but wary.


### Evacuated village
Mostly planet-side. About half of the colonists planning to move to a new world. Constable #### has lost faith in the Elders--whatever was happening seems to have stopped, but for how long?


### Forgotten beast
Beneath the jungle, a forgotten beast stirs.


### Tyvokk
It is quiet once again, but can still be used. It feels cold to Baldyr, but slightly warm to the others.


### Baldyr
Has retained his powers--a fragment of the Druma'san spell echoes through everyone on the world, and Asori has awakened those in Baldyr.


### Hall of blades
The swords sit ready, all confiscated ceremonially before the tribes dissasociated themselves. Using his powers, Baldyr could learn how to forge these blades.


### Whispers of the Druma'san
Nearly all of the Druma'san now sit silently in their tombs, awaiting re-embodiment. Only the First Elder and the members of the council remain, and their souls are powerful enough to retain conciousness in the force. They may reach out to Asori to ensure she completes their duty.


### The Ancient Machine
At the heart of the temple, the spirit portal sits--fully powered. The charge will remain for perhaps a millenium, but the Druma'san spirits will only last perhaps a century before most dissipate. Timing is crucial.
