Generally positive atmosphere
A explains the dream with Tenebrous
  T wants to go searching for Tenebrous
  H cautions -> Reason H avoids using the force
B and A discuss the nature of Destiny
Arrive at Thila
Greeted by the constable
  Meet elders in tents
H is worried about other people of Thila disliking Asori
Hear about wildlife and temple issues
B pushes onwards to the temple
A checks on the pull of the force
  Light, wrmth personified
  A cold wind
H isn't sure why A ever left
  A - m,ake sure what heppened to you didn't happen to others
  H - lets make sure it never comes here
Elders explain the state of things
  Arguing over staying vs building a new temple
Leaves the Druma'san codex with the other Elders to study
B goes to get A
Decide to seek out those afflicted first to examine them
  No dice, but find herbalist
Go to retrieve the Tyvokk
AHT go up to the temple
  T feels dark energies
  H thinks it's the same sand
  A has visions of the sand turning to glass
  T senses the bird and the spriti of Eos
T thinks the Tyvokk woke something
  H wants to destroy it
Resolve to enter the dark temple to retrieve the Elder
  Gather strength
  Eos grants protection
Step inside
  split up to search for Kom'sara
  B and T break into his office
  A and H find him unconcious
  A picks him up
  H grabs documents -> spots a battle-phantom
  B grabs more documents
  T tries to reach the others -> hears a wraith cry
  H fails to reach others
  A leads back out -> spots a battle-phantom
  H stabilizes Kom'sara
  T blasts a phantom
  B breaks a door and heads for a window -> spots a battle phantom
  T and B escape
HEad down the hill
  Spot Eos
  Hear howl of hunt-hounds

Kom'sara left with the herbalist
Baldyr invited to deliver a speech
  Agrees
B tried to convince the others of the importance 
  Fails to convince -> appreciate A's help
A studies Kom'sara's work
  Find out there were many maps among the work
  Looking back through ancient stories and ancient maps
H helps Tep with treating Kom'sara
  Suppresses iwith 1 pip
  Detects some dark contamination from within Kom'sara
  Leaves it for now
Elders deliver a speech
  B says will ensure that safety/sanctity will be restored
  With help of the warriors of lightd
  Manage to calm the tribe
Organize sleeping arrangements
  A sleeps outside
  B and H sleep with Druma'san
  T sleeps in inn -> Jado the keeper
T studyies the maps, gets an understanding of the region
A easts mangoes with B
Uxibeasts attack!
  T moves to engage!
  T mashes them into a bloody pulp (Conflict)
  H assists
  B and A arrive a bit late
  B knocks one out
H magically saves a hunter
A tries to see what caused it
  Nothing... I guess
Townsf wonder about cause
B notices the creature is sick
  H confirms -> dark sickness from inside
H does not realize what caused the carnage
Decide what to do with the beast
  B wants to restrain
  T wants to destroy
  H wants to restrain
  A wants to restrain
  B willing to take responsibility


Wake
A finds dying mango tree
H talks to hunters from last night
  A bit traumatized -> unable to raise spirits
  Constable is thankful for their help
  Might get Saar to come talk to them
B discusses with the elders
  Vaals want more patrols
  Jesma says no to intrude
  B supports 
A checks on the temple -> still spooky
  Returns to speak to the council
  Asks to speakk about her powers
T checks in on everyone
  Has breakfast w T and A 
  Goes to buy a plasma engraver and garotte
A speaks to the council
  Recognize the difficulty of integrating to the Galaxy
  Always recognized she was destined for greatness in the Galaxy
  Tries to speak to them of spirits she has met'
  And of the creatures in her dreams
    -> Mel'ya knows something
  Why have you returned -> to find answers, deal with a darkness
  Send B to help, find the spirit, offer any help
PLan what to do -> Tyvokk in the temple?
Go to the uxibeast
  Charges B
  T holds on
  Move the Tyvokk around - it's chasing B
  touch it with the Tyvokk - Nothing
  Calm it
  B notices the disease
  T notices pain, anguish, anger
  H draws out the darkness from the beast - at ease
A asks a question
  Is the Tyvokk important for removing the darkness from this planet? -> yes
T and H begin researching Kom'sara's work
  With Codex + teacher Cislso
A and B go to stroll in jungle, looking for Eos
  H gives beast call
  B listens for something - nothing
  A strolls to the temple, ominous - feels Eos' protection
  B freaks A out by saying that she might lead to the destruction of the Druma'san
  A decides to go alone

B standing atop the hill
T and H continue research
  Make progress! Find a picture of Prasath'seng
A trecking throuh the ungle
  Perceives a creature nearby
  Tries to spot it
    Succeeeds evenually
  Spots the war hound
    No fear!
    ignites sabers
  Loses sight of the creature
  Sees the creature is not affraid
    So makes it affraid
    Waves sabers and drives it off
    Sneaks away, following Eos
B grabs mangoes
  Notices dying tree
  Tears it out and burns it
T and H meet the returning hunters
  T clashes with Va'ko
  H heals the injured hunter
  T helps too, I guess
  Tep wants to learn too!
    -> combat healing
  WArns B of the danger
T patrols
  Spots B's fire
H calls B back
  mentions A is away
  B goes to follow
T hears skittering noises
  Assemble!
  H nukes them + others help
Not sure how to handle threats like this
H radios A about the panthers
  Already attacked by panthers!
  War beast arrives and helps
  Drives off the panthers
  A orders it to leave... it does
A speaks to Eos, learns of her destiny
  Summons the sword
  Dismisses the sword

**XP**
5 - Rescuing Elder Kom'sara
2 - Recovering Kom'sara's research
4 - Defending from Uxibeasts
2 - Healing an afflicted beast
3 - Surviving the hound/Defending against the green bugs
3 - Asori found Eos

**CONFLICT**
A  H  T -  
1  1  3   Exposure to the temple's energies
      2   Slaughtering the Uxibeast
   2      Choosing not to heal Kom'sara
1         Fight with Baldyr :'(
1         Drawing uppon new powers