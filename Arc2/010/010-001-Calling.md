* Asori wonders "Why is it always me?"
  * Tenebrous reaches out "You have the spark of change"

```
"Why is it always you? A question perhaps you already understand the answer to."

Tenebrous' voice echoes around you, though the wyrm itself sits unmoving before you.

"Others can see it obviously in you--and perhaps finally you are beginning to see it in yourself."

"What was once the spark of initiative has now kindled into the fire of change, of transformation. The power to shape the worlds around you. The power to impose your vision, make will into reality."

"Wherever you go tyrants fall, planets burn, destinies shift."

"Many wish to use your fire to their own ends, I am among them. Selflessly. Selfishly. To create. To destroy. Perhaps you think it unfair that we seek your light? Yet you hold great power over others, the cost of that is the flocking moths drawn to your flame."

"A time will come soon when I ask something of you, but first you must come to terms with who you are and where you came from. A destiny long preordained is about to come due, and the forgotten ancients cry out from the deep black."

"All are clamoring for your help, you would do well to guard your flame jealously."
```

* Eos warns Baldyr of the coming war
  * Asori's time is now
```
You have become accustomed to the dreams of Thila each night. A calm respite from the galaxy you are not fully comfortable in.

Over the past nights, though, the jungle has begun to shift. A fell wind blows from the West, carrying the faint smell of smoke. It is unusually quiet with only the occasional, fearful cry of a distant animal. The sky remains overcast, like the calm before a summer storm.

Eos sits on a branch nearby. Of late it has been away more often than not, leaving you alone with the anxious jungle. On nights when it sits with you, it has done so in silence.

Tonight, though, it speaks:

"You have done well, child, Baldyr. Truly you are of the Druma'san."

Another gust of cool wind rushes through the jungle and in the distance you hear the rumble of thunder.

"You have protected Asori... you have trained her... but there is no more time."

"A great trial draws near and Asori is key. She must safeguard the future of the Druma'san."

"Guard her. Prepare her. She must be ready to fulfill her destiny. Only she can draw out the venom. Destroy it."

"A warrior of light, to stand against the manipulators. Against the dark. She must know what is at stake. Her conviction wanes, but she must stand strong."

"If she falters... if she falls, the Druma'san fall with her."

"She must save us."

After a moment the bird takes to the air, gliding down to the valley before banking and turning eastward.
```

* En-route to meet Nasra, currently deep in hyperspace
  * Regular duties need to be carried out
    * Training
      * Mathias
      * Squib
      * Sar
    * Meditating
      * Distractions
    * Cooking
      * Grachaw bored with human-compatible food
      * Therm-zapper broken
    * Servicing
      * Malfunctioning navicomp
      * Water leak in the fresher
      * Knocking sound from the port plasma centrifuge
    * Medicating
      * Squib comes in with a broken arm
      * Sar has the flu
      * dealing with excessive servo wear in new legs

* The artefacts to revivify the Druma'san have been assembled
  * Tyvok - Power
  * Sword of Destiny - Key
  * Spirit of the Druma'san - Connection
  * Alignment - Time
  * Ha'am - Life

* Minds of the Druma'san reach out to Asori, permeating the ship
  * Tyvok more active
    * "Keeping the Tyvok has been little more than a nuisance so-far. The others treat it with great care, but it seems like little more than a black stone to you... though you have learned to not leave anything fragile out lest they slip onto the floor while you are out. Certainly less troublesome than the Lothcat. -- Today, though, you enter your room to find it turned over. All your possessions have been pulled from the cabinets and drawers, latched and locked compartments hang open with their contents spilled all over. In the centre of the room sits almost everything you own, heaped into a crude imitation of the altar in which you first found the Tyvok, with the black stone now resting near the middle, atop a wadded-up, grey linnen. -- There is a smell of ozone in the air."
  * Asori stands before a council of 11
    * Swear fealty
  * Visions of the tomb
    * Raised tomb in a ring of eleven sarcofogi
    * "You stand atop a raised platform next to a short, black tomb laid along the floor. Around the edge of the room, eleven sarcofogi stand judging. Each of them is unique, and covered in intricate runes. Small, sharp protrusions distinguish each from the others. The tomb itself is smooth, cold, and expectant, carved directly from the stone of the floor beneath you... or perhaps pulled from it. No cracks or seams mar the surface of the tomb, except for a small notch in the centre. You can feel the sarcofogi waiting. Expecting. Imploring. There is a bitter taste in the air."
    * "Above you a faint light filters through a hole in the roof. Alien machinery breathes slowly above, and pulse of the sleeping tomb can be faintly felt through the floor."
  * Sarcophagi filled with black sand
    * Pours out over the one who opened it
    * Inside, a figure, sleeping 
  * Structures of bone
    * Sprawling cities among the canyons
    * Barren terrain, choked my smog
  * Beating heart beneath the sand
  * Roaring forges
    * Fire of creation burning brightly, but no heat
    * Weapons and beasts are being shaped
  * Beasts of war
    * Black creature of bone and sinew - Asori once faced one
      * Recognizes Asori
  * Cries of the Druma'san
    * Incoherent rumble
    * Rises to a deafening caucophany
    * 100M voices begging, demanding, hoping--hard on the psyche
  * Golem from the Tyvok (if Asori touches it?)
  * Entity formed by the sand of the Druma'san ammulet
    * Sand has formed a black, liquid glass
    * Bottle shatters, glass runs off
    * Disappears through the ship
    * Draws life out of people on the ship
      * Returns each night to Asori
      * Product of Druma'san alchemy
      * Leaves a mark on Asori
  * Ship begins to change, Druma'sa alchemy causes transformations
    * Illusory or real?
    * Panelling becomes bone
    * Couplings become crystaline
    * Runes appear on the walls
    * Beating noise from the reactor
    * Patches of sand behind the panels


* Asori is awakened to her connection to the Druma'san
  * Sword of destiny is empowered
* Baldyr is summoned back to Thila
  * "A letter, from First Elder Mel'ya: Elder Baldyr, There is ugrent need for you at Prasath Seng. Something has changed in the temple and I have called a meeting of the Elders. Come immediately."


## Cursed
* Must find a way to make it to Thila while Asori channels unknown powers
  * Dark power floods out of her.. but she is not the source
  * Power can be *suppressed*... but is constantly flowing
  * Can be controlled... but she must remain focused
  > Engraved across your chest: a strange tattoo, one you never received. Familiar, but hard to make up from where you stand. Looking in the mirror you see a pattern of triangles and sharp, sweeping lines in intricate patterns that spill across your front and down your arms.
  > You look away, and in catching a glimpse of the tattoo from the corner of your eye, you notice something: the centre of the tattoo resembles the symbol of the Druma'san--but subtly wrong. The lines are too sharp, the shapes curving agressively, triangles stretched like claws. You can scarcely make out the Miralan markings: Warrior of Heart.
* Four days to reach Thila
* Two forces are searching/calling to Asori:
  * Light-side entity from Thila
    * Eos - Spirit of Thila
    * Summoning home the prodigal daughtes
    * A warm, familiar, welcoming  light
    * Vast, incomprehensible, uncontainable
      * Impossible resilience check
  * Dark entites from the void
    * Cries of the Druma'san
    * Incoherent rumble
    * Rises to a deafening caucophany
    * 100M voices begging, demanding, hoping--hard on the psyche
      * Impossible fear check


### The Ship
* Ship is affected, transformed by Druma'san alchemy
* Ship begins to change, Druma'sa alchemy causes transformations
  * Illusory or real?
  * Panelling becomes bone
  * Couplings become crystaline
  * Runes appear on the walls
  * Beating noise from the reactor
  * Patches of sand behind the panels

### The Dreams
* People are affected, as dark thought press onto them
* Sarcophagi filled with black sand
    * Pours out over the one who opened it
    * Inside, a figure, sleeping 
  * Structures of bone
    * Sprawling cities among the canyons
    * Barren terrain, choked my smog
  * Beating heart beneath the sand
  * Roaring forges
    * Fire of creation burning brightly, but no heat
    * Weapons and beasts are being shaped
  * T dreams of the Holocron

### The Ghosts
* Spirits intrude, alien minds lost to the void
* Golems and worse are summoned
* Lost spirits are drawn to Asori's powers

### Day-to-day

#### Day 1
* A
  * Every morning on waking, runes cover the walls
    * Not visible on electronics
* B
  * Something wrong with the ship
    * Shaft bearings on engine 3 seconray compresor showing wear warnings
    * Replaced last week
    * Hard, black sand has gotten into the bearing 
* H
  * Lothcat acting up
    * Sar notices it hasn't been around
    * Refuses to leave H's room
* T
  * Illegible runes on the control panels
    * Unable to run scans 
* Communications failing
  * Widespread interference
  * Communications panels throughout the ship have fused


#### Day 2
* A
  * Dreams of the council of Druma'san
  * Unable to get into cockpit
    * Mysteriour rune on the door
* B
  * Dreams of a beating heart beneath black sands
  * Can sense dozens of judging eyes on you
* H
  * Dreams of a sprwaling city of bone
  * Grachaw comes in with fractured ribs
    * Refuses to say what happened
    * K-17 recalls him crying out before knocking a plasma housing off the grav-lift
    * Saw an apparition, too proud to admit being afraid
* T
  * Dreams of roaring forges
  * Holocron will not start

#### Day 3
* Systems lockout
  * Only a short time available to restructure the code
  * Must be done from the main computer core and command core at the same time
* Druma'san retrievers
  * Emerge from strange geometry
  * Fear check to attack
  * Stalking through the corridors
  * Do not attack Asori... but may ambush others
* Failing power couplings to the hyperdrive
  * No more spare parts

#### Day 4
* Druma'san spirits
  * Not aggressive... but are harmful
  * Fear checks every round
  * Fought with willpower, not stat
  * Not visible on electronic systems
* Golem
* Reactor shielding ablating
  * Will become dangerous soon
    * Requires significant repairs
    * Extremely challenging in-flight
  * On inspection, the material has begun to change


