Continuing to travel
H asks T whether to tell B about the dreams?
  -> Leave it to A if she wants to
Discuss how the ship has been renovated
T enjoys a morning coffee
B meditates in own room
H is always looking for leads
  Nothing on official channels
  Rebellion doing well enough
  Piracy up or down on the rim
  Investments continuing
A is scanning the galaxy for hazards
  Currently en-route to Xani station in the outer rim
  T tries, half-heartedly to cheer her up
T talks to Val-Isa about time travel
  Mediations took us somewhere else
  Learns about the future
H goes to bug A as well
  A doesn't want to be around others
  Doing science!
B trains Sa and M
  M picks up B's technique
T and H discuss the A problem
T talks to M about the Miraluka
  And about seeing the future
G disturbs A
A goes to meditate
Then goes to examine the sword of destiny
  Older than the others
  Worn despite being well cared for
  Plain, no markings
  Feels a deep connection back to thila
  Does katas -> visions of the tomb
Sa and H clean up
A reflects on the happy moment
  Goes to speak to K
  Asks about K's memories
  Bond over mortality
  M overhears
B returns to find the enrgized Tyvok
  Calls T
  First time this has happened since Kashyyyk
  Cleans up and leaves sword outside
  H and A show up
  A offended
  B goes to help
    T tries to stop... realizes better than to try and stop B
      B has nothing to say
      A says it's fine... B believes him -> just needs to be polite
  T and H decide to try and do something with it
    B comes and wraps it up
  H reminds T that only B will touch it
  B locks up the Tyvok
  Ask K to install a monitor
     Leave T's remote watching it
A dreams again of the tomb
  Does not recgnize, but is not fearful
  "This is not Druma'san" - Trepidation
  Realizes the sword of destiny is the key
  "Asori Ta'am of the Druma'san and I will not submit to fear"
  Inserts the key
A goes to visit H
  A is concerned about own stability
  Worried about losing control
    Remembers dream where killed B
B checks on the Tyvok
  B and T discuss how to store it
  H storms out
  A follows
  Discuss whether it should be destroyed
  A goes to tell T how to test safely
  Tells B that it will be destroyed if it goes badly
    B confides that it is essential
  Agree to wait until somewhere safer
H patches up Sq
  Goes to tell K that Sq if off of duties
  Vision of a sarcophagus
T wants to search for the rest of the Tyvok
  Meditates on it
  T brings H for safety, says to leave A out
    Too risky
  Channel H's force into the seek
  Feel the phenomenal search of the Tyvok
  It too is seeking where it belongs
  H -> put it out of its misery
A feels a tug of the force
  H says T is searching for answers
T wants to connect with the Tyvok 
  -> it must be searching for something for A
  Agree they want to help A
A dreams of the Druma'san council

B tried to understand the Tyvokk using the Druma'san codex
  Doesn't seem to have a relationship to the Druma'san
A and T go to the cockpit
H wants to devise force-science to understand or affect the Tyvokk
B notifies them of the summons
  Agrees to drop B off at Thila
    Tyvokk is too important
  B thinks A must not return to Prasath'seng
    Not sure whether to leave the Tyvokk
  A has issues with navigation - weird symbols 
    Grachaw reboots the systems -> it's fine
B jokes with A -> fails
  A is upset
H is doing force science
  Sets up a 'force-detector'
  T is watching -> sense life
  H uses force heal
    1 conflict as energy flows in to Tyvokk
  Detect the increase of activity in the Tyvok
    No presences or entities
B reports for shift in engineering
  Breaks the bearing, finds the sand
A notices issues weith the computer again
  Calls K-17 nto take a look
H is testing Suppress on Tyvok
  Shut it down
  T tries to sense it -> failure
Decide to leave the supress on
T checks the sand from the bearing
  Not the same as the Tyvok
Un-suppress the Tyvok before bed
  Move it back to Baldyr's room
  Under heavy surveilance
T tries to chear up A
  A takes it badly
OUBURST (GIVE ASORI CONFLICT)
H dreams of the city
T dreams of the forges
  T reads their mind
B dreams of the black desert
A dreams of the council
  Recognizes the Druma'san swords
  Decides to destroy the Sword of Destiny
  Cleanly destroyed
B wakes up - commotion
  A lies
  Hides the destroyed sword
  Uses misdirect to hide the damage to the sword
  B apologizes for earlier and makes A feel bad
A vents the sword out the airlock
  Notices the mark of power



H checks beast call, which had recorded the Druma'san
  Goes to wake up A
A wakes up and finds room covered in runes
  H is aggressive about wanting A to open the door
  A compares to runes on the wall -> maybe similar?
  A is worried about B and the Tyvokk -> Goes to wake up B
A wakes up B
  B checks the Tyvokk
H wants to suppress the Tyvokk
  calls T
  suppresses the Tyvokk
T looks at the beast call -> seeing double
  same thing in A's room
  A recognizes that they are the Prasath'seng markings
  call in B -> also recognizes it
    unable to recall where it came from in Prasath'seng
  A touches it while suppressed -> nothing happens
  H stops suppressing
  T reads A''s mind... finds something
    But stays strong... does not fear, senses the Druma'san
    Tells A that something is trying to enter her mind
      A is terrified FEAR
A freaks and bolts
  heads for airlock
  B notices the missing sword
  B chases
  B grabs A
  A is still fearful, but calming FEAR
H and T catch up
  A is freaked out!
Matthias comes by -> concerned
T wants to try and connect to the being in A's mind
  H comforts
  B summons Eos' help -> shakes off the fear
  A is worried about Tenebrous
T reads A's mind
  T senses both entities
  Hears the cacophany of the Druma'san
  x2
  Tells A about the entities
K asks B about anomalous readings on the hyperdrive
  B says nothing noticed
  B asks about the issues occurring
    No idea 
  B sets to work removing the old power conduits
    Takes a long time to get the coupling out
    Notices its weird
T decides to rest before reattempting
A asks the universe: would the universe be a better place without me?
  yes and no
A chills with the runes
  T and H watch her
B finishes up with K and offers to help with anything
  Explains to H what the issues are
  B is worried about A, thankful that H helped her
A resolves to tell B about the SoD
  Confides in T
T takes H and A to delve into A's mind again
  dive in -> A is helping
    A hopes T finds what he's looking for and stops being burden
  T connects to the light side
    T is burned!
    Makes light of the situation
  H tends to A -> no issues
  T says it's alright and helping :+1:
A goes to talk to B
  B is working
  A tells B about the connection to Thila
  Says there is something good inside
    "If T said it was good... it must be good!"
  A is worried that Tenebrous' desires are not alligned
  B still doesn't want A to return to Thila
    ...but A wants to go back -> there is something there
  B lets loose about Eos, still thinks returning to Thila is a danger
  A not sure how to take it -> accepts
    thankful, sorry that B had to bear this burden
  A is worried about the future she saw
  H says to follow heart, will keep up her end of the pact
  decide to return to Thila
  T supports A
  H is wary of prophecies
    B still likes them...
A decides to meditate and try to reach out to Eos
  B channels E's power to help
    "Come home"
  ...but the Druma'san cries echo out
    But she is strong
    
**TO THILA**


2 - Investigating and understanding Tyvokk
2 - Connecting with Dark forces
2 - Connecting with Eos
2 - Saving Asori
5 - Pursuing Asori's destiny
1 - Dark understanding from dreams

A  H  T -  
2         Dark visions of Tombs
1  2  2   Cutting Asori out of... everything
1         Fear of destiny
   1  2   Channelling into the Tyvokk - Already accounted
2  1  1   Asori gets mad at everyone
2         Sad because broke sword
5  3  3   Asori almost airlocked (dark entity connected)
1  1  1   Dark dreams
1     3   Dark voices
2  1      Fear of Asori's destiny