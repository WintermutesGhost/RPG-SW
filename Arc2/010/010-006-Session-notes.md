Asori returns
  Hugs Baldyr
  Shares her new-found certainty
  Certain that it was a god she met
  H suggests that maybe Eos was responsible
    B is very upset at the suggestion
  Summond the sword of destiny
  Discuss the path forwards
Go to speak to the Elders
  Aiming to improve camp moralea
  Melya shares that she once met Eos
  Melya implores A for help
  A gets emotional
  Melya bows
  B asks Melya why she didn't recall -> Lies "it was a long time ago"
H asks around if anyone knows about Eos' grove
  Sounds like a place in the jungles -> Not sure the specifics
T and H discuss how to actually do things
A tries to sense the location of the ancient sites
  Senses... something 
B inspires hte tribe to be confident
A does more research

Dream of the forges
Hear the hunting horn
  Learn of the Terrentatek hunting
  Unable to convince the hunting party to accept help
  Allow them to leave -> with contact information
  Decide to follow -> cannot get close enough
  Decide to tell them they are coming -> not happy about it
  A senses the shadow-hound nearby
Continue following the hunters
  T senses the twited reek nearby
  Back off to let the hunters handle it
Reek attacks
  Immediately steps in 
  Kill the beast easily enough
  Sotra smooth things over with Elorrs
  Elorrs is suspicious of the tribe -> Accepts that ATB don't know much
    What do you mean by sickness
  Advise that they could follow the tracks back to where it came from


  ~~~

Haam completes research
Arrive in swamp
  Fight scrange
  One retreats - H not sure if a mercy or not
  Another limps off
  T becomes infected *******
T is spooked by the flesh coming off in his hands 
A senses the corruption in the swamp - conflict
H shares information on the journal scraps
Press on towards the source of the poison
H is affected by the bad airs
H spots something in the pools
  T checks with the force - very dark, retches
  T and A look at the compounds
  Find the accumulated poison
  Notice it is coming from the river
Head up-river
  escape the oppressive poisonous atmosphere
  camp by the river
  A draws out dreams of the Druma'san
    Sees the warbeasts
  Get moving
  Cross river to the South side
  COntinue
Find some pottery 
  Find markings that look mirialan-ish
Move forwards and spot the furnace stack
  Asori steps into a pit

 ~~~

A checks the pit and clambers out
  wonder (and wander) about them
T surveys the area
A percieves the dark energy, senses the Druma'san near by
T inspects the tower
  Senses the dark energies inside
  A and H survery the area above the forges
  T sends the remote down, but only sees the crucible
All enter through the visitors entrance--
Arrive in marketplace
  T wonders (Is there anything of silhouette 1 or more that wants to attack us) -> No
  A and H wonders what is the room for -> Market
  A wopnders if the structure is cortosis? - Maybe
  B thinks it's the Druma'san
  A focuses and hears the Druma'san nearby
    Afraid! - But not much
Head towards the domeciles
  Don't get lost
  T detects the pervasive darkness
  A spots the refuse outlet
  All see spirits
    H is affraid
    T blasts them
      A fails to stop
      "WTF T?"
Head towards the refuse outlet
  Get a bit tired from A's poor navigation
  A tries to communicate
    Draws upon the Druma'san powers
    H provides the force
    "I am Asori Ta'em of the Druma'san"
    Speaks to one of the Druma'san
    "You must return us to our vessels"
    Where are the vessels -> "Deeper"
    How? -> "Deeper. Seek us out. Draw us forth. Return us to our vessels."
    H wakes her -> A says "I will do as you say"
A thinks they are suffering
Continue to the refuse area
  A is affected by the compounds
  A Tries to feel which way to go
    Revolting, vile, but must go deeper
  H grabs a sample of the greenish liquid
Head deeper
Find the bore
  Set up a fusion lamp to see
  A goes out to the pedestal
  A asks Eos "Is it your will that I place the Sword of Destiny in this device"
    -> Yes, return them to their tomb
  A plunges sword in...
    Black
    Fear resonates with the Druma'san
    Charms them "I am Asori Ta'em"
    Almost overwhelmed -> Uses Druma'san powers
      They are bound
      "return to your vessels"
      B helps offer words of encouragement
What happened
  B offers Hot Tyvokk - nah
A asks Eos 
  "Have we succeeded in bringing closure?" -> No
A settles to try and calm herself at the edge of the cavern
  Doesn't feel right to have controlled them

**XP**
4 - Protected the village from the twisted reek
2 - Found the corruption in the swamps
2 - Tracked down the ruins
2 - Contacted the spirits
3 - Explored and investigated the ruins
5 - Returned the spirits to their vessels

**CONFLICT**
A  H  T -  
2  2  2   Dark dreams (x2)
1     2   Rebuked by the hunters
      1   Disgusted by the dead scrange

4  3  4   Bathed in dark energies
2  2  2   Arguing over attacking the "spirits"
2  1  1   Communing with the spirits
1         Commanding the spirits


---

AH Want to explore the place a bit more
B woners what this place is -> no idea, doubtful
T waiting to hear what to do
H asks B and A - Are you prepared if the answers you get are not favourable
Decide to keep searching the place
  T makes a faff about not shooting non-hostiles
  Note identical endravings on the face of the sarcofagi
    H wonders why
    A wonders if they were slaves or warriors? Is this a prison?
    recognizes it as a spell
    T looks down ast the bottom - not sure how many there are
Split up to search for clues
  T tries to search for life - too darksided
AT go to the warbeast forges
  Examine the cages
  No way in or out
  Find some glassy bones
  "How did they get in here?"
BH go to the tradecraft area
  H asks about Druma'san history
  B shares personal history with Druma'san
  Explore the tradecraft areas
  B grabs a bolt of fabric
  H grabs a file and a hammer
  B manages to trigger the glass-spindle
    *** H uses force L+D to sense what is happening
    B grabs a ball of the fillament
    H suspects the same forces as A uses to summon swords
    B assumes it's because of Druma'san history
    H thinks it's just because of will
      H activates it as well
    B realizes connection to Drumasan amulet and A's sword
AT go up to warbeast forges
  A wonders what they are for - Realizes, but not affraid
Meet up in foundry
  AT examine the creature
  B sees if he can make it move
    Goes to lift itself up - stops again
  HT are afraid ***
  Decide to try and talk to it (???)
  H applies happy feelings to it
  B connects to it and feels it drawing on energy
    Drainied x3 ****
  ABH figure out the purpose of the foundry
  H thinks A helped some tortured souls
Examine the weapon forges
  H finds fragments of blades -> "welcome home, sword of destiny
  A and B think the blades are similar
  "Do these frasgments belong to druma'san blades" -> Yes
  A and B disbelieves
A tries to communicate to the golem
  Nothing there - blank
  A - Should we put it down?
    B and A say yea
    T says nay
    Choose to leave it, waiting...
Leave
  A spots the beast - Does not make the connection with the glowing red eyes
  Points it out, the others see
  B is not affraid
  A realizes the beast is from her dreams

---

A wakes the others to tell them about her dream of the beast
  She wants to... save it?
  ... by capturing it and reading its mind, then killing it
  B is worried that it is too dangerous
  A: "It is a victim"
Resolve to  try and capture it
Locate a place to ambush
  H gives B a chisel from the ruins
  H cannot find a sniping position
  T digs a very cool pit trap
A focuses and draws the creature out
  and again
It arrives at the clearing
  A draws it in
  H is afraid of the beast ***
  A wills it forwards... steps into the pit
  B is afraid
A tries to communicate
  T senses the connection to Asori
  H tries to heal 2
    - Burns the beast - lashes out 
  A tries to calm again - failure, darkness
  T reads mind - Anger, emptiness (lack of CnC), partially filled by Asori
  A hides ATH from it, but still feels connection
  Decide to blast it
    T opens up first - feels some guilt
  A waits until it is disabled and leaps down
    Plunges both swords into it - humming energy from sabre, waiting for command from sword
    "Sleep now, forever in peace" - It dies.
T feels uneasy about the whole thing
  B not sure if it's dead
  AH agree this is for the best
  cremate it
  A feels pity
  B is upset that A treats him a bit patronizedly


**XP**
3 - Plumbing the ruins for secrets
3 - Building a trap
2 - Drawing out the war-hound
7 - Ending the war-hound

**CONFLICT**
A  H  T -  
   1      Dark Force use
   1  1   Fear of the Golem
1         Understanding the purpose of the ruins

1  1  1   Conflict over the nature of the beast
   2      Fear of the beast
3     1   Linked with the beast
   1  2   Hunting a trapped animal
1  1  1   Ashes to ashes, dust to dust

---

H debates whether to tell others of the encounter with the phantom
  Maybe she is sick?
Decide to take the Salon pod 
Scan for the city
  T disrupts the ships sensors with his phenomenal power
  T spots it no problem
  A tries to scan for the structure of the city -> Gets a rough understanding
Tie selves together and prepare to descend
  Land in the midst 
Check one of the buildings
  Check a room
  Try to understand them
  Suspect they are rellated to the preevious ruins
  Seem to be some diversity -> upscale
T uses seek for A to find the source 
  Locate it beneath the lake floor
  A returns the the imagined city
  Tries to communicate to the beings
    Learns the name of this place
Dive down to the floor above the heart
  Find nothing 
  Swim back up
Explore the grand plaza
  Inspect the statues
  A recognizes the "Aynn'men" hall
  A tries to understand the hall
  Sees as a simulacrum of Prasath'seng
  Locate the elevator to the throne room
Travel down the elevator to the Throne
  Deduce its function somewhat
  Decide not to mess with it yet
A discovers how to power the lights
  T and H can with some effort
Find the center of the bunker
  Not sure its purpose
  Decide to investigate further before activating

Timm'eh looks through the door to see
  Spots the spirits -> not affraid
A wills open the door
  Realize they can only see the spirits with the force
Explore!
  Find the tombs
  Sense the sand inside -> but it is dead
  A checks the writing - Ayyn'man + 'peanut'
  Find the stairs
A wants to meditate in the tomb rooms
  Connects with the spirits
  **Learns word for 'home', 'help me', 'destiny'** -> sports it on some of the tombs
  B sees the writing is dense and overlapping, but can make out the similarities
Travel up to the residential area
  A wonders on the purpose of the tombs
    Slaves? Machines? Servants?
T asks the universe "Would Asori putting the sword in the tomb help resolve the spirits issues?" -> Yes
Explore the merchant district
  Wonder what it was for - Bunker? War? 
  H checks for poison in the water -> inconclusive
A explores a deliberation chamber 
  Find fragments of Druma'san blade -> B keeps
  A notcies word DESTINY above the doors
Explores docks
  H takes a couple spirit vessels
  A grabs some tablets
  Grab a bit of everything
Explore the slums
  H takes a sample of the structural clay
  T asks "If the degredation of the buildings here due to unnatural forces?" -> No
Return to the Throne
  T "Is sitting in the chair an important step for to saving the spirits?" -> Yes
  Meditate around the throne
  A decides to hop on
    Connects to the city
    Tries to understand the truth of the city *** 1 conflict for Asori
    Realizeds she could surface it
    Decides to do it -> No awareness of consequence
    Requires 15 Dark side pips
    A asks Eos "Is this your will?" -> "No, let it crumble"
    Decide to go put sword in stone
A plunges sword in -> darkness
  Summons them with charm -> Success!
    B feels the warm Tyvokk
  All roll for fear -> No problem
    T realizes the identity of the beings following Asori
    B realizes connection to A and lends power
  A askes them to return to their resting places
    And let the place crumble -> fear and trepidation from the spirits
      Rest peacefully, without burdens
Want to investiagte the artefacts
  Despite being grounded in darkness, lively people 

**XP**
2 - Finding the lost city
3 - Exploring the ruins of Aynn'men
4 - Learning of the planet's history
5 - Returning the spirits

**CONFLICT**
A  H  T -  
2     1   Contact with the spirits in the city... and the city itself
1  1  1   Understanding the nature of the creatures
1         Frightened the spirits
1  1  1   Time spent in the deep dark


---

A explains the events of the past day
  Maybe she is interfering - at least we are removing the darkness
  "Is that not our purpose?"
H begins giving Crumpets to the villagers
  They are well enjoyed
  H begins spinnning a tale about the history of the """tradition""" -> To tell a story of heroic deeds
  H describes the battle against the warbeast -> Convinces even those that remember the first attachs
  +1 Village trust
T goes to speak to the Constable -> How can we help?
  Realizes he is being pressed for details -> Negotiates them
  +1 Village trust
  Learns the biggest threats -> Ghosts and malaise
B talks to the villagers about the attacks
  Learns there are 5 phantoms haunting
  Receives another journal fragment
All decide to investigate artefacts
  T scans contents for dangers -> 3 has an entity, 7 has radiation emanating
  1 -> contains jewelery from Gunn'sara, H takes one
  2 -> contains Druma'san swords, must be a connection with the Druma'san
        -> Could this be part of why the darkness is so bad in Prasath'seng?
        -> Is the darkness of Baldyr's sword found in these?
  3 -> contains the sealed canopic jars, 12 with entities, 4 empty
  4 -> contains burial wrappings
  5 -> contains scroll tubes
  6 -> contains slates, all identical
  8 -> contains the spindle of a bone lathe
  9 -> contains a variety of alchemical sands
  0 -> contains files and crafting blades
  7 -> contains sacrificial amplifiers
A tries to put it all together
  H wonders what they were crafted for
  A cannot decipher if they are from here or not
  A discovers they were probably not interstellar, and tended to crafting
  H deciphers that they were a society of traders
  A&H figure they might have entombed themselves to return in the future

---

Working late, hear howl of the battle-phantoms
  H T  fail fear (H tri) *** CONFLICT
    H fears losing control of the force
    T fears failing his comrades
  Rush out to help the camp
A tries to command the figment to leave
  Success but dispair
    A sucecssfully repells it, but the darkness taints her - *** CONFLICT
B sees the phantoms and fears ghosts
  Stays composed
Start warding off the figments
  T "witnesses" one kill A but resists
  H feels energies welling up but resists
    Uses dark side *** CONFLICT
  Both shake off the illusion
A rebukes another
  H tries it as well -> it works
A crys out "Where are they coming from?"
H does J's incluence on one
  fear bounces back
T disbelieves them -> Not quite an illusion, still disipates one
T's final blast catches B
All discuss what ahappened
  No one hurt
  A rallies the camp
  A feels the darkness 
H tries to analyze the purpose of the attack
A and B figure they are using Dmsna like techniques
  B wonders about the hall of blades
Elders implore to protec
  A stresses importance of protecting the spriti of  the warriors -> Jesma and Va'ko will work on that
T seeks the entity he banished -> finds the crystal, and the pyramid, in the desert
A seeks the source of the darkness -> Sense the aura, but it's not here
A and H discuss where to go, get T in with them
  Decide to go south via salon pod
  H makes chawls from the burial robes
Prep the Salon pod and fighter to go
  A is flying fighter
  H pilots the pod
  Bring the soul urns
A goes ahead and crosses the mountains, failing to do it stealithily
H fails to navigate efficiently over the mountains -> High altitude balistic trajectory
-> Dust storms are making spotting difficult
A moves in to reconnoitre the area
  Spots structures at the bottom of the crater
  A scouts a landing point -> Safe landing, but 

Took too long - evening now
Will not be able to leave until the storm abates above

T scans for lifeforms (obviously) 
A hears the callinng from beneath the pyramid, and hears the name of Nada'tuna
H Searches the village -> finds the pottery wheel and loom
  Examines the animal pens -> Beasts of burden, not native to the desert?
A examines behind the pyramid, finds the stones and the arena
  Notes the marks of weapons in the arena
  B examines the weapon racks -> Some resemblance to those found in Prasath'seng
B tries to pull on of the stones out of the sands
  Fail + despair
  Creature is awakened and attacks B
Begin taking down the phantoms 
  FEAR - NONE
  T tears apart the crystals
    FEAR - T feels the feedback but resists it partly ** 2 Conflict for T
         - H is nearly afraid but draws on Navid's necklace and Jesera's Influence ** 1 conflict -> Afraid of ghosts
         - B afraid, suffers strain
A confronts the sole remaining phantom
  Fails but does stop it for a moment
  Turns to attack Timm'eh instead - Smooshes it with glass shards
  H smashes the last crystal
A checks on the crystal
T checks Tyvokk -> It's a bit warm
A and B check the meditation rooms -> find broken pottery
H and T check the memorial hall
  T looks in the racks -> Sees the jars and the life within
  H and T use force to open door
  A opens it and grabs a jar
  T takes more burial veils
Explore the Mess,
  H recognizes the implements from Gunn'sara
Explore the dorms
  Sense a multitude of spirits around
  B is not afraid, but uncomfortable
Go to place the urns in the meditation rooms 
  Starting with a full jar
  A kneels on the meditation pad...

T feels a funny feeling
  Blocks the door to ensure it won't close
  Stresses A a bit
H dons the force-detecting goggles
A focuses on hearing what is going on
  Senses the flow of the energy into the slab
Pushes a trace of light energy into the slab
  Backlash! on A and H
  A feels the connection with the jar
  A does not want to channel the dark energies into the stone
  Leave the chamber
Head to the main hall
  Sense no particular danger
  Examine the spirit altar
Head into the crypts
  Claustrophobia
  T looks for signs of presence 
    small bits of dust -> Only near the entrance, not deeper in
B thinks about and realizes the intricate artworks of the Druma'san
  Can probably identify them in the future
  H notes that the current tribe does not, either
A leads to the main tomb
  Draws on the ichor and place the sword
  Summons the spirits
  They are fewer, but some more powerful
  The powerful spirits hunger for the energy of Asori
  They witness her resolve
  A has a new word etched into mind: RESPECT
  Asks what happened here: PLACE RESPECT MEMORIES PRESERVATION KNOWLEDGE
  The greater spirits help A to calm the others -> They do not return to sarcofagi like the others
    Promises to protect the temple so that it is not disturbed
    A spots enw words on sarcofogi
A checks to understand the pyramid
  Understands something about the temple location
  T remembers the holocrons from his world
A decides to go back to the urn
  H stays with her
  A channels 1 dark pip *** CONFLICT
  Asks for help -> PRIEST NADA'TUNA, A -> DESTINY
  "Why do you return after so long?" -> KNOWLEDGE PRESERVATION MEMORIES AFTERLIFE
    -> DRUMASAN DESTINY CHILD-OF-DESTINY (triggers darkness in A) *** CONFLICT
  "How can we protect your relics/memories?" -> KNowledge of all the locations, DESITNY HOME


**XP**
4 - Defending the Village
3 - Exploring the ruins of Nada'tuna
3 - Eliminating the threat of the phantoms
2 - Resurecting and speaking to one of the beings
3 - Returning the spirits

**CONFLICT**
A  H  T -  
   2  2   Fear from first encounter with the phantoms
1         Commanding one of the phantoms to leave
   1      Channelling dark force (repelling the phantoms)
   1  2   Feedback after destroying the crystals
2         Channelling dark force (during seance)
2         Speaking to the figments in the void
2  2  2   Time spent in the deep dark

---

A challenges B to battle chess
T asks H to teach healing techniques
A and B dream of Eos
  A-> Are we not doing enough
  "You have been doing as asked so far"
  A -> they are not all defilers, some are victims, no?
  "They are borne of darkness, and will bring darkness. They will destroy ther Druma'san."
  B -> we will do as you wish, can you say how they are related?
  "No, your destiny as blessed children of Thila. They are setroyers with a false destiny."
  B -> were they the original denizens?
  "They came before but were not chosen"
  A -> just because they are of darkness does not mean they are evil, should they be pitied not hated?
  "No hate, but they bring destruction. They will end Thila and Druma'san."
  B backs away
  A -> Tell me why we are the chosen and not them
  "You were chosen to be one with Thila, for the Deceivers there is only darkness"
  A passes out
  "Fulfill your destiny. Save Druma'san and Thila."
B notices A is unconcious
  Calls for H, T hears
  T wakes H
  Unable to rouse A
  H gives A a shot of adrenaline
Resolve to get some more rest
  A has some night terrors of friends dying -> A is afraid *** CONFLICT ***
H senses that there is some force afliction on A (though she is recovering)
  Decides to put the force-restriction manacles on A -> Thinks better of it
A asks T whether the massage last night was weird -> T lies
A feelsd recovered-ish
Decide to head to Ayyn'seng
B sends a message to K asking on the status of the village
A guides them to the feeling
  T detects the ruins
  A does a quick scan of one of the enclaves
A guides to land
   Sesnss wherre to go 
   Chats with B about the dream with Eos -> share with H and T
   Arrive at the library
T check and finds the phantoms do not exist any longer
A accesses the Midnight proejector, and sees its name
  Explore the room
  H pokes at the boxes -> senses not for her to move
Head into the tunnels
  H T B recognize they are like the tunnel that took them back to Prasath'seng
  T considers exploring, but A mentions it is leading away -> Decide not to go deeper just yet
A heads through tunnels back towards the Central Hall
  Sneaks up the stairs

A -> First Elder
H -> Elder's Second
T -> Warrior Master
B -> Commander of the Enclaves

A gives an inspiring speech
Defeat the creatures handily

Sneak into the grand library
Explore and find quarters for 50
  Only 30 sarcofagi
Consider whether to see to the soul anchor or the midnight projector
  Eos certainly pressed them to act, rather than thinking
Put together the lenses, witness the past

A thinks the Druma'san poisoned the planet after they united
T thinks Eos fought back, a spirit of the world
  A disagrees -> nature itself

A places sword in the anchor
  Done with kindness, hope, not oppressive

**XP**
2 - "Helping" Asori recover
6 - Holding the line at Cru-nik'ka
2 - Discovering the Great Library
5 - Viewing the four remaining Nyctalopian Lenses

**CONFLICT**
A  H  T -  
2         Night terrors of friends' deaths
2  2  2   Exposure to the Midnight Projector
1  1  1   Doubts about what you are doing (optional)
2  2  2   Time spent in the deep dark

---

Awaken with Eos
  T recognizes Eos as taking the form of Val Isa
  A asks if Eos was there when the Drumasan fell, Eos says it was Thila that rejected them
  B asks if they had a name, Eos says they gave up that right when they perverted destiny
  B asks about the Tyvokk, Eos says it was to Seek, but wasn't needed
  T asks about destroying the Tyvok, Eos says to pitch it into the sun
  A asks "why me"... Eos says "fate, lol"
  H looks around, spots signs of Tenebrous 
  Eos sends them out urgently

Party believes it was Tenebrous that was stalking them or Eos
  Urgency

Take off immediately
  T receives message from Matthias
  Calls K-17 -> T unable to reach due to Eos' interference, A same

Get in touch after crossing the mountains
  K indicates issues with the villagers
  M says everyone is sick
  A asks to evacuate the village into space for a few days
  B suggests moving everyone to elsewhere on planet
  B asks K to pass status along to Elders

A recalls a saying of the darkside -> Darkside is like venom
  "K should force them to move if he has to"

A meditates on where to go -> No dice
  Takes a hit from a stone sorceror
  Strafes the stone blindly
  T detects the other stones and the city
  A keeps strafing while H navigates the Salon Pod
  A uses deadly reputation (?)
  B takes the helm, stops, settles into the canopy
  H suppresses force and hides selves
  T scans the enemies
  A keeps blasting the turrets, clearing a path
  Set down the Salon pod at the edge of the city

H is frustrated that Eos seems to have little respect for A's safety
  T senses no danger, sees the turrets have been firing
  A senses where the source is
  Head towards the academy
  A notices the route undergoround, covered by another stone sorceror (inactive)
  Close over to it
  H recognizes trhe crystals as matching the amplifiers
   H tries to pry out a crystal -> blows it up
   Absorbed by H's force suppress
  Head into the tunnels

A steps in
  Feels that this is not the right place to be, carries on
  H is suppressed, B overcomes and convvinces to be allowed, T knows to go on... but feels wrong
  Move towards the objective
  Come across the entrance to Bib'tuna
    T triggers the sorceror -> narrowly avoids the bolt
    A charges in and blows one up... with herself
    B charges in to help A
    T is left without even being checked on
    H pries free the crystal and feels the power dump into her *** 1 conflict
    Continue into Bib'tuna proper
    Reach the sanctum
    T is hit by a mind-searing weapon
    Stumbles back out

A puts the sword of destiny in the slab
  Charms the spirits back to their vessels
    A is not sure if it's honest -> Doing it for Eos *** Conflict
  Decide to leave -> Too important! -> Might return later
  Unable to map the facility with ship sensors -> Just a big, absorptive slab
  On the way to the far north, Salon pod radios back to the Solar Fate
  A tries to fix the Destiny mid-ar -> Unsuccessful, the damage runs deep


**XP**
3 - Speaking to Eos
4 - Reaching the city of Bib'tuna
2 - Destroying all of the Stone Sorcerors
5 - Returned the spirits to their vessels

**CONFLICT**
A  H  T -  
   1      Dark energy exposure from removing a crystal
2         Willing to mislead for the cause
1  1  1   Attacked by dark energies
2  2  2   Time spent in the deep dark

---

Detect the aid ship on the way in
  T makes a call
  Hear about Kodo cares
  Leave it in Sar and K-17s hands

Approach cautiously
Pick up the disruptions
  H imparts fear into A about the consequences of failure *** 1Pip Conflict
  A leads the flight
  B almost crashlands, blows the reactor
  T holds aloft
  H restarts reactor
  B and A manage a gentle landing
  Take off again... 
  B is helped by psychic connection with A
  A leads again, but B clips a stone pillar -> Tailspin

Encounter the mirror
  A spots their reflection
  A evades -> Notices the reflection
  Avoid touching it
  T stops the Salon pod
  A checks the edges but is unable to find edges

Decide to land by the barrier
  H heads out and towards the barrier
  T experiments a bit with the barrier
  B notices the Tyvokk is hot
  Argue about how to cross
  H decides to just cross
    Tries to just swim -> Fail
  H falls though
  A is worried, but 

A feels the thrum of the darkness
  but picks out the thin call of the slab
  determine the Tyvokk is very toasty
    B tests whether they are healing or anything -> nope
  T notices the Basilisk in the distance

A leads the party forwards, picking carefully through the terrain
  T recons for dangers -> Can see some of the pillars with black shadows
  H is unsure if the well is getting farther
  B notes the flakes of black snow
  H does a med check on B -> clean bill of health

A navigates through the Pillars of Shadow
  Successful
  H believes they should take the lead -> Disposable
    B agrees
    A thinks it is a slight on their abilities
    H moves to restrain A... but A knows the safeword
  T notices the shade approaching
    Moves to engage
  H asks a question of the universe:
    Is the intention of this creature to prevent us from curing the snakebite? -> No
    *** Conflict (3 dark)

T doesn't know whether to shoot... decides to!
  Shade is approaching
  A tries to comprehend it, cannot see what it is, bvut senses that it is not affecting the world
  H tries to impart good feels -> fails, feels only hunger
  A swipes with sabers -> shut off
  B takes an attack meant for Asori -> notices it takes shape as it attacks
  B/A ready to attack in a coordinated manner
  H moves in to clamp the force/suppresing bracelets on the shade
    Foces it to have a reptillian tail
  T surveys for additional adversaries
  B/A chop off a tentacled maw
  H blasts off a tail and brainmatter
  B/A chop off a rancor claw
  dies, leaving nothing but shadows
  B uses force goggles to check for... force -> like midnight
    Sees the dark shining from the false sun

A plans to guide the party through
  T scouts with remote and force
  H uses pathfinding knowledge
  B imbues A with energy -> Eos is beyond reach
  H senses the strange navigation required
  B feels the silence
  T senses Tenebrous
    A thinks this is its home
    A thinks it is no evil per-se
  Arrive through

Reach the shadow village
  H notices it is not warfare
  A wonders if it is an imitation? recapturing the glory days?
  B and A confer on the causes
    Encounter figures created from the memory
  T notices the statues in the windows
  H notices a statue replacing an implement
  Leave as the villages begins to remember
    A worries they have had a bad effect?
    Wants to leave
    Reaches out with force -> Only void
    T senses life -> Absolutely nothing, except the four of them

Arrive at the fortress
  Welcomed inside

-

Awake in the room
A recalls entering the room

Hear something coming down the corridor behind them
  Most hide
  A stands around getting ahold of themselves

Shades enter
  A tries to leadership
    Spends dark force to convince not to attack *** NO CONFLICT
  B cannot make out their form
  H tries to instll happy
    Fails

One charges
  A dodges
  H use Jezerals influence  *** NO CONFLICT

Sense something else coming
T takes down Shade C - Attacked by tenebrous

Asori - Wounds 6 Strain 6
Baldyr - Wounds 3 Strain 4
Ha'am - Wounds 15 Strain 0 Crit Stinger(1) Compromised(2)
Timm'eh - Wounds 0 Strain 4 

Try to rationalize the happenings
  A realizes what has happened
  B thinks it is an illusion
  A backs off, tries to stop fighting -> regards tenebrous

H and A attack Tenebrous
  B joins in -> takes its eye

Tenebrous disappears in a flash of anger
  A and H take 3 conflict

A decides the shadows are not their enemies
  Tells T not to attack
  T stuns the shade -> notes the bestial form coming out

A brawls with a shade

H steals ability to steal abilities from shade Ha'am
  Strips away the false self leaving only primal shade

B plunges off the ledge destroying shadow b
H attaches force binders to shadow A (or what remains)

Finally dissipate all the remaining shadows

A believes Tenebrous and Eos are two sides of the same coin
  Believes she has been asked to destroy them because of their difference
  Their incompatibility
  Is Eos manipulating the destiny of the beings?
  Is darkness really evil? Not bad jsut because of the darkness
  B feels that Eos has the destiny of the Druma'san in mind
  B is confused the Tenebrous was atatcked by the shadows
  A believes there should be a place for the Druma'san
  B wonders if their time has come and gone
  A has not felt any anger or hate from the D spirits
  T doesn't understand what A wants to do
    Eos is the path to health and life
  A could not live with locking away the Druma'san forever


B goes for the Tyvokk
A cracks open the door -> darkness pours forth

-

B - Calls out to the others -> No answer
  Yells at Tene "What have you done?"
  Tene offers final advice before leaving
  B tries to fdind way forwards -> stumbles over T's corpse
  B feels the connection to Asori and her necklace
  B cradles A
  B shuts the door

Everyone awakes
  A asks what happened?
  B stammers an insufficient response
  Everyone ponders what happened
  Talk about how to enter the facility

A checks whether she can hold the Tyvvok
  Will burn her -> Decide to all touch the Tyvvok and walk into the room
  H wavers... *** Conflict
  A inspires her but feells *** Conflict
  All feel darkness channel into them
  T seeks... realizes the sky is empty
    Not affraid, instead confident

A opens the door, and they stay concsious
  B leads coordination to find way in
  A thinks back to discussing with H about "who said to throw it into the sun?"
    Eos said to throw it into the sun after finishing here
  A draws thw SoD -> struggles to maintain control -> places it in the slab
    Back to the void
    Calls on the Druma'san -> Nothing, just hunger

A tries to force the darkness to close
  Somehow taps into the ancient machinery and alchemy to shut off the beacon
  It is... impossibly... sealed

Exit the more normal fortress
  See the ships just down the hill
  Call K-17
    Hear that the evacuation was completed
    Hear about Kodo Cares

Decide to head to Prasath Saeng, to see this ended


---
