Unable to find information because of the info blackout
H negotiates with Daro to increase pay to 8000 cr.
Banter with Yysen, Aqualish captain of the Lean 
Banter with Soma, H captain of the Naboo, and Quenn, Human captain of the Bari
Hear from Nyn that something is wrong with the last delivery
Learn of Oini, captain of the BT-136-A
T gets worried and powers up the engines
  * Can't scan anyone, but senses with force
  * Decides to go for a walk
H uses J's influence on the STs
SHoot their way out
T refuses to cover Nyn
Fly away
Action VI left behind


1 for meeting the captains investigating the ships' history
1 for loading medical supplies
4 for escaping without casualtiessw

---

Pt. 2

Receive details of insertion
Investigate viability -> Decide it's alright
Astrogate well to arrive unnoticed
  * Drop into an asteroid's gravity well
  * Nearest ship is expected to be the Naboo Huntress
Try to find a way to sneak in, but unsuccessful
  * Still manage some repairs in the mean-time
Decide to try to keep going, with multiple attempts at failure
Receive the message from Bari's
  * Decide it's not safe
  * Rig an escape pod as a cry-baby
  * Fire it off at the VSD to try and pull it away from Bari's
Are taken to an alternate landing zone by Nyn
  * Nyn says there is a mole
T goes to set up a way to track the supplies
  * Inscribes a symbol,
Wants to flush the Lean out of Sterya City
  * T finds that the ship's boat has a separate transponder
  * Plans to sneak in that way and locate the L57
  * Leaving K, Squib, Mathias, Grachaw  on the SF
Decide to leave the crews of the ships to find hiding places
Leave with Nyn and FO Balen
  * Nyn will check with contacts
Salon pod and fighter take off
Intercepted by ties
  * A use misdirect to believe codes have been transmitted
Convince Balen that the codes were supplied by the Rebels
Arrive in the city
Investigate the L57
  * Currently in a covered port in the main port
  * No lifesigns
  * Currently clamped in
  * Lifeforms in area of the starport
  * Engines warm, off, at least a couple hours
  * Bonus detail later

3 for silent insertion
1 for sending some cursory help for the Bari's
1 for stealthy entry to Sterya

* T feels may have abandoned someone for the greater good--concerned
  * H felt the same

---

Pt. 3


Nyn directs to a landing bay
  * Asori screws up and trips a warning on ISB consoles
About 1 hour travel on foot
  * No efficient transport there
Timm'eh finds out not much in the starport
  * One patrol, easily avoided
Follow Nyn's plan
  * Timm'eh will nav
Asori looks for information
  * Plague still starting
  * Airborne, ILI
B unable to read the faces of the imperials
Reach the starport with no trouble or attracting attention
T remembers that the L57 is off some distance towards a segregated area of the starport
T searches for captain Yysen
  * Finds he's back in the city core
  * White room
  * Somewhat stressed
  * Planning to leave
  * Alone
T lies to Nyn about where Yysen is
  * Lets still check the ship
Sneak into the ISB starport
  * Go onboard
  * Try to investigate what happened
  * Find a holo-messenger
    * Contains transponder codes and a promise of payment
Decide they want to go to get teh medical supplies from the SD
  * Nyn will help if they bring Yysen here
Going to wait until Yysen gets here
A asks the universe a question:
  * "Would liberating the supplies from the SD and delivering to the people of Deersheba make a significant difference in how they recover from the pandemic. (self-actualization)"
  * Yes
H asks the same about health
  * No
T senses Yysen is coming with people 

2 for locating the Lean 5-7
1 for finding evidence of the mole


---
Pt. 4

Check the logs and see that some medical supplise are still in the landing ares
  * Unable to reach them stealthily
  * locate them
  * Don't think that they can get them now
  * May come back later
Lay in ambush
  * Miss him
Detect he's on the lamba shuttle taking off
  * T uses force (1 dark side)
  * PIns it down
  * A tries to deceive ground crew that there is a major failure on the shuttle
Baldyr plays sabacc with Nyn
  * Notices she is cheating, catches her
  * Asks to be taught how
Wait for ground crew to start working
  * Wondering whether to go after Yysen or capture the supplies
T decides to pull the supplies out
Yysen breaks out
  * T grabs supplies
  * H tracks Yysen
  * A and Nyn prep the ship
  * Balen breaks secutrity
  * B does after Yysen
T loads the supplies
B charges him
  * Y takes off on rocket boots
  * B chases
  * Grabs him 
  * "Come with me, I'm getting you to your ship"
A radios to port authority
  * Trips an alert
  * Baldyr takes fire from the ISB agents
  * A gets stuck, traffic control keeps her talking
  * T slides open the doors
A punches it
  * Baldyr rattles 
  * Keeps control
Decide that Nyn will fly, T and A will bail out
  * Land successfully
  * Nyn stops at a Hutt port to draw off attention
  * Cuts a long course back
  * A and T try to shake any pursuit
Meet up, party tries to take control
  * T asks to interrogate yysen
  * T reads his thoughts
  * T convinced he's lying
A asks the universe: "Did Yysen wilfully collaborate with the Imperials to the detriment of the aid smuggling mission?"
  * yes
SEND HIM TO THE GULAG
  * B: "No I don't know what they'll do to you"
Make plans with Nyn
  * decide to go along with it
  * She gives them the ion torpedo

2 for recovering medical supplises
2 for capturing Yysen
1 for getting the Lean 5-7 out


---
Pt. 5

* Argue about whether to go
* Go talk to K-17
  * KNows sometyhing about ISD design
* Rig the Ion torp with a timer
  * Attach a timer
  * But its delicate, a risk to transport while assembled
* Leave Saar to try and contact the Rebels
* Rig a secure comms from L57 and SFate
  * Relay to commlink

* Nyn creates an opening
  * Adjudicator follows
* Adjudicator hails as they close in
  * Dropping off additional supplies
  * Urgent! Let us land!

* Get on board
  * ask the flight crew to come on board
  * Steal their outfits
  * Slice their systems
  * Get info from control about cargo and Bari's crew
  * Stuff H and B into boxes (???)

* T tries to get on the Bari's -> STs say no
  * Stealth way to the aft
  * T invis
  * A looks normal
  * A gets hassled by an officer but talks past

* Decide to split up
  * A go to Weapons Control
    * Assmbles weapon
    * Sneaks towards weapon control (uses dark force)
    * Appears as a gunnery corps officer
    * just... walk in!
    * Plant the ion torp
  * B T H go to holding
    * Steals an ST uniform ST-137472
    * Get radioed
    * B hands it to A
    * STs come and take cpt Quenn
      * Attack!
      * STs seal the holding block
      * B's helmet is destroyed

* Grab the captain
  * "Save my crew!"

* Ion torp goes off... everything powers down

---
Pt. 6

* A needs to make a fear check
  * "My arm... what is this power... a force user?"
  * A is afraid
  * Pushes through the crowd

* B re-equips a new ST helmet
  * Quenn wants to rescue crew
  * T resists

* T tears open the door and smashes an ST
  * B flies in and punts the trooper into an open elevator

* A arrives, concerned by effects of ion
  * Fixes LS
  * B disables cams

* A cuts into the door
  * Charms captain Quenn

* Move to the control centre
  * STs from behind
    * H uses Jezeeras influence to spook em

* A accesses the control room
  * Chops off one guard's arm
  * T comes in to intmidatate the other
    * Surrenders
    * Quenn punches him
  * T negotiates with him
    * T stuns them unconciousd

* Begin liberating the crew
  * Baldyr goes for a stroll...
  * H goes with
  * A and T free more
  * A locks up the guards
    * Stims a guard
  * Finish freeing the crew

* A freaks out about B
  * B doesn't know

* Begin navigating their way out
  * T leads the group sprinting out
  * A fails to stealth through
    * T uses dead reckoning to point the right way
  * A stealths through
  * H fends off the Imperials
    * lose one bothan
  * A stealths forward
  * T tries to rally the crew
    * Fails, pinned down
    * B moves up
    * H blacks out and goes for a walk...
    * A sneaks the rest of the way

* A goes to Lean 5-7
  * Rest to Bari's
  * T fends off troopers

* See the hydraulics for the hangar
  * Attempt to blast it open
  * A squeezes past the Baris and supppresses
  * Gunners open the door

* Quenn is thankful, crew are worried

* 1 for getting on
* 2 for rescuing the crew
* 3 for getting off
* 2 for delivering all the supplies


Morality

A   H   T
    1       Jezeeras influence on StormTroopers
        1   Refuse to cover Nyn under ST fire
2   2   2   Leaving the Bari's Opportunity to their fate
1   1   1   Suspicions towards Nyn
1   1   1   Deceiving 'allies' about nature of powers

1   1   1   Worry about the nature of the disease, "Are we at risk?""
        1   Lying to Nyn about information on Yysen's location
    1       Knows this will not help the people of Deersheba's health
3   3   3   Turning Yysen over to Nyn and their backers
1   1   1   Worry about whether the meds will be delivered
1   1   1   Not sure whether to try and help further

2           Fear that something has come for Asori
    2       Lying to not risk Asori becoming unpredicable
        1   Not wanting to save the crew of the Bari's, "It's too risky!"
1   3   1   A moment of great violence. Was it necessary?


Bonus XP for Baldyr in deception