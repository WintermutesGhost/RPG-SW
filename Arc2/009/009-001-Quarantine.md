# Synopsis
The party is contracted by Daro Tillie to deliver medical supplies on behalf of the Rebellion to a blockaded planet. Supplies are being provided by a Consotrium affiliate at a high markup.

During pickup, Imperials raid the dock. Most ships take off, but some are unable. During insertion, a Consortium ship comes under fire and hails for aid.

On landing, the supplies are offloaded by Consortium droids and moved to a warehouse for distribution. The warehouse comes under attack by a local hutt-affiliated crime syndicate that wishes to profit off them instead.


# Crawl
## 01
DEERSHEBA, a once prosperous planet now stricken by both plague and Imperial blockade. A former contact, DARO TILLIE, employs the party for a mercy mission on behalf of the Rebellion. 

Now ASORI, HA'AM and TIMM'EH travel to the planet of SAYBLOHN to pick up medical supplies provided by an unknown benefactor. There they will rendezvous with the convoy before attempting to breach the blockade.

## 02
DEERSHEBA, a once prosperous planet now stricken by both plague and Imperial blockade. ASORI, HA'AM and TIMM'EH have accepted a contract to run medical supplies past any Imperial resistance.

Before taking off, however, the six smuggler ships were raided by Stormtroopers. Now, they are scrambling to regroup and figure out their plan of attack.

## 03
DEERSHEBA, a once prosperous planet now stricken by both plague and Imperial blockade. Our heroes have brought medical supplies to the planet, but others in their convoy were not so lucky.

Now, the leader of the smugglers NYN YONSUNA suspects treachery: Someone sold out the smugglers to the EMPIRE, and she wants the party's help finding them...

## 04
DEERSHEBA, a once prosperous planet now stricken by both plague and Imperial blockade. Our heroes have brought medical supplies to the planet only to find they had been betrayed by CAPTAIN YYSEN.

Now, they make preparations to capture the fellow smuggler, and maybe even recover the appropriated supplies...

## 04
DEERSHEBA, a once prosperous planet now stricken by both plague and Imperial blockade. Our heroes have secured and delivered supplies to the troubled people--but only some of them made it to the planet.

Now, they are making plans to retrieve the remaining  supplies from a bastion of the Empire: The Imperial Star Destroyer ADJUDICATOR. Even with stolen transponder codes and help from a dangerous smuggler this will not be an easy operation...


# Script
## Pickup
> Sayblohn - Pirate-plagued mid-world on the decline
> Found in the Quess sector

* Daro has organized pickup of medical supplies at Sayblohn
  * At a facility operated by *Mitse Pharmaceuticals*--a ZC front
  * ZC has no stake in this, but supplies are profitable and save lives
	* Planetary governor with Alliance support is financing the supplies

* Meet with *Nyn Yonsuna* who is organizing the convoy
  * Five ships alongside the Solar Fate
    * *Black Cetus* - Heavily modified VT-49 Decimator (EotE-FC:55)
    * *Naboo Huntress* - Modified J-type 327 with concealed weapons
    * *Lean-5-7* - Up-engined YKL-37R Nova Courier (EotE-FC:56)
    * *Bari's Opportunity* - Low-profile CR70 Corvette
    * *BT-136-A* - Q-ship Action VI

> Nyn Yonsuna - Bold smuggler on the Consortium payroll
> Bold and courageous but impolite
> Female Human Cyber legs
> Used to work with the Hutt clans, and did time in an imperial prison
> ZC provided a better life, and she is loyal to them for practical reasons

* As supplies are being loaded, Imperial teams storm the facility
  * ISB has been investigating Mitse Pharmaceuticals
  * Tipped of on the illegal goods
	* Corporate assets already seized
	* Leadership being rounded up
  * Initial breach by elite STs
    * Sergents
    * Including ST with shields
  * Reinforcements
    * AT-ST
    * Unlimited ST squads

* Scramble to launch in time
  * Release docking clamps
  * Complete refueling
  * Load supplies
  * Reactor ignition

* While leaving, the Black Cetus breaks off and fires on the Mitse headquarters


## Insertion
> Deersheba - Stricken mid-world
> Found in the Iskin sector

* Travel to Deersheba on the Mid Rim
  * Encounter an Imperial blockade/quarantine
  * Need to reach the planet and lose any imperal pursuers

* Supplies are now limited without the BT-136-A
  * Plan is for bulk of supplies to be carried by Bari's and Solar
  * Nyn gives differing orders to the separate captains to suss-out any leaks
    * Insists low risk of a traitor
    * Transmits insertion trajectories to everyone
    * After Lean and Bari's have left sends update to Solar and Huntress
      * Update from company contact, routes updated to new patrol patterns
      * Leaving to catch up to the Lean and Bari's
  
* Blockade is extensive
  * Not absolute, but close
	* Only hand-chosen vessels allowed to land
  * Significant forces
    * *ISD-610 Adjudicator* - Imperial I Star Destroyer
    * *VSD-472-E* - Victor Star Destroyer
    * 2 Dreadnought Heavy Cruisers (AoR:279)
    * DP20 Gunships (EotE:266)
    * Lancer frigates (AoR:278)

* During entry, run across a Lancer (interdictor mod) on patrol with 4 TIE/
  * Can try to sneak past--ECM and stealth
  * Can fight--one of its weapons has been replaced with a tractor beam
  
* During entry the *Bari's Opportunity* hits an ion mine-field
  * Still travelling her original route, not the updated one
  * Dead in the water, needs time to restore engine power
  * Engaged with a DP20, Lancer frigate and multiple wings of fighters/bombers bearing down



## Mole

* ZC droids supposed unload the supplies and transport to a secure warehouse

* Deersheba is suffering greatly for the unknown disease
  * Believed to have spread from Neimoidian colonists
  * Empire not allowing supplies in to apply pressure on the growing Rebel cell
  * Rebels unable to procure and deliver supplies in time, humanitarian
  * ZC wants to establish its own power base, loosen hold of Rebels and Imperials

* "Neimoidian Plague"
  * Believed to have arrived with Neimoidian immigrants
  * Causes respiratory distress
  * ~ 200 000 infected out of 10 Million inhabitants
    * Still escalating 

* Sterya City
  * Site of the Rebel cell
  * ISB investigation also centered here, clandestine
    * Rebels not aware of their presence
  * Full-time lockdown
    * Approved Imperial employees only
      * ST and IA patrols
      * Must produce papers
    * Full facial masks used
  * Anti-neimoidian sentiment rising
    * Graffiti
    * Hardline humanist faction has held masked protests
      * Redirected by the ISB -> Anti rebel sentiment
  * Anti-human sentiment rising
    * 


* Imperial investigation
  * Led by Agent Qyn Eleren
    * Levelheaded, faithful, uncreative
    * Brown skin, short black hair
    * Once survived a shipwreck
  * Operating under orders from sector command
  * Several Neimoidians arrested
    * Know nothing, no involvement
  * Agent Qyn currently clearing to distribute the medical supplies as an Imperial relief program
    * Starving out the Rebels has been effective
    * Need to bolster pro-Imperial sentiment
    * Without him, the blockade will likely continue

* Rebel cell
  * Led by Lt. Cmdr. Aol'ilar
    * Alert, engaging, unkind
    * Twi'lek tactician, green skinned, one eye milky-white
    * Fond of strategy games
  * Lost support and operations halted
  * Willing to accept the lost to avoid the humanitarian cost

* Consortium operation
  * Operating through PWE Networks
  * Distributed through black-market channels
  * Primarily interested in expanding connections for 

* Captain YYsen of the Lean 5-7 has turned over information on the smuggling operation to the local Imperials
  * Currently in ISB custody in Sterya City
  * Not being allowed to leave
    * Might succeed if tried to escape
    * Being questioned to track down the other ships
      * Black huntress and Solar Fate never arrived, but detected entering atmo
      * Naboo huntress broke out of the blockade 2 hours ago
      * No sign of the supposed Rebel involvement
    * Going to be moved off-world to the *Adjudicator* in 2 hours
      * Rebels are aware
      * Told they will be returned to their ship afterwards
      * Yysen is planning to break out before arriving at the Starport

* Nyn suggests finding Yysen at the L57
  * "He's proud of it, wouldn't want to leave it behind"
  * "You seem to be able to handle yourselves in a fight"
  * Snatch him and get out

* Second plan
  * "Make him come to us, minimize exposure"
  * Transmit a message that we want to break him free of ISB captivity
    * "We saw them taking parts off the L57, we can get you off-world"


## Bolt

* Yysen is being transported back to the starport to be moved off-world
  * Loaded onto a Lambda shuttle for questioning, Lean 5-7 will be moved there
* If spooked, will break for the Lean 5-7
  * ISB will be in hot pursuit led by agent Eleren


## Raid

* Planning to storm the ISD *Adjudicator*
  * Retrieve medical supplies
  * Using the appropriated *Lean 5-7*

* *Bari's Opportunity* is currently held on-board the ISD *Adjudicator* 
  * Ship in main bay
  * Crew in holding cells
  * Supplies still onboard the Bari's--mostly stowed in hidden compartments
  * Small amount offloaded

* Nyn will draw fire and pull off fighter patrols
  * Provides an ion torpedo, detonated near weapons control will take out the tractor beam as well

* Essentially limitless STs and Ties onboard

* Issues escaping
  * Asori's cybernetics require some repair
  * STs are mobilizing... but disorganized
    * Large contingent en-route to detention
  * Detention cells locked down
    * Require elevated access to release any cells
  * Aft lift blocked 
    * No power, issues moving crew from crew areas
  * Docking bay is sealed
    * Need to reach the hangar manual overrides

* Consequences
  * Heat or atmospheric failure
  * Lights off


# Resources
## Maps
Rebel hideout
ISB Office
Starport
Slums


## Tokens
ST
ST +
ST Breacher
AT-ST

Consular
CR70
DP20
Lancer
Tie
Tie Interceptor
Tie Bomber
ISD
Aetherspritew

Goon
BT-1


## Needed
* Stats
  * ST
  * Pilots
  * Crew

* Maps
  * Hangar
  * Detention bay
  * Corridors
  * Weapons control

* Tokens
