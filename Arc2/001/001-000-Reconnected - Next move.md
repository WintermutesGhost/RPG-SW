Back in deep space, the repairs from Satellite Petrichor are holding up well. The thrum of the oversized drive engines can still be heard throughout the ship, but you are no-longer rattled awake in the middle of the night-cycle.

Complaints from the droid have quieted, as well. It has begun referring to itself as the ship's Chief Mechanic, and has pressed the other crew into a make-shift engineering team. The newer crewmembers have not yet settled in, but work dilligently nonetheless.

There are eight of you aboard now, and while day-to-day operations are easier with the refurbished systems and additional hands, there is still more work than your non-military crew can manage easily.

*Each session the ship is undercrewed, either the ship or each member of the crew suffers one unrecoverable strain. The strain can only be recovered by time spent in dock, or on shore leave.*

---

The Petrichor repairs included a software update package to provide some compatibility with the modern Holonet. It takes several days to reconnect with your usual channels, and several more before you are caught up.

Everything looks roughly the same as when you left, but the buzz and the noise gives the impression of a galaxy in motion.


...coded communications arrive from Dr. Sungazer--he is eager to hear about your expedition to Kanthi. The past few months he has had to lay low, but he is trying to find a way back into academia and the Empire's good graces.

...sent only to trustworthy crews, Sgt. Starbringer and the Deaf Bandits need transport for 'official business'. A personalized attachment from Nej asks how Saar is doing.

...some ad holos slip through the ship's out-dated filters announcing the upcoming Cantonica TT. Despite being outlawed, Podracing still draws huge crowds to the "underground" venues.

...a standing request from Daro Tillie asks for a fast ship to make a delivery to Mygeeto. The Imperial blockade currently in place around the planet goes unmentioned.

...donations are still being collected to raise funds for those displaced by the Demophon Supernova. Tens of millions had to be evacuated, while billions more require expensive anti-radiation drugs on their homeworlds.

...a rambling message from Kodo invites you to visit his shop in Mos Shuuta. The city never recovered from the Imperial brutality nor the loss of Teemo, but he seems to be in good spirits.

...every official news-holo is covering the destruction of a rebel fleet above Atollon. In-depth reports include interviews with several Imperial heroes, as well as detailed depictions of Rebel attrocities on Lothal.

...underworld channels are buzzing with a hiring blitz from the shipping company TPI Consolidated. Mercenary crews are being hired in seven sectors for all positions aboard tramp freigters.


With fresh transponder codes and no sign of your pursuers, the galaxy feels open. You take the opportunity to plan your next moves...

