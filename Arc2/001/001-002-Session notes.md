A talks to Nasra, learns more about Nightsisters
Talk to Matthias, learn more
 - H asks for teaching about details
A and B train some with Saar and Skib
T asks Val Isa about possible relics and artefacts
H and A think dark side is alright, more how you use it that matters
Re-christen the ship as the "Solar Fate"
Visit Kodo - town not so good 
	- Mos Shuuta suffered, Askaj alright
Go to Baba's grill, he makes them
	- calls in CSA
	- Sgt. Jardan comes in, judges the situation
Quickly suspicious of Jardan, leave, go to talk to Kodo again
See Besh Seruna -> Female Miralan Podracer
A meets Melyn, goes to get water
	- Discuss the future of the town
Sgt. Jardan tries to slow them down, goes to slow them down
	- Gets mindread
Hesh comes aboard
	- Negotiation goes poorly for Hesh
	- T intimidates
	- Argue about what to do, A and T unwilling to be underhanded against imperials
	- Plan to double-cross him
	- T threatens
	- H wants to threaten, hates working with them
Message Daro, asking about Administrator Jenam
	- No relation
Message Isotech to sell durasteel, offer to come set up mining facility on Askaj, less hostile than usual
	- Will buy durasteel, no interest in mining
Nihi-Oshin looking to secure mineral sources on the Rim
Message Hesh, B threatens
	- Not hard to threaten
Go to Saleucami to negotiate with Walton Carthen
	- Want an investment
	- Offers stipend + repairs
		- Give 1000 to Hesh
		- 1000 to The People "Kodo cares"
		- 1000 to us
	- Suggests meeting with TPI Consolidated
		- Agree to meet with Uema
		


3 - Mathias is an ally
2 - Looking into pod racing
5 - Hesh under their thumb
4 - Secure a future for Mos Shutta
3 - Establishing a contact with Nihi-Oshin
2 - Contacts with TPI
1 R Support for Mos Shutta