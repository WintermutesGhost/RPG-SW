## Sungazer
> ...coded communications arrive from Dr. Sungazer--he is eager to hear about your expedition to Kanthi. The past few months he has had to lay low, but he is trying to find a way back into academia and the Empire's good graces.

* Currently staying on Sullust
* Ashur is curious what the Jedi have been up to
  * Disheartened to have missed the secrets of Kanthi
	* Would like to visit some time
  * Remains loyal to them
* Knows he has a target on his back, especially due to the imprisonment
  * Perhaps not so bad since the ISB agent responsible is disgraced
  * Been laying low but looking for a way back to academia
	* Preferably coreward, but could live in the mid-rim
	* Never like Heron, can't stand being out here on the rim
* Needs a way to prove his loyalty
  * Perhaps turining over some sufficiently interesting treasures...?


## Starbringer
> ...sent only to trustworthy crews, Sgt. Starbringer and the Deaf Bandits need transport for 'official business'. A personalized attachment from Nej asks how Saar is doing.

* Needs transport and support on a mission to *Wobani*
* Ostensibly a mission to break out captured rebels
  * Imperial labour camp LEG-822
  * 11 rebel pilots
	* Actually just pirates
* Actually providing diversion for Extraction Team Bravo
  * Evacuating Jyn Erso
  * Important rebel informant in LEG-817
* ISD-86 Besh - Victory class star destroyer sitting in orbit
  * Need to draw it away from the other operation


## Cantonica
> ...some ad holos slip through the ship's out-dated filters announcing the upcoming Cantonica TT. Despite being outlawed, Podracing still draws huge crowds to the "underground" venues.

* Beach side track
* Need a sponsor


## Daro
> ...a standing request from Daro Tillie asks for a fast ship to make a delivery to Mygeeto. The Imperial blockade currently in place around the planet goes unmentioned.


## Mos Shuuta
> ...a rambling message from Kodo invites you to visit his shop in Mos Shuuta. The city never recovered from the Imperial brutality nor the loss of Teemo, but he seems to be in good spirits.

* 70% of the population has left Mos Shuuta
* Current administrator (Hesh Oetho) has done little to help or harm the locals
  * Mostly just ineffective
  * Knows of the actions of the Jedi here
	* Not interested in turning them in, knows he doesn't have the resources
  * Offers to instead help the city... if only he had more pull
	* Perhaps could sabotage his superior
	  * Planetary Administrator Jenam
		* Straight-laced, effective if dull administrator
	  * Plant evidence of Rebel affiliation, provide Oetho with evidence
	  * Destroy imperial resources of Jenam's, to prompt an investigation
* Kodo is happy as usual, but makes do by hunting wild Ralaks
  * Mid-large, stone-skinned lizards, with delicate underbellies
* Nyseth-Motta Communications looking to expand to the planet
  * Actually a front for the Consortium
  
## TPI Consolidated
> ...underworld channels are buzzing with a hiring blitz from the shipping company TPI Consolidated. Mercenary crews are being hired in seven sectors for all positions aboard tramp freigters.

* Hiring for the future consortium fleet
* Uema Duskrover is TPI Rep of Mercenary Crew relations on Elrood
  * Will vet anyone before passing them on
  * Assisted by Sister Terrano - Reading minds, spells of truthfullness
* If the pass, sent to an asteroid in the Felucia system
  * Gathering crews, building a navy
  * Expected to prove loyalty
	* Raid an imperial transport
