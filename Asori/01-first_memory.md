01 Training day
	Awake to the sounds of drums. Wake in room on 9th day of summer, you are roughly 19. Heat is setting in, and the insects are numerous, even early in the morning.

	M Meet training masters for meditation
		. Takes place in the courtyeard.
		. Black sand in the yard, ivory stone for the structure
		. Half students already there
			. Most carry blades at hip or on back
			. Some still not been granted their own blade
				. Granted when ready
			. [AO] enters and sits next to Asori
				. Most recent student to be granted blade
				. Also second youngest in recorded tribe's history at only 17
					. After Master Va'ko
	D Training w/ Elder Jesma (learn the traits of a warrior)
		> Exercises / obstacle course
			+ Climbing (Athletics)
			+ Hop accross pillars (Coordination)
			+ Avoid swinging stones (Vigilance)
			+ Crawl through sands (Resilience)
			+ Swing along rope (Coordination/Athletics)
	A Fetching food from the village
		> Hunting party returns, no sign of any prey

02 Allies
	M Training w/ Elder Va'ko (learn to fight together)
		> Sparring between pairs of students
			. [AO] and Asori paired togethern
	D Meet Elder Zardoum
		> Sent to assist Titanis (Smith)
			. Recently provided materials for the school and repaired broken generator
	A Helping repair power converters for titanis
		+ Mechanics
		> Porrel arrives to deliver damaged farming equipment
			. Complains about noises in the jungle keeping awake

03 Rivals
	M Meet w/ Elder Kom'sara for education on the ways of fates 
		> Do any recoall the lessons of the last class on the histroy of the tribe?
			. When did the Tribe arrive on this planet?
			. 300 years ago
			. Very little known of the tribe at the time
			. First Scribe oonly 100 years ago, to help document the tappestris of fate
		> Today's lesson is on Master Va'Ko
			. Led the defence against pirate attack 21 years ago
			. Was a young warrior left by parents
			. Pirate frigate arrived to collect slaves and demand tribute from village, eventually came to walls of the monastery
				. Threatened to burn from orbit
			. Va'ko observed their leader
				. Saw he was a coward
			. When the pirates entered and the warriors readied to face them, Va'ka snuck behind
				. Attacked the captain herself with bare hands
				. Killed the honour guard with vibrodagger
			. Tactical discourse
				. Disarray as captain sought to protect self
			. Pirates slain
				. Ship sold to passing salvager to pay for reparations to village
		> Any progress in own meditations, meaning you do not understand?
			. Decisions of each weave together to make the whole
			. Each action produces ripples along 
			. What does fate mean
	D Training w/ Elder Vals (must learn to fight alone)
		> Sparing against [AO]
	A Summoned by Va'ko
		> Sent to village to speak to the herder, Landa
			. Animals have been killed overnight
			. On return, asked to speak to Me'lya and explain the situation

04 Inaction
	M Me'lya addresses students
		> To stay indoors unless told otherwise
		> Jesma leads through meditations
	D Nothing
	A Villagers come to temple seeking help
		> Uysen has gone missing

05 Ember
	M Summoned by Jesma
		> Villagers are assembling a hunting party
			. Jesma going with -- they will not survive alone
		> Time for [AO] to learn what it means to hunt
			. Asori will accompany, much to learn
	  Kom'sara steps forwards to explain what else they know
	  	. Beast is not of this world
	  	. May not be vulnerable to your weapons
	  	> Visitor to this planet gene
	  Jesma arms for trip
	  	. Both given armour
	  	. Asori given sword
	  	. Pack with food and travel goods
	D Travel with village hunters
		> Seek former home of the offworlder
			. Travel through jungle
				+ Survival
				+ Resolve
			. Cross a river
				+ Athletics
				+ Face rotortices
			. Reach the abandoned home
				+ Search for sword, Asori led to it
					. Burried beneath a Jade Rose plant
					. Long, straight blade, no crossguard, blue crystal at the hilt
			. Follow beast tracks
				+ Survival
	N Make camp
		> Attacked at night by beast
			+ []
				. Can only be killed by blade wielded by force-sensitive

06 Hero
	M Return to village
		> Journey is arduous with wounded
			+ Survival
			+ Resolve
	D Arrive in village
		> Celebration
	A Return to temple
		> Solemn congratulations
	N Visited by Grimm