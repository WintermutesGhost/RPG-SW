Dramatis Personae

Temple

00 Prasath Saeng
	. Monastery with four tall spires atop a bald hill
	. Made of solid piece of ivory-coloured stone resembling bone
	. Nothing grows near the monastery grounds
	. History unknown, legends of the Mirialan say nothing of its history, been here as long as recorded.
	. 26 here
		. 6  Masters
		. 6  Support
		. 14 Students
			. 3 off-worlders
				. Mivdu       - Poor student arrived with family on trader
				. Mula Vonden - Young Mirilan studying from Mirial
				. Afwar Agon  - Wealthy prince from Kamdio
			. 11 locals
				. Teckla Syko
				. Aumiyat Auten
				. Vital Dine
				. Eelleli Dara
				. Kandra Dine
				. Darro Anen
				. Vyna Berenk
				. Linis Honall
				. Nej Felis
				. Soont Dellian
				. [AO]

01 Elder Me'lya [F]
	R: First elder
	A: tall, slender, red hair and cyan eyes
	T: concentric diamonds on forhead, arc of diamonds beneath eyes
	P: committed, creative, surley
	L: recognition
	D: darkness
	H: stood alone in the tribe's darkest hour, when the villagers approached the fort during a time of famine, resolved without bloodshed
	S: has not held a blade since slaying her betrothed--an agent of the darkness

02 Elder Kom'sara [M]
	R: Scribe of Fates (Storyteller)
	A: tall, wide head-dress, brown eyes, burns along left of body
	T: stacked horizontaly bars along either side of face and vertical bars across bridge of nose
	P: objective, prudent, condescending
	L: competition
	D: loose-cannons
	H: badly burned pulling Me'lya from burning library 
	S: has read the prophesy at the heart of the temple, no-one else has

03 Elder Zardoum [M]
	R: Keeper of Blades
	A: short, plain leather tunic and gloves, bald
	T: downward-pointed triangles beneath eyes
	P: selfless, likable, hesitant
	L: good deals
	D: rushed work
	H: studies the creation of ancient swords
	S: posseses a protosabre

04 Elder Vals [M]
	R: Warrior-Master
	A: gray dreadlocks tied back, gray eyes
	T: stacked chevrons above and below eyes
	P: dutiful, cheerful, thin-skinned
	L: discovery
	D: messes
	H: bested Jesma at age 19
	S: arrived on planet alone in an escape pod as an infant

05 Elder Va'ko [F]
	R: Warrior-Master
	A: shoulder length brown hair tied into an intricate knot
	T: interlocking triangles across brow and chevrons beneath eyes
	P: investigative, organized
	L: tactics
	D: public speaking
	H: lead the defence of the castle when pirates attacked
	S: dark force user

06 Elder Jesma [F]
	R: Warrior-Master
	A: buzz-cut brown hair, dark green eyes
	T: diamond tattoos covering cheeks
	P: skeptical, cunning, wilful
	L: hunting
	D: messes
	H: used to help with the village hunters, slew massive ratha-beast that harrassed the village
	S: slew the beast in its sleep


99 Rival
	N: Veba [M]
	A: Missing right eye
	P: authentic, vivacious, rebellious
	L: combat
	D: pranks

Village

01 Constable:
	N: Elorrs Limpaan [M]
	A: small scars on face
	P: supportive, humble
	L: people-watching
	D: criminals

02 Mechanic:
	N: Eyn [F]
	A: grease stains on hands and face
	P: practical, direct, opinionated
	L: learning something new
	D: space travel

03 Physician:
	N: Issal Vehbar [F]
	A: large, hooded cloak
	P: assertive, imperturbable, unfocused
	L: scientific discovery
	D: gossip

04 Offworld merchant:
	N: Kallo [M]
	A: eyes different colour
	P: daring, spunky, scatterbrained
	L: novels
	D: not being in the loop

05 Luxuries merchant:
	N: Vo Cari [M]
	A: thick leather glove on right hand
	P: self-posessed, guileful, dominant
	L: combatants
	D: wasting time

06 Farmer:
	N: Vitaara Cai [F]
	A: goggles
	P: loyal, earnest, unrealistic
	L: reading holonet
	D: pranks

07 Herder:
	N: Landa Imessi [F]
	A: heavyset, scanning the area
	P: resourceful, outspoken, irritable
	L: learning something new
	D: commitment

08 Smith:
	N: Titaniss Urwi [F]
	A: scars on face, tired
	P: caring, civil, smothering
	L: being relied on
	D: everyone not pulling their part

09 Teacher:
	N: Chim Borko [M]
	A: left cybereye
	P: cultured somewhat sensible, self-indulgent
	L: fighters
	D: outskirts of civilization

10 Hunter:
	N: Orca [M]
	A: striking yellow eyes
	P: friendly, enthusiastic, perfectionist
	L: swoop-racing
	D: rushed work


Tenebrous

Ahhhh, young Asori, so eager to prove herself, risking her life to show her worth, to find her place. Desperate to know what fate held in store.

. Why are you here?

Did you know then, all those years ago, that the beast would never have come were it not for you?

 No base predator, it awoke from its sleep to stalk a power it had not tasted for decades. It killed only to draw out its true prey.

You, Asori, are a flickering ember against the cold void, calling the wolves from their shadows. 

Three died that night, for you--the only worthy prey.


. Who are you?

I am the one that lives in the shadows of your heart.

The guiding voice echoing from the deep.

Bringer of strength to those who are lost.

I have many names. You will know me as Tenebrous.


. Why me?

I am here that you may remember your story, and understand the darkness.

Through your life, you stepped apprehensively along fate's path, unaware of the flickering shadows you cast all around --plunging the lives of others into darkness.

Flickering glimpses by the candle-light, but given time, you will remember, and you will understand where your path leads.


. What do you want?

You were taught to fear the darkness itself, a simplification for those with simpler paths to walk. 

All beings cast shadows. It is not the dark that threatens you, but those that live within it and those who would take your path.

You have spent your life fighting the darkness, failing as it grew deeper all around. Soon you will realize it was only your shadow--an inseparable companion: challenging you, training you, guiding you.

A time will come when only your shadow stands beside you against the schemers in the dark--you will need to trust it by then if you wish to see the end of your path.


