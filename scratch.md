♢ ⬡ ◻
### Ideas
#### Campaigns
https://starwars.fandom.com/wiki/Leviathan_(creature)

#### Threats
https://starwars.fandom.com/wiki/Sith_poison


### Collections
##### WEG Books
http://d6holocron.com/downloads/wegsourcebooks.html  


### Creatures
##### Creatures of the Galaxy (WEG)
http://d6holocron.com/downloads/books/WEG40080.pdf  
> **015** - Brown Nafen  


### Places
##### Hideouts & Strongholds (WEG)
http://d6holocron.com/downloads/books/WEG40111.pdf  
> **093** - Rebel Outpost  

### Stories
##### 
http://d6holocron.com/downloads/books/WEG40063.pdf
> **017** - Special Operations Slang  
> **023** - High Inquisitor Tremayne  